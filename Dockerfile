# Prepare front-end build stage
FROM node:15 AS frontend-build
ENV NODE_ENV=production
WORKDIR /usr/src/app

# Download yarn dependencies using the package and yarn lock file.
COPY frontend/package.json frontend/yarn.lock ./
RUN yarn install --pure-lockfile

# Build the front-end with all its source code present.
COPY frontend .
RUN yarn build

# Prepare back-end build stage
FROM maven:3.6.3-adoptopenjdk-11 AS backend-build
WORKDIR /usr/src/app

# Copy just the POM initially so that Maven can use it to establish dependencies.
COPY backend/pom.xml .

# Download the dependencies for "offline mode" (Docker will cache this image for faster subsequent builds).
RUN mvn -B -e -C -T 1C org.apache.maven.plugins:maven-dependency-plugin:3.1.2:go-offline

# Copy the src folder for compilation
COPY backend/src ./src

# Run verify phase to compile fat JAR and run checks
RUN mvn -B -e -T 1C -P test verify \
    && mkdir -p target/dependency \
    && (cd target/dependency; jar -xf ../*.jar)

# Prepare service stage
FROM adoptopenjdk:11-jre-hotspot AS service

# Provide a volume for "tmp" storage
VOLUME /tmp

# User setup
RUN groupadd --system --gid 1000 spring \
    && useradd --system --gid spring --uid 1000 --shell /bin/bash --create-home spring

# Image setup (install dockerize)
ARG DOCKERIZE_VERSION=v0.6.1
RUN apt-get -q update \
    && apt-get -qy install wget \
    && wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

# Copy build artifacts from build stages
ARG DEPENDENCY=/usr/src/app/target/dependency
COPY --from=backend-build ${DEPENDENCY}/BOOT-INF/lib /home/spring/app/lib
COPY --from=backend-build ${DEPENDENCY}/META-INF /home/spring/app/META-INF
COPY --from=backend-build ${DEPENDENCY}/BOOT-INF/classes /home/spring/app
COPY --from=frontend-build /usr/src/app/build /home/spring/app/static

# Change ownership to non-root user
RUN chown -R spring:spring /home/spring
WORKDIR /home/spring
USER spring:spring

# Run entrypoint
ENTRYPOINT ["java", "-cp", "app:app/lib/*", "pzzo.leaflet.LeafletApplication"]
