# Leaflet

It's a web app to help you journal your plants!

The back-end is developed in Java + Spring Boot, and the front-end is developed using React and Redux.


## How to use

Once you've checked out the project, there are a couple of extra setup steps to install the service locally or set it up for development:

1. Make a copy of the `.env-sample` file as `.env` in the `backend` directory. Replace the placeholders in the file with the credentials of the external systems (AWS and MySQL).
2. Run `docker-compose up` to deploy the DB and web server.

The application should now be accessible through http://localhost:8080. 

## Development

### Front-end

To start the front-end, go into the `frontend` directory and run:
```
yarn start
```

This will start a server that serves the front-end on port `3000`. It will also watch for code changes and rebuild when changes have been detected.

If the back-end is running, the front-end server will also proxy out API requests to the back-end if you access the page using port `3000`. 


### Back-end

If you plan to have the back-end serve the page and not run the front-end server, you'll need to build the front-end first. To do that, go into the `frontend` directory and run:

```
yarn build
```

To start the back-end, go into the `backend` directory and run:
```
mvn spring-boot:run
```

The server will be running on port `8080`. Like the front-end server, it will watch for source code changes and restart when they are detected.
