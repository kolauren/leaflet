package pzzo.leaflet.collection;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import pzzo.leaflet.plant.CurrentUserPlant;
import pzzo.leaflet.user.LeafletUserDetails;

import javax.validation.Valid;
import java.util.List;

import static pzzo.leaflet.core.OpenAPIConfig.Security.USER_SESSION;
import static pzzo.leaflet.core.OpenAPIConfig.Tags.CURRENT_USER;

@RequestMapping(path = "/api/me/collections")
public interface CurrentUserPlantCollectionAPI {

    @Operation(
            description = "Returns any plant collections managed by the logged-in user.",
            security = @SecurityRequirement(name = USER_SESSION),
            tags = CURRENT_USER
    )
    @GetMapping
    List<PlantCollection> getCollections(@AuthenticationPrincipal LeafletUserDetails userDetails);

    @GetMapping("{id}")
    PlantCollection getCollection(
            @AuthenticationPrincipal LeafletUserDetails userDetails,
            @PathVariable Long id
    );

    @Operation(
            description = "Creates a new, empty plant collection.",
            security = @SecurityRequirement(name = USER_SESSION),
            tags = CURRENT_USER
    )
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    PlantCollection addCollection(
            @AuthenticationPrincipal LeafletUserDetails userDetails,
            @Valid @RequestBody PlantCollectionUpdate collectionDetails
    );

    @Operation(
            description = "Update details about the user's plant collection.",
            security = @SecurityRequirement(name = USER_SESSION),
            tags = CURRENT_USER
    )
    @PutMapping("{id}")
    PlantCollection updateCollection(
            @AuthenticationPrincipal LeafletUserDetails userDetails,
            @PathVariable Long id,
            @Valid @RequestBody PlantCollectionUpdate collectionDetails
    );

    @Operation(
            description = "Remove collection from the user's account.",
            security = @SecurityRequirement(name = USER_SESSION),
            tags = CURRENT_USER
    )
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteCollection(
            @AuthenticationPrincipal LeafletUserDetails userDetails,
            @PathVariable Long id
    );

    @Operation(
            description = "Fetches all plants from the collection.",
            security = @SecurityRequirement(name = USER_SESSION),
            tags = CURRENT_USER
    )
    @GetMapping("{id}/plants")
    List<CurrentUserPlant> getPlantsFromCollection(
            @AuthenticationPrincipal LeafletUserDetails userDetails,
            @PathVariable Long id
    );
}
