package pzzo.leaflet.collection;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import pzzo.leaflet.plant.CurrentUserPlant;
import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.user.LeafletUserDetails;

@RestController
public class CurrentUserPlantCollectionController implements CurrentUserPlantCollectionAPI {

    @Autowired
    private PlantCollectionService collectionService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<PlantCollection> getCollections(LeafletUserDetails userDetails) {
        return collectionService.getAllFromOwner(userDetails.getUser());
    }

    @Override
    public PlantCollection getCollection(LeafletUserDetails userDetails, Long id) {
        return getFromOwner(id, userDetails);
    }

    @Override
    public PlantCollection addCollection(
        LeafletUserDetails userDetails,
        PlantCollectionUpdate collection
    ) {
        return collectionService.create(userDetails.getUser(), collection);
    }

    @Override
    public PlantCollection updateCollection(
        LeafletUserDetails userDetails,
        Long id,
        PlantCollectionUpdate updates
    ) {
        return collectionService.update(id, userDetails.getUser(), updates)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public void deleteCollection(
        LeafletUserDetails userDetails,
        Long id
    ) {
        PlantCollection collection = getFromOwner(id, userDetails);
        collectionService.remove(collection);
    }

    @Override
    public List<CurrentUserPlant> getPlantsFromCollection(
        LeafletUserDetails userDetails,
        Long id
    ) {
        return getFromOwner(id, userDetails)
            .getPlants()
            .stream()
            .map(this::map)
            .collect(Collectors.toList());
    }

    private PlantCollection getFromOwner(Long id, LeafletUserDetails userDetails) throws ResponseStatusException {
        return collectionService
            .getFromOwner(id, userDetails.getUser())
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
    
    private CurrentUserPlant map(Plant plant) {
        return modelMapper.map(plant, CurrentUserPlant.class);
    }
}
