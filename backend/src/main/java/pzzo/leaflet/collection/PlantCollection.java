package pzzo.leaflet.collection;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.user.User;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class PlantCollection {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @NotBlank(message = "Plant collection name must not be blank.")
    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @JsonIgnore
    private User owner;

    @OneToMany(mappedBy = "collection", cascade = { CascadeType.PERSIST })
    @Builder.Default
    @JsonIgnore
    private List<Plant> plants = new ArrayList<>();

    public void addPlant(Plant p) {
        plants.add(p);
        p.setCollection(this);
    }

    public void removePlant(Plant p) {
        plants.remove(p);
        p.setCollection(null);
    }

    public void removeAllPlants() {
        plants.forEach(p -> p.setCollection(null));
        plants.clear();
    }
}
