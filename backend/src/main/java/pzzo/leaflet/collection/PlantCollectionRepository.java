package pzzo.leaflet.collection;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pzzo.leaflet.user.User;

@Repository
public interface PlantCollectionRepository extends CrudRepository<PlantCollection, Long> {
    List<PlantCollection> findByOwner(User owner);
}
