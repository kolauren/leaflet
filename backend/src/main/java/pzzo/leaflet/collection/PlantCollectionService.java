package pzzo.leaflet.collection;

import java.util.List;
import java.util.Optional;

import pzzo.leaflet.user.User;

public interface PlantCollectionService {
    
    Optional<PlantCollection> getFromOwner(Long collectionId, User owner);

    List<PlantCollection> getAllFromOwner(User owner);

    PlantCollection create(User owner, PlantCollectionUpdate details);

    Optional<PlantCollection> update(Long id, User owner, PlantCollectionUpdate details);

    void remove(PlantCollection collection);
}
