package pzzo.leaflet.collection;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pzzo.leaflet.user.User;

@Service
public class PlantCollectionServiceImpl implements PlantCollectionService {

    @Autowired
    private PlantCollectionRepository collectionRepository;

    @Override
    public Optional<PlantCollection> getFromOwner(Long collectionId, User owner) {
        return collectionRepository.findById(collectionId).filter(c -> c.getOwner().getId().equals(owner.getId()));
    }

    @Override
    public List<PlantCollection> getAllFromOwner(User owner) {
        return collectionRepository.findByOwner(owner);
    }

    @Override
    public PlantCollection create(User owner, PlantCollectionUpdate details) {
        PlantCollection collection = PlantCollection.builder()
                .name(details.getName())
                .owner(owner)
                .build();
        return collectionRepository.save(collection);
    }

    @Override
    public Optional<PlantCollection> update(Long id, User owner, PlantCollectionUpdate details) {
        Optional<PlantCollection> collection = getFromOwner(id, owner);
        return collection.map(c -> {
            c.setName(details.getName());
            return collectionRepository.save(c);
        });
    }


    @Override
    public void remove(PlantCollection collection) {
        // Unlink all the plants from the collection
        collection.removeAllPlants();
        collectionRepository.save(collection);

        // Remove it.
        collectionRepository.delete(collection);
    }
}
