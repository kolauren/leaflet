package pzzo.leaflet.collection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlantCollectionUpdate {

    @NotBlank(message = "Plant collection name must not be blank.")
    private String name;
}
