package pzzo.leaflet.core;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;
import pzzo.leaflet.core.storage.s3.LeafletAmazonS3Client;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "app.aws")
public class AmazonS3Config {

    private String accessKey;

    private String secretKey;

    private String region;

    private String bucketName;

    @Bean
    public LeafletAmazonS3Client getClient() {
        return new LeafletAmazonS3Client(accessKey, secretKey, region, bucketName);
    }
}