package pzzo.leaflet.core;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.tags.Tag;

@SecurityScheme(
    name = OpenAPIConfig.Security.USER_SESSION,
    type = SecuritySchemeType.APIKEY,
    in = SecuritySchemeIn.COOKIE,
    paramName = "JSESSIONID"
)
@OpenAPIDefinition(
    info = @Info(
        title = "Leaflet API",
        version = "0.0.1",
        description = "An API that manages data for your plants!"
    ),
    tags = {
        @Tag(
            name = OpenAPIConfig.Tags.AUTHENTICATION,
            description = "Endpoints dedicated to Authentication (login, logout, signup)."
        ),
        @Tag(
            name = OpenAPIConfig.Tags.CURRENT_USER,
            description = "Endpoints working in the current user context."
        )
    }
)
@Configuration
public class OpenAPIConfig {

    public static final class Security {
        private Security() {}

        public static final String USER_SESSION = "userSession";
    }

    public static final class Tags {
        private Tags() {}

        public static final String AUTHENTICATION = "authentication";
        public static final String CURRENT_USER = "currentUser";
    }
}
