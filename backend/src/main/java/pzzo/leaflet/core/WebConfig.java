package pzzo.leaflet.core;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "app.web")
public class WebConfig implements WebMvcConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(WebConfig.class);

    @Getter
    @Setter
    private String[] resourceLocations;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        Arrays.asList(
            "/",
            "/landing/*",
            "/login",
            "/plants/**",
            "/signup"
        ).forEach(pattern -> registry.addViewController(pattern).setViewName("/index.html"));
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String[] locations = new String[resourceLocations.length];
        for (int i = 0; i < resourceLocations.length; i++) {
            locations[i] = leafletURIParser(resourceLocations[i]);
            logger.info("Parsed resource location: {}", locations[i]);
        }
        registry.addResourceHandler("/**").addResourceLocations(locations);
    }

    private String leafletURIParser(String url) {
        try {
            URI uri = new URI(url);
            if (!"leaflet".equals(uri.getScheme())) {
                return url;
            }
            return Path.of("..", uri.getRawSchemeSpecificPart())
                    .toAbsolutePath()
                    .normalize()
                    .toUri()
                    .toString();
        } catch (URISyntaxException e) {
            return url;
        }
    }
}
