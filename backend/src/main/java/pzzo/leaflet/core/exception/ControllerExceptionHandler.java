package pzzo.leaflet.core.exception;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

@ControllerAdvice
public class ControllerExceptionHandler {
    
    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<Problem> handleResponseStatusException(HttpServletRequest r, ResponseStatusException e) {
        HttpStatus s = e.getStatus();
        Problem p = Problem.builder()
            .title(s.getReasonPhrase())
            .status(s.value())
            .detail(e.getReason())
            .instance(r.getRequestURI())
            .build();
        return ResponseEntity.status(s).contentType(MediaType.APPLICATION_PROBLEM_JSON).body(p);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Problem> handleMethodArgumentNotValidException(HttpServletRequest r, MethodArgumentNotValidException e) {
        // Parse the validation errors from the exception and return it with the 'data' of the problem.
        Map<String, Object> data = new HashMap<>();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        data.put(fieldName, errorMessage);
        });
        
        HttpStatus s = HttpStatus.BAD_REQUEST;
        Problem p = Problem.builder()
            .title(s.getReasonPhrase())
            .status(s.value())
            .detail("Validation errors were found while handling the request: see 'data' for details.")
            .instance(r.getRequestURI())
            .data(data)
            .build();
        return ResponseEntity.status(s).contentType(MediaType.APPLICATION_PROBLEM_JSON).body(p);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Problem> handleMethodArgumentTypeMismatchException(HttpServletRequest r, MethodArgumentTypeMismatchException e) {
        // This error would come up when path variables don't arrive in the right format. We'll treat these as Bad Requests.
        HttpStatus s = HttpStatus.BAD_REQUEST;
        Problem p = Problem.builder()
            .title(s.getReasonPhrase())
            .status(s.value())
            .detail("Request path is incorrect: check the identifiers and try again.")
            .instance(r.getRequestURI())
            .build();
        return ResponseEntity.status(s).contentType(MediaType.APPLICATION_PROBLEM_JSON).body(p);
    }
}
