package pzzo.leaflet.core.exception;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Problem {
    
    @JsonInclude(Include.NON_NULL)
    private String type;

    private String title;

    private Integer status;

    private String detail;

    private String instance;

    @JsonInclude(Include.NON_NULL)
    private Map<String, Object> data;
}
