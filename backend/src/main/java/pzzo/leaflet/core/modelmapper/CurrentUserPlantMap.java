package pzzo.leaflet.core.modelmapper;

import org.modelmapper.PropertyMap;

import pzzo.leaflet.plant.CurrentUserPlant;
import pzzo.leaflet.plant.Plant;

public class CurrentUserPlantMap extends PropertyMap<CurrentUserPlant, Plant> {

    @Override
    protected void configure() {
        skip().setClassification(null);
        skip().setCollection(null);
        skip().setOwner(null);
        skip().setDefaultImage(null);
    }
}
