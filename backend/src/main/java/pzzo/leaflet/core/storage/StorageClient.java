package pzzo.leaflet.core.storage;

import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.util.List;
import java.util.function.Function;

public interface StorageClient {

    Function<String, URL> getPublicURLMapper();

    String upload(MultipartFile file) throws StorageOperationException;

    void delete(String key) throws StorageOperationException;

    void deleteAll(List<String> keys) throws StorageOperationException;
}
