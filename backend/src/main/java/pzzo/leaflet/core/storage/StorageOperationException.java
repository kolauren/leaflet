package pzzo.leaflet.core.storage;

public class StorageOperationException extends Exception {

    public StorageOperationException(String message) {
        super(message);
    }

    public StorageOperationException(String message, Throwable t) {
        super(message, t);
    }
}
