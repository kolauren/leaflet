package pzzo.leaflet.core.storage.s3;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import pzzo.leaflet.core.storage.StorageClient;
import pzzo.leaflet.core.storage.StorageOperationException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class AmazonS3StorageClient implements StorageClient {

    private final AmazonS3 amazonS3;

    private final LeafletAmazonS3Client client;

    private static final Logger logger = LoggerFactory.getLogger(AmazonS3StorageClient.class);

    public AmazonS3StorageClient(LeafletAmazonS3Client client) {
        this.client = client;
        this.amazonS3 = AmazonS3ClientBuilder.standard()
                .withCredentials(client.getCredentials())
                .withRegion(Regions.fromName(client.getRegion()))
                .build();
    }

    @Override
    public Function<String, URL> getPublicURLMapper() {
        return (key) -> {
            try {
                return client.getObjectUrl(key);
            } catch (MalformedURLException e) {
                throw new RuntimeException("Storage public URL could not be parsed because it is malformed.", e);
            }
        };
    }

    @Override
    public String upload(MultipartFile file) throws StorageOperationException {
        try {
            String fileName = generateFileName();
            String bucketName = client.getBucketName();

            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(file.getSize());
            objectMetadata.setContentType(file.getContentType());

            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName, file.getInputStream(), objectMetadata);

            this.amazonS3.putObject(putObjectRequest);
            return fileName;
        } catch (Exception e) {
            logger.error("Amazon S3 upload failed: {}", e.getMessage());
            throw new StorageOperationException("File upload has failed.", e);
        }
    }

    @Override
    public void delete(String key) throws StorageOperationException {
        try {
            String bucketName = client.getBucketName();
            amazonS3.deleteObject(bucketName, key);
        } catch (Exception e) {
            logger.error("Amazon S3 delete failed: {}", e.getMessage());
            throw new StorageOperationException("File deletion has failed.", e);
        }
    }

    @Override
    public void deleteAll(List<String> keys) throws StorageOperationException {
        try {
            String bucketName = client.getBucketName();
            List<DeleteObjectsRequest.KeyVersion> keyVersions = keys.stream()
                    .map(DeleteObjectsRequest.KeyVersion::new)
                    .collect(Collectors.toList());
            DeleteObjectsRequest deleteObjectsRequest = new DeleteObjectsRequest(bucketName).withKeys(keyVersions);
            amazonS3.deleteObjects(deleteObjectsRequest);
        } catch (Exception e) {
            logger.error("Amazon S3 delete failed: {}", e.getMessage());
            throw new StorageOperationException("File deletion has failed.", e);
        }
    }

    private String generateFileName() {
        return UUID.randomUUID().toString();
    }
}
