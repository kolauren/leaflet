package pzzo.leaflet.core.storage.s3;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;

import lombok.Getter;

import java.net.MalformedURLException;
import java.net.URL;

@Getter
public class LeafletAmazonS3Client {

    private final String accessKey;

    private final String secretKey;

    private final String region;

    private final String bucketName;
    
    public LeafletAmazonS3Client(String accessKey, String secretKey, String region, String bucketName) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.region = region;
        this.bucketName = bucketName;
    }

    public AWSCredentialsProvider getCredentials() {
        BasicAWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
        return new AWSStaticCredentialsProvider(credentials);
    }

    public URL getObjectUrl(String key) throws MalformedURLException {
        final String host = String.format("%s.s3.%s.amazonaws.com", bucketName, region);
        final String file = String.format("/%s", key);
        return new URL("https", host, file);
    }
}
