package pzzo.leaflet.plant;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;
import pzzo.leaflet.collection.PlantCollection;
import pzzo.leaflet.plant.classification.PlantClassification;
import pzzo.leaflet.plant.image.PlantImage;

@Data
public class CurrentUserPlant {

    @JsonProperty(access = Access.READ_ONLY)
    private Long id;

    @NotBlank(message = "Plant must have a name.")
    private String name;

    private Date bornDate;

    private Date acquiredDate;

    @JsonProperty(access = Access.WRITE_ONLY)
    private Long collectionId;

    @JsonProperty(access = Access.READ_ONLY)
    private PlantCollection collection;

    @JsonProperty(access = Access.WRITE_ONLY)
    private Long classificationId;

    @JsonProperty(access = Access.READ_ONLY)
    private PlantClassification classification;

    @JsonProperty(access = Access.READ_ONLY)
    private PlantImage defaultImage;

}
