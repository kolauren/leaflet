package pzzo.leaflet.plant;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MatchingOwnersValidator.class)
@Documented
public @interface MatchingOwners {
    String message() default "{pzzo.leaflet.plant.MatchingOwners.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
