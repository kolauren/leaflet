package pzzo.leaflet.plant;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import pzzo.leaflet.collection.PlantCollection;
import pzzo.leaflet.user.User;

public class MatchingOwnersValidator implements ConstraintValidator<MatchingOwners, Plant> {

    private String message;

    @Override
    public void initialize(MatchingOwners constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Plant value, ConstraintValidatorContext context) {
        User owner = value.getOwner();
        PlantCollection collection = value.getCollection();

        boolean isValid = collection == null || owner.getId() == collection.getOwner().getId();
        if (!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message)
                .addPropertyNode("collection")
                .addConstraintViolation();
        }

        return isValid;
    }
    
}
