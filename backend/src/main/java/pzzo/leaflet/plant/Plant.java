package pzzo.leaflet.plant;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pzzo.leaflet.collection.PlantCollection;
import pzzo.leaflet.plant.classification.PlantClassification;
import pzzo.leaflet.plant.image.PlantImage;
import pzzo.leaflet.user.User;

@Entity
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@MatchingOwners
public class Plant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String name;

    @Temporal(TemporalType.DATE)
    private Date bornDate;

    @Temporal(TemporalType.DATE)
    private Date acquiredDate;

    @ManyToOne
    @JoinColumn
    private PlantCollection collection;

    @ManyToOne(optional = false)
    @JoinColumn(nullable = false)
    private User owner;

    @ManyToOne
    @JoinColumn
    private PlantClassification classification;

    @ManyToOne
    @JoinColumn
    private PlantImage defaultImage;

    @OneToMany(mappedBy = "plant", cascade = { CascadeType.PERSIST })
    @Builder.Default
    @JsonIgnore
    private List<PlantImage> images = new ArrayList<>();
}