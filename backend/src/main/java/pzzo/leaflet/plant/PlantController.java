package pzzo.leaflet.plant;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import pzzo.leaflet.core.storage.StorageClient;
import pzzo.leaflet.core.OpenAPIConfig;
import pzzo.leaflet.plant.classification.PlantClassification;
import pzzo.leaflet.collection.PlantCollection;
import pzzo.leaflet.plant.image.PlantImage;
import pzzo.leaflet.user.User;
import pzzo.leaflet.plant.classification.PlantClassificationService;
import pzzo.leaflet.collection.PlantCollectionService;
import pzzo.leaflet.plant.image.PlantImageService;
import pzzo.leaflet.user.LeafletUserDetails;

@RestController
@RequestMapping("/api/me/plants")
public class PlantController {

    @Autowired
    private PlantService plantService;

    @Autowired
    private PlantCollectionService plantCollectionService;

    @Autowired
    private PlantClassificationService plantClassificationService;

    @Autowired
    private PlantImageService imageService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private StorageClient storage;

    @Operation(
        description = "Searches for plants owned by the current user.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @GetMapping
    public List<CurrentUserPlant> getAllPlants(@AuthenticationPrincipal LeafletUserDetails userDetails) {
        return plantService.getPlantsFromOwner(userDetails.getUser())
            .stream()
            .map(this::map)
            .collect(Collectors.toList());
    }

    @Operation(
        description = "Add plant records to the current user.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @PostMapping
    public CurrentUserPlant createPlant(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @Valid @RequestBody CurrentUserPlant currentUserPlant
    ) {
        Plant plant = map(currentUserPlant, userDetails.getUser());
        return map(plantService.save(plant));
    }

    @Operation(
        description = "Return a single plant record owned by the current user.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @GetMapping("{id}")
    public CurrentUserPlant getPlant(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long id
    ) {
        return map(getFromOwner(id, userDetails));
    }

    @Operation(
        description = "Updates a plant record.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @PutMapping("{id}")
    public CurrentUserPlant updatePlant(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @Valid @RequestBody CurrentUserPlant currentUserPlant,
        @PathVariable Long id
    ) {
        Plant currentPlant = getFromOwner(id, userDetails);
        Plant plant = map(currentUserPlant, userDetails.getUser());
        plant.setId(id);
        plant.setDefaultImage(currentPlant.getDefaultImage());
        return map(plantService.save(plant));
    }

    @Operation(
        description = "Deletes a plant record.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @DeleteMapping("{id}")
    public void removePlant(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long id
    ) {
        Plant plant = getFromOwner(id, userDetails);
        List<PlantImage> images = imageService.getImagesFromPlant(plant);

        try {
            if (images.size() > 0) {
                List<String> fileNames = images.stream().map(image -> image.getUrl()).collect(Collectors.toList());
                storage.deleteAll(fileNames);
                imageService.deleteAll(images);
            }
            plantService.delete(plant);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Plant could not be removed due to an internal error.",
                    e
            );
        }
    }

    private Plant getFromOwner(Long id, LeafletUserDetails userDetails) throws ResponseStatusException {
        return plantService
            .getOwnedPlantById(userDetails.getUser(), id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    private Plant map(CurrentUserPlant plant, User owner) {
        PlantCollection collection = Optional.ofNullable(plant.getCollectionId())
            .map(id -> plantCollectionService.getFromOwner(id, owner).orElse(null))
            .orElse(null);
        PlantClassification classification = Optional.ofNullable(plant.getClassificationId())
            .map(id -> plantClassificationService.getClassification(id).orElse(null))
            .orElse(null);
        Plant p = modelMapper.map(plant, Plant.class);
        p.setCollection(collection);
        p.setClassification(classification);
        p.setOwner(owner);
        return p;
    }

    private CurrentUserPlant map(Plant plant) {
        CurrentUserPlant currentUserPlant = modelMapper.map(plant, CurrentUserPlant.class);
        PlantImage image = currentUserPlant.getDefaultImage();

        if (image != null) {
            image.setUrl(storage.getPublicURLMapper().apply(image.getUrl()).toString());
        }

        return currentUserPlant;
    }

}
