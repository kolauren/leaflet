package pzzo.leaflet.plant;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pzzo.leaflet.user.User;

@Repository
public interface PlantRepository extends CrudRepository<Plant, Long> {

    Optional<Plant> findByIdAndOwner(Long id, User owner);

    List<Plant> findByOwner(User owner);
}
