package pzzo.leaflet.plant;

import java.util.List;
import java.util.Optional;

import pzzo.leaflet.user.User;

public interface PlantService {

    public Optional<Plant> getOwnedPlantById(User owner, Long id);

    public List<Plant> getPlantsFromOwner(User owner);

    public Plant save(Plant plant);

    public void delete(Plant plant);
}
