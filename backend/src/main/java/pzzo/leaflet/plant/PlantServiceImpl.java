package pzzo.leaflet.plant;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.stereotype.Service;

import pzzo.leaflet.user.User;

@Service
public class PlantServiceImpl implements PlantService {

    private final Validator validator;

    private final PlantRepository plantRepository;

    public PlantServiceImpl(Validator validator, PlantRepository plantRepository) {
        this.validator = validator;
        this.plantRepository = plantRepository;
    }

    @Override
    public Optional<Plant> getOwnedPlantById(User owner, Long id) {
        return plantRepository.findByIdAndOwner(id, owner);
    }

    @Override
    public List<Plant> getPlantsFromOwner(User owner) {
        return plantRepository.findByOwner(owner);
    }

    @Override
    public Plant save(Plant plant) {
        Set<ConstraintViolation<Plant>> errors = validator.validate(plant);
        if (!errors.isEmpty()) {
            throw new ConstraintViolationException(errors);
        }
        return plantRepository.save(plant);
    }

    @Override
    public void delete(Plant plant) {
        plantRepository.delete(plant);
    }
}
