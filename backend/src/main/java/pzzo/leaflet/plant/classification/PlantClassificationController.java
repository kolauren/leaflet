package pzzo.leaflet.plant.classification;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/classifications")
public class PlantClassificationController {

    @Autowired
    private PlantClassificationService plantClassificationService;

    @GetMapping
    public List<PlantClassification> getClassifications(@RequestParam String query) {
        return plantClassificationService.search(query);
    }
}
