package pzzo.leaflet.plant.classification;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantClassificationRepository extends CrudRepository<PlantClassification, Long> {
    
    @Query(value = "SELECT * FROM plant_classification "
        + "WHERE (to_tsvector('english', common_name) @@ to_tsquery('english', ?1 || ':*')) "
	    + "OR (to_tsvector('english', scientific_name) @@ to_tsquery('english', ?1 || ':*'));",
        nativeQuery = true)
    List<PlantClassification> search(String needle);
}
