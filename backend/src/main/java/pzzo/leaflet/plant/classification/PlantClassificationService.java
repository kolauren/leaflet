package pzzo.leaflet.plant.classification;

import java.util.List;
import java.util.Optional;

public interface PlantClassificationService {
    public List<PlantClassification> search(String needle);

    public Optional<PlantClassification> getClassification(Long classification);
}
