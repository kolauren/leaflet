package pzzo.leaflet.plant.classification;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlantClassificationServiceImpl implements PlantClassificationService {

    @Autowired
    private PlantClassificationRepository classificationRepository;

    @Override
    public List<PlantClassification> search(String needle) {
        return classificationRepository.search(needle);
    }

    @Override
    public Optional<PlantClassification> getClassification(Long id) {
        return classificationRepository.findById(id);
    }
}
