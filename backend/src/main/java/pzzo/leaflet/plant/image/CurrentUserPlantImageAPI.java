package pzzo.leaflet.plant.image;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pzzo.leaflet.core.OpenAPIConfig;
import pzzo.leaflet.user.LeafletUserDetails;

import java.util.List;

@RequestMapping("/api/me/plants/{plantId}/images")
public interface CurrentUserPlantImageAPI {
    @Operation(
        description = "Gets all plant images.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @GetMapping
    List<PlantImage> getImages(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long plantId
    );

    @Operation(
        description = "Add image",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    List<PlantImage> uploadImage(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @RequestParam MultipartFile file,
        @RequestParam Boolean isDefault,
        @PathVariable Long plantId
    );

    @Operation(
        description = "Set image as default",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @PutMapping("{plantImageId}/default")
    List<PlantImage> setDefault(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long plantImageId,
        @PathVariable Long plantId
    );

    @Operation(
        description = "Deletes a plant image.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @DeleteMapping("{plantImageId}")
    List<PlantImage> removeImage(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long plantImageId,
        @PathVariable Long plantId
    );
}
