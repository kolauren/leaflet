package pzzo.leaflet.plant.image;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import pzzo.leaflet.core.storage.StorageClient;
import pzzo.leaflet.core.storage.StorageOperationException;
import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.user.User;
import pzzo.leaflet.plant.PlantService;
import pzzo.leaflet.user.LeafletUserDetails;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PlantImageController implements CurrentUserPlantImageAPI {

    private static final Integer IMAGE_LIMIT = 5;

    @Autowired
    private PlantService plantService;

    @Autowired
    private PlantImageService imageService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private StorageClient storage;

    @Override
    public List<PlantImage> getImages(LeafletUserDetails userDetails, Long plantId) {
        return getAllImages(getPlantFromOwner(plantId, userDetails));
    }

    @Override
    public List<PlantImage> uploadImage(LeafletUserDetails userDetails, MultipartFile file, Boolean isDefault, Long plantId) {
        String contentType = file.getContentType().toLowerCase();
        
        if (!contentType.equals("image/jpg") && !contentType.equals("image/jpeg") && !contentType.equals("image/png")) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        Plant plant = getPlantFromOwner(plantId, userDetails);
        List<PlantImage> images = imageService.getImagesFromPlant(plant);

        if (images.size() >= IMAGE_LIMIT) {
            throw new ResponseStatusException(
                HttpStatus.FORBIDDEN,
                "Unable to upload plant image due to max image limit."
            );
        }

        String url;
        try {
            url = storage.upload(file);
        } catch (StorageOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Unable to upload plant image due to an internal server error.",
                    e
            );
        }

        PlantImage image = imageService.save(map(
            url,
            plantId, 
            userDetails.getUser()
        ));

        if (isDefault || images.size() == 0) {
            updatePlantDefaultImage(image);
        }

        return getAllImages(plant);
    }

    @Override
    public List<PlantImage> setDefault(LeafletUserDetails userDetails, Long plantImageId, Long plantId) {
        Plant plant = getPlantFromOwner(plantId, userDetails);
        PlantImage plantImage = getPlantImage(plantImageId, plant);
        PlantImage defaultImage = plant.getDefaultImage();

        if (defaultImage != null && !defaultImage.getId().equals(plantImage.getId())) {
            updatePlantDefaultImage(plantImage);
        }

        return getAllImages(plant);
    }

    @Override
    public List<PlantImage> removeImage(LeafletUserDetails userDetails, Long plantImageId, Long plantId) {
        Plant plant = getPlantFromOwner(plantId, userDetails);
        PlantImage plantImage = getPlantImage(plantImageId, plant);

        deletePlantImage(plantImage);

        return getAllImages(plant);
    }

    private PlantImage getPlantImage(Long plantImageId, Plant plant) throws ResponseStatusException {
        return imageService.getPlantImageByPlant(plant, plantImageId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    private Plant getPlantFromOwner(Long plantId, LeafletUserDetails userDetails) throws ResponseStatusException {
        return plantService
            .getOwnedPlantById(userDetails.getUser(), plantId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    private List<PlantImage> getAllImages(Plant plant) {
        return imageService.getImagesFromPlant(plant).stream()
                .map(image -> {
                    image.setUrl(storage.getPublicURLMapper().apply(image.getUrl()).toString());
                    return image;
                }).collect(Collectors.toList());
    }

    private PlantImage map(String url, Long plantId, User owner) {
        Plant plant = plantService.getOwnedPlantById(owner, plantId).orElse(null);
        PlantImage plantImage = new PlantImage();
        plantImage.setUrl(url);
        plantImage.setPlant(plant);
        return plantImage;
    }

    private void updatePlantDefaultImage(PlantImage image) {
        Plant plant = image.getPlant();
        plant.setDefaultImage(image);
        plantService.save(plant);
    }

    private void deletePlantImage(PlantImage plantImage) {
        String fileName = plantImage.getUrl();
        try {
            storage.delete(fileName);
        } catch (StorageOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Unable to remove plant image due to an internal server error.",
                    e
            );
        }

        imageService.delete(plantImage);
    }
}
