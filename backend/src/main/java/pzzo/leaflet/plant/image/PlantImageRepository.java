package pzzo.leaflet.plant.image;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.plant.image.PlantImage;

@Repository
public interface PlantImageRepository extends CrudRepository<PlantImage, Long> {

    Optional<PlantImage> findByIdAndPlant(Long id, Plant plant);

    List<PlantImage> findByPlant(Plant plant);

}
