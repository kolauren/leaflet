package pzzo.leaflet.plant.image;

import java.util.List;
import java.util.Optional;

import pzzo.leaflet.plant.Plant;

public interface PlantImageService {

    Optional<PlantImage> getPlantImageByPlant(Plant plant, Long id);

    List<PlantImage> getImagesFromPlant(Plant plant);

    PlantImage save(PlantImage image);

    void delete(PlantImage image);

    void deleteAll(List<PlantImage> images);
}