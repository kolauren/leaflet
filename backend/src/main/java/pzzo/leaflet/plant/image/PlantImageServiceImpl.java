package pzzo.leaflet.plant.image;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.plant.PlantService;

@Service
public class PlantImageServiceImpl implements PlantImageService {
    @Autowired
    private PlantService plantService;

    @Autowired
    private PlantImageRepository plantImageRepository;

    @Override
    public Optional<PlantImage> getPlantImageByPlant(Plant plant, Long id) {
        return plantImageRepository.findByIdAndPlant(id, plant);
    }

    @Override
    public List<PlantImage> getImagesFromPlant(Plant plant) {
        PlantImage defaultImage = plant.getDefaultImage();
        List<PlantImage> images = plantImageRepository.findByPlant(plant);

        if (defaultImage != null) {
            Long id = defaultImage.getId();
            Collections.sort(images, (a, b) -> a.getId().equals(id) ? -1 : b.getId().equals(id) ? 1 : 0);
        }

        return images;
    }

    @Override
    public PlantImage save(PlantImage image) {
        return plantImageRepository.save(image);
    }

    @Override
    public void delete(PlantImage image) {
        Plant plant = image.getPlant();
        PlantImage defaultImage = plant.getDefaultImage();
        List<PlantImage> allImages = getImagesFromPlant(plant);

        if (defaultImage != null && defaultImage.getId().equals(image.getId()) && allImages.size() > 1) {
            allImages.stream()
                    .filter(img -> !img.getId().equals(defaultImage.getId()))
                    .findFirst().ifPresent(this::updatePlantDefaultImage);
        }

        plantImageRepository.delete(image);
    }

    @Override
    public void deleteAll(List<PlantImage> images) {
        images.stream().forEach(image -> delete(image));
    }

    private void updatePlantDefaultImage(PlantImage image) {
        Plant plant = image.getPlant();
        plant.setDefaultImage(image);
        plantService.save(plant);
    }
}
