package pzzo.leaflet.plant.journal;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;
import pzzo.leaflet.plant.journal.PlantMood;

@Data
public class CurrentUserJournalEntry {

    @JsonProperty(access = Access.READ_ONLY)
    private Long id;

    @JsonProperty(access = Access.READ_ONLY)
    private Long plantId;

    private String note = "";

    @NotNull(message = "Journal entry must include a date.")
    private LocalDate entryDate;

    private Boolean watered = false;

    private PlantMood mood;
}
