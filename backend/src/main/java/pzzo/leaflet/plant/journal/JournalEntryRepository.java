package pzzo.leaflet.plant.journal;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import pzzo.leaflet.plant.journal.PlantJournalEntry;

public interface JournalEntryRepository extends 
    CrudRepository<PlantJournalEntry, Long>,
    JpaSpecificationExecutor<PlantJournalEntry> {
}
