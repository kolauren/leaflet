package pzzo.leaflet.plant.journal;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.plant.journal.PlantJournalEntry;

public interface JournalEntryService {
    public List<PlantJournalEntry> getEntriesForPlant(Plant p);
    public List<PlantJournalEntry> getEntriesForPlantBetweenDates(Plant p, LocalDate from, LocalDate to);
    public Optional<PlantJournalEntry> getFromId(Long id);
    public PlantJournalEntry save(PlantJournalEntry entry);
}