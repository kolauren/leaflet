package pzzo.leaflet.plant.journal;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import pzzo.leaflet.plant.Plant;

@Service
public class JournalEntryServiceImpl implements JournalEntryService {

    private final JournalEntryRepository repository;

    public JournalEntryServiceImpl(JournalEntryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<PlantJournalEntry> getFromId(Long id) {
        return repository.findById(id);
    }

    @Override
    public PlantJournalEntry save(PlantJournalEntry entry) {
        return repository.save(entry);
    }

    @Override
    public List<PlantJournalEntry> getEntriesForPlant(Plant p) {
        return repository.findAll(JournalEntrySpecs.isPlant(p));
    }

    @Override
    public List<PlantJournalEntry> getEntriesForPlantBetweenDates(Plant p, LocalDate from, LocalDate to) {
        return repository.findAll(JournalEntrySpecs.isPlant(p).and(JournalEntrySpecs.isCreatedBetween(from, to)));
    }
}
