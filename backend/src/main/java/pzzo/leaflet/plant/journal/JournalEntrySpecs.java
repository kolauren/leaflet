package pzzo.leaflet.plant.journal;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.plant.journal.PlantJournalEntry;
import pzzo.leaflet.plant.journal.PlantJournalEntry_;

public class JournalEntrySpecs {

    public static Specification<PlantJournalEntry> isCreatedBetween(LocalDate fromDate, LocalDate toDate) {
        return (root, query, builder) -> builder.between(
            root.get(PlantJournalEntry_.entryDate),
            fromDate,
            toDate
        );
    }

    public static Specification<PlantJournalEntry> isPlant(Plant p) {
        return (root, query, builder) -> builder.equal(root.get(PlantJournalEntry_.plant), p);
    }

    public static Specification<PlantJournalEntry> isWatered(Boolean isWatered) {
        return (root, query, builder) -> builder.equal(root.get(PlantJournalEntry_.watered), isWatered);
    }
}
