package pzzo.leaflet.plant.journal;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import pzzo.leaflet.core.OpenAPIConfig;
import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.plant.PlantService;
import pzzo.leaflet.user.LeafletUserDetails;

@RestController
@RequestMapping("/api/me/plants/{plantId}/journal")
public class PlantJournalController {

    @Autowired
    private PlantService plantService;

    @Autowired
    private JournalEntryService journalService;

    @Autowired
    private ModelMapper modelMapper;

    @Operation(
        description = "Get journal entries for current user's plant.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @GetMapping
    public RecordsBatch<CurrentUserJournalEntry> findEntries(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long plantId,
        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> from,
        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> to
    ) {
        Plant plant = getFromOwner(plantId, userDetails);
        Boolean hasDateRange = from.isPresent() && to.isPresent();

        List<PlantJournalEntry> entries = hasDateRange ?
            journalService.getEntriesForPlantBetweenDates(plant, from.get(), to.get()) :
            journalService.getEntriesForPlant(plant);

        List<CurrentUserJournalEntry> records = entries.stream()
                .map(entry -> modelMapper.map(entry, CurrentUserJournalEntry.class))
                .collect(Collectors.toList());
        RecordsBatch<CurrentUserJournalEntry> batch = new RecordsBatch<>();
        batch.setRecords(records);
        return batch;
    }

    @Operation(
        description = "Adds a new journal entry for the current user's plant.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @PostMapping
    public CurrentUserJournalEntry createEntry(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long plantId,
        @Valid @RequestBody CurrentUserJournalEntry input
    ) {
        Plant plant = getFromOwner(plantId, userDetails);
        PlantJournalEntry entry = modelMapper.map(input, PlantJournalEntry.class);
        entry.setPlant(plant);
        return modelMapper.map(journalService.save(entry), CurrentUserJournalEntry.class);
    }

    @Operation(
        description = "Updates a journal entry for the current user's plant.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @GetMapping(path = "{entryId}")
    public CurrentUserJournalEntry getEntry(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long plantId,
        @PathVariable Long entryId
    ) {
        PlantJournalEntry entry = getPlantEntryFromOwner(entryId, plantId, userDetails);
        return modelMapper.map(entry, CurrentUserJournalEntry.class);
    }

    @Operation(
        description = "Updates a journal entry for the current user's plant.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    @PutMapping(path = "{entryId}")
    public CurrentUserJournalEntry updateEntry(
        @AuthenticationPrincipal LeafletUserDetails userDetails,
        @PathVariable Long plantId,
        @PathVariable Long entryId,
        @Valid @RequestBody CurrentUserJournalEntry input
    ) {
        PlantJournalEntry entry = getPlantEntryFromOwner(entryId, plantId, userDetails);
        entry.setMood(input.getMood());
        entry.setNote(input.getNote());
        entry.setWatered(input.getWatered());
        return modelMapper.map(journalService.save(entry), CurrentUserJournalEntry.class);
    }

    private Plant getFromOwner(Long id, LeafletUserDetails userDetails) throws ResponseStatusException {
        return plantService
            .getOwnedPlantById(userDetails.getUser(), id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    private PlantJournalEntry getPlantEntryFromOwner(Long entryId, Long plantId, LeafletUserDetails userDetails) throws ResponseStatusException {
        return journalService.getFromId(entryId)
            .map(e -> e.getPlant().getId() == plantId && e.getPlant().getOwner().getId() == userDetails.getUser().getId() ? e : null)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}