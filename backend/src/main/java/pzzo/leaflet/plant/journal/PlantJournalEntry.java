package pzzo.leaflet.plant.journal;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import pzzo.leaflet.plant.Plant;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(
    name = "journal_entry",
    uniqueConstraints = @UniqueConstraint(columnNames = {"plant_id", "entry_date"})
)
public class PlantJournalEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "entry_date", nullable = false)
    private LocalDate entryDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plant_id", nullable = false)
    private Plant plant;

    @NotNull
    private Boolean watered;

    @Enumerated(EnumType.STRING)
    private PlantMood mood;

    @NotNull
    private String note;
}