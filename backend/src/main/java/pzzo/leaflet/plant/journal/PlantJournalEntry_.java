package pzzo.leaflet.plant.journal;

import java.time.LocalDate;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import pzzo.leaflet.plant.Plant;

@StaticMetamodel(PlantJournalEntry.class)
public abstract class PlantJournalEntry_ {
    public static volatile SingularAttribute<PlantJournalEntry, Long> id;
    public static volatile SingularAttribute<PlantJournalEntry, LocalDate> entryDate;
    public static volatile SingularAttribute<PlantJournalEntry, Plant> plant;
    public static volatile SingularAttribute<PlantJournalEntry, Boolean> watered;
    public static volatile SingularAttribute<PlantJournalEntry, PlantMood> mood;
    public static volatile SingularAttribute<PlantJournalEntry, String> note;
}
