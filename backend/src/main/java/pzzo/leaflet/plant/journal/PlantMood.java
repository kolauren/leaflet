package pzzo.leaflet.plant.journal;

public enum PlantMood {
    HAPPY,
    OKAY,
    SAD,
    DEAD
}
