package pzzo.leaflet.plant.journal;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RecordsBatch<T> {
    private List<T> records;
}
