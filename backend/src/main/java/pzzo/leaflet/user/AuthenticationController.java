package pzzo.leaflet.user;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import pzzo.leaflet.user.User;
import pzzo.leaflet.user.UserRepository;

@RestController
@RequestMapping(path = "/api/auth")
public class AuthenticationController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Operation(
        description = "Sign up with a new user.",
        tags = { "authentication" }
    )
    @PostMapping(
        path = "signup",
        consumes = { MediaType.APPLICATION_JSON_VALUE }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public void signup(@Valid @RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Operation(
        description = "Login with an email and password.",
        tags = { "authentication" },
        responses = {
            @ApiResponse(
                responseCode = "204",
                description = "Successful login attempt."
            ),
            @ApiResponse(
                responseCode = "401",
                description = "Failed login attempt."
            )
        }
    )
    @PostMapping(path = "login", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public void login(
        @Schema(required = true)
        String email,
        @Schema(required = true, format = "password")
        String password
    ) {
        // Should be overridden by Spring Security. If not, then we'll return a "Not Implemented" error.
        throw new ResponseStatusException(HttpStatus.NOT_IMPLEMENTED);
    }

    @Operation(
        description = "Log out the current user.",
        tags = { "authentication" },
        responses = {
            @ApiResponse(
                // responseCode = "204",
                description = "Successful logout attempt."
            )
        }
    )
    @PostMapping(path = "logout")
    public void logout() {
        // Should be overridden by Spring Security. If not, then we'll return a "Not Implemented" error.
        throw new ResponseStatusException(HttpStatus.NOT_IMPLEMENTED);
    }

    public void changePassword() {}

    public void resetPassword() {}
}
