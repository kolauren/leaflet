package pzzo.leaflet.user;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import pzzo.leaflet.core.OpenAPIConfig;

@RestController
@RequestMapping(path = "/api/me")
public class CurrentUserController {

    @GetMapping
    @Operation(
        description = "Returns information about the current logged in user.",
        security = @SecurityRequirement(name = OpenAPIConfig.Security.USER_SESSION),
        tags = OpenAPIConfig.Tags.CURRENT_USER
    )
    public User getCurrentUser(@AuthenticationPrincipal LeafletUserDetails userDetails) {
        return userDetails.getUser();
    }
}
