package pzzo.leaflet.collection;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import pzzo.leaflet.core.ModelMapperConfig;
import pzzo.leaflet.user.User;
import pzzo.leaflet.user.LeafletUserDetails;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CurrentUserPlantCollectionController.class)
@Import(ModelMapperConfig.class)
public class CurrentUserPlantCollectionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlantCollectionService collectionService;

    private static final LeafletUserDetails testUser = new LeafletUserDetails(
            User.builder()
                    .id(1L)
                    .email("mike@leafy.net")
                    .firstName("Mike")
                    .password("test")
                    .build()
    );


    @Test
    public void testGetCollections() throws Exception {
        final User owner = testUser.getUser();
        final List<PlantCollection> collections = Arrays.asList(
                PlantCollection.builder().id(2L).name("Plant Nook").owner(owner).build(),
                PlantCollection.builder().id(3L).name("Basement").owner(owner).build()
        );

        when(collectionService.getAllFromOwner(owner)).thenReturn(collections);
        mockMvc.perform(
                get("/api/me/collections")
                .with(csrf())
                .with(user(testUser))
        ).andExpect(status().isOk()
        ).andExpect(jsonPath("$[0].keys()").value(everyItem(oneOf("id", "name")))
        ).andExpect(jsonPath("$..id").value(hasItems(2, 3))
        ).andExpect(jsonPath("$..name").value(hasItems("Plant Nook", "Basement")));
    }

    @Test
    public void testGetCollection() throws Exception {
        final Long id = 1L;
        final String name = "Plant Nook";
        final User owner = testUser.getUser();
        final PlantCollection collection = PlantCollection.builder().id(id).name(name).owner(owner).build();

        when(collectionService.getFromOwner(id, owner)).thenReturn(Optional.of(collection));
        mockMvc.perform(
                get("/api/me/collections/{id}", id)
                .with(csrf())
                .with(user(testUser))
        ).andExpect(status().isOk()
        ).andExpect(jsonPath("$.keys()").value(everyItem(oneOf("id", "name")))
        ).andExpect(jsonPath("$.id").value(id.intValue())
        ).andExpect(jsonPath("$.name").value(name));
    }

    @Test
    public void testCollectionNotFound() throws Exception {
        final long collectionId = 1L;
        final User owner = testUser.getUser();
        final PlantCollectionUpdate update = PlantCollectionUpdate.builder().name("Test").build();

        when(collectionService.getFromOwner(collectionId, owner)).thenReturn(Optional.empty());
        when(collectionService.update(collectionId, owner, update)).thenReturn(Optional.empty());

        MockHttpServletRequestBuilder[] requests = {
                get("/api/me/collections/{id}", collectionId),
                get("/api/me/collections/{id}/plants", collectionId),
                put("/api/me/collections/{id}", collectionId)
                        .content("{\"name\":\"Test\"}")
                        .contentType(MediaType.APPLICATION_JSON),
                delete("/api/me/collections/{id}", collectionId)
        };
        for (MockHttpServletRequestBuilder request : requests) {
            mockMvc.perform(request.with(csrf()).with(user(testUser))).andExpect(status().isNotFound());
        }
    }

}
