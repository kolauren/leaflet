package pzzo.leaflet.collection;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import pzzo.leaflet.collection.PlantCollection;
import pzzo.leaflet.collection.PlantCollectionRepository;
import pzzo.leaflet.user.User;

import static org.assertj.core.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
@Sql(scripts = {"classpath:plant-collection-test-data.sql"})
public class PlantCollectionRepositoryTest {
    
    @Autowired
    PlantCollectionRepository repository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testFindByOwner() {
        User owner = User.builder().id(101L).build();
        List<PlantCollection> collections = repository.findByOwner(owner);
        assertThat(collections).hasSize(2)
            .extracting("id")
            .contains(102L, 103L);
    }

    @Test
    public void testFindByOwner_noCollections() {
        User owner = User.builder().id(100L).build();
        List<PlantCollection> collections = repository.findByOwner(owner);
        assertThat(collections).isEmpty();
    }

    @Test
    public void testSave_newCollection() {
        User owner = User.builder().id(100L).build();
        PlantCollection collection = PlantCollection
            .builder()
            .name("Bathroom")
            .owner(owner)
            .build();

        PlantCollection savedCollection = repository.save(collection);
        assertThat(savedCollection.getId()).isNotNull();
    }

    @Test
    public void testSave_unlinkPlantsFromCollection() {
        Long collectionId = 102L;
        PlantCollection collection = repository.findById(collectionId).get();
        assertThat(collection.getPlants()).hasSize(2);

        collection.removeAllPlants();
        repository.save(collection);

        collection = repository.findById(collectionId).get();
        assertThat(collection.getPlants()).isEmpty();
    }

    @Test
    public void testDeleteWithPlants() {
        PlantCollection collection = repository.findById(102L).get();
        assertThat(collection.getPlants()).hasSize(2);
        assertThatThrownBy(() -> {
            repository.delete(collection);
            entityManager.flush();
        }).isInstanceOf(Exception.class);
    }

    @Test
    public void testDeleteWithNoPlants() {
        User owner = User.builder().id(100L).build();
        PlantCollection collection = PlantCollection
            .builder()
            .name("Bathroom")
            .owner(owner)
            .build();

        PlantCollection savedCollection = repository.save(collection);
        entityManager.flush();

        assertThatNoException().isThrownBy(() -> {
            repository.delete(savedCollection);
            entityManager.flush();
        });
    }
}
