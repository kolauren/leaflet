package pzzo.leaflet.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import pzzo.leaflet.plant.Plant;

import static org.assertj.core.api.Assertions.*;

public class PlantCollectionTest {
    
    @Test
    public void testRemoveAllPlants() {
        PlantCollection plantCollection = createPlantCollection();
        plantCollection.setPlants(LongStream.of(1L, 10L)
            .mapToObj(id -> Plant.builder().id(id).collection(plantCollection).build())
            .collect(Collectors.toCollection(ArrayList::new)));
        List<Plant> plants = List.copyOf(plantCollection.getPlants());

        plantCollection.removeAllPlants();

        assertThat(plantCollection.getPlants()).isEmpty();
        assertThat(plants).allMatch(p -> p.getCollection() == null);
    }

    @Test
    public void testAddPlant() {
        PlantCollection plantCollection = createPlantCollection();
        Plant plant = Plant.builder().id(1L).build();
        plantCollection.addPlant(plant);

        assertThat(plantCollection.getPlants()).hasSize(1);
        assertThat(plant.getCollection()).isEqualTo(plantCollection);
    }

    @Test
    public void testRemovePlant() {
        Plant plant = Plant.builder().id(1L).build();
        PlantCollection plantCollection = createPlantCollection();
        plantCollection.setPlants(Stream.of(plant).collect(Collectors.toCollection(ArrayList::new)));

        plantCollection.removePlant(plant);

        assertThat(plantCollection.getPlants()).isEmpty();
        assertThat(plant.getCollection()).isNull();
    }

    private PlantCollection createPlantCollection() {
        return PlantCollection.builder()
            .id(1L)
            .name("Bathroom")
            .build();
    }
}
