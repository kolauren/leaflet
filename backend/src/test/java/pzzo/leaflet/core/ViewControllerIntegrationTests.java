package pzzo.leaflet.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ActiveProfiles("test")
public class ViewControllerIntegrationTests {
    MockMvc mvc;

    @BeforeEach
    public void setup(WebApplicationContext context) {
        this.mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    public static String[][] viewControllerPathsData() {
        return new String[][]{
            {"/"}
        };
    }

    @ParameterizedTest
    @MethodSource("viewControllerPathsData")
    public void testViewControllerPaths(String path) throws Exception {
        this.mvc.perform(get(path))
            .andExpect(status().isOk())
            .andExpect(view().name("/index.html"));
    }
}
