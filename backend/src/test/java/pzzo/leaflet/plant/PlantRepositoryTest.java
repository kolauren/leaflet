package pzzo.leaflet.plant;

import java.util.Optional;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.plant.PlantRepository;
import pzzo.leaflet.user.User;

import static org.assertj.core.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
@Transactional
@Sql(scripts = {"classpath:plant-collection-test-data.sql"})
public class PlantRepositoryTest {

    @Autowired
    private PlantRepository repository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testFindByIdAndOwner() {
        User owner = User.builder().id(101L).build();
        Optional<Plant> plant = repository.findByIdAndOwner(104L, owner);
        assertThat(plant).isPresent();
        assertThat(plant.get().getId()).isEqualTo(104L);
    }
    
    @Test
    public void testFindByIdAndOwner_notOwner() {
        User owner = User.builder().id(100L).build();
        Optional<Plant> plant = repository.findByIdAndOwner(104L, owner);
        assertThat(plant).isEmpty();
    }

    @Test
    public void testSave_nonExistentOwner() {
        Plant plant = repository.findById(104L).get();
        plant.setOwner(null);
        assertThatThrownBy(() -> {
            repository.save(plant);
            entityManager.flush();
        }).isInstanceOf(Exception.class);
    }
}
