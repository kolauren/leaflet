package pzzo.leaflet.plant;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.collection.PlantCollection;
import pzzo.leaflet.plant.PlantService;
import pzzo.leaflet.plant.PlantServiceImpl;
import pzzo.leaflet.user.User;
import pzzo.leaflet.plant.PlantRepository;

public class PlantServiceImplTest {

    private static final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    private PlantService service;

    private PlantRepository repository;

    @BeforeEach
    public void setUp() {
        repository = mock(PlantRepository.class);
        service = new PlantServiceImpl(validatorFactory.getValidator(), repository);
    }

    @Test
    public void testGetPlantsForOwner() {
        User owner = User.builder().id(1L).build();
        List<Plant> plants = Stream.of(1L, 2L, 3L)
                .map(id -> Plant.builder().id(id).name("Plant " + id).owner(owner).build())
                .collect(Collectors.toList());
        when(repository.findByOwner(owner)).thenReturn(plants);
        assertThat(service.getPlantsFromOwner(owner)).isEqualTo(plants);
    }

    @Test
    public void testSave() {
        Plant plant = getTestPlant();
        Plant mockOutput = getTestPlant();
        mockOutput.setId(999L);
        when(repository.save(plant)).thenReturn(mockOutput);
        assertThat(service.save(plant)).isEqualTo(mockOutput);
        verify(repository, times(1)).save(any());
    }

    @Test
    public void testSaveWithOwnerMismatch() {
        User differentOwner = User.builder().id(2L).build();
        Plant plant = getTestPlant();
        plant.setOwner(differentOwner);
        Condition<ConstraintViolationException> constraintCondition = new Condition<>(
            exception -> exception.getConstraintViolations()
                .stream()
                .anyMatch(violation -> violation.getPropertyPath().toString().contains("collection")),
            "collectionConstraintCondition"
        );
        assertThatExceptionOfType(ConstraintViolationException.class)
            .isThrownBy(() -> service.save(plant))
            .has(constraintCondition);
        verify(repository, never()).save(any());
    }

    private Plant getTestPlant() {
        Calendar date = Calendar.getInstance();
        date.set(2020, 4, 1);
        User owner = User.builder()
            .id(1L)
            .build();
        PlantCollection collection = PlantCollection.builder()
            .id(1L)
            .name("Plant Room")
            .owner(owner)
            .build();
        return Plant.builder()
            .bornDate(date.getTime())
            .acquiredDate(date.getTime())
            .collection(collection)
            .name("Mississippi")
            .owner(owner)
            .build();
    }
}
