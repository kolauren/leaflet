package pzzo.leaflet.plant.journal;

import static org.assertj.core.api.Assertions.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import pzzo.leaflet.plant.Plant;
import pzzo.leaflet.plant.journal.JournalEntryRepository;
import pzzo.leaflet.plant.journal.JournalEntrySpecs;
import pzzo.leaflet.plant.journal.PlantJournalEntry;
import pzzo.leaflet.plant.journal.PlantMood;

@DataJpaTest
@ActiveProfiles("test")
@Sql(scripts = {"classpath:plant-collection-test-data.sql"})
public class PlantJournalEntryRepositoryTest {

    @Autowired
    private JournalEntryRepository repository;

    /**
     * Testing all the details from the SQL data corectly map to the entity object.
     */
    @Test
    public void testFindById() {
        Optional<PlantJournalEntry> entryOptional = repository.findById(112L);
        assertThat(entryOptional.isPresent()).isTrue();

        PlantJournalEntry entry = entryOptional.get();

        assertThat(entry)
            .extracting("id", "note", "watered", "mood", "entryDate")
            .contains(112L, "A watered plant is a happy plant!", true, PlantMood.HAPPY, LocalDate.of(2021, 1, 2));
        assertThat(entry.getPlant().getId()).isEqualTo(105L);
    }

    @Test
    public void testFindAllBySpecification() {
        Plant p = Plant.builder().id(104L).build();
        List<PlantJournalEntry> plants = repository.findAll(
            JournalEntrySpecs
                .isPlant(p)
                .and(JournalEntrySpecs.isWatered(true))
        );
        assertThat(plants).extracting("id").containsExactlyInAnyOrder(109L);
    }
}