package pzzo.leaflet.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import javax.transaction.Transactional;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class AuthenticationIntegrationTests {
    private MockMvc mvc;

    @BeforeEach
    public void setup(WebApplicationContext context) {
        this.mvc = MockMvcBuilders.webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Test
    public void testSignupAndLogin() throws Exception {
        mvc.perform(post("/api/auth/signup")
            .with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"firstName\":\"John\",\"email\":\"john@john.com\",\"password\":\"jon\"}")
        ).andExpect(status().isCreated());

        mvc.perform(formLogin("/api/auth/login")
            .user("email", "john@john.com")
            .password("jon")
        ).andExpect(authenticated().withUsername("john@john.com"));
    }

    @Test
    public void testLoginBadCredentials() throws Exception {
        mvc.perform(formLogin("/api/auth/login")
            .user("email", "john@john.com")
            .password("badpassword")
        ).andExpect(unauthenticated());
    }

    @Test
    public void testLogout() throws Exception {
        mvc.perform(logout("/api/auth/logout")).andExpect(status().is2xxSuccessful());
    }
}
