INSERT INTO leaflet_user (id, email, first_name, password) VALUES
    (100, 'mike@leafy.net', 'Mike', ''),
    (101, 'lauren@leafy.net', 'Lauren', '');

INSERT INTO plant_collection (id, name, owner_id) VALUES
    (102, 'Living Room', 101),
    (103, 'Kitchen', 101);

INSERT INTO plant (id, name, born_date, acquired_date, collection_id, owner_id) VALUES
    (104, 'Idaho', '2018-12-30', '2020-04-02', 102, 101),
    (105, 'Missouri', '2018-12-30', '2020-04-02', 102, 101),
    (106, 'Washington', '2018-12-30', '2020-04-02', 103, 101),
    (107, 'North Dakota', '2018-12-30', '2020-04-02', 103, 101),
    (108, 'Vermont', '2018-12-30', '2020-04-02', NULL, 101);

INSERT INTO journal_entry (id, entry_date, plant_id, note, watered, mood) VALUES
    (109, '2021-01-02', 104, 'I watered him.', 1, NULL),
    (111, '2021-01-03', 104, '', 0, 'OKAY'),
    (112, '2021-01-02', 105, 'A watered plant is a happy plant!', 1, 'HAPPY');