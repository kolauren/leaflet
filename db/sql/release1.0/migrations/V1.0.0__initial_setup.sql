create sequence hibernate_sequence start 1 increment 1;

create table journal_entry (
    id int8 not null,
    entry_date date not null,
    mood varchar(255),
    note varchar(255) not null,
    watered boolean not null,
    plant_id int8 not null,
    primary key (id)
);

create table leaflet_user (
    id int8 not null,
    email varchar(255),
    first_name varchar(255),
    password varchar(255),
    primary key (id)
);

create table plant (
    id int8 not null,
    acquired_date date,
    born_date date,
    name varchar(255) not null,
    classification_id int8,
    collection_id int8,
    default_image_id int8,
    owner_id int8 not null,
    primary key (id)
);

create table plant_classification (
    id int8 not null,
    common_name varchar(255),
    scientific_name varchar(255) not null,
    primary key (id)
);

create table plant_collection (
    id int8 not null,
    name varchar(255) not null,
    owner_id int8 not null,
    primary key (id)
);

create table plant_image (
    id int8 not null,
    url varchar(255) not null,
    plant_id int8 not null,
    primary key (id)
);

alter table if exists journal_entry
    add constraint UK59yh7g8s1b1otur11d50igufp unique (plant_id, entry_date);

alter table if exists leaflet_user
    add constraint UK_qw9uecwfuhsuoo5mye1qrne0s unique (email);

alter table if exists journal_entry
    add constraint FKgyalj060wprr46vnjhe3gi9v8
    foreign key (plant_id)
    references plant;

alter table if exists plant
    add constraint FK1fd7rdsgeto1wxjed61j2krva
    foreign key (classification_id)
    references plant_classification;

alter table if exists plant
    add constraint FKchry4uktr62en0r6pdslj80ag
    foreign key (collection_id)
    references plant_collection;

alter table if exists plant
    add constraint FK8tfdyltsg4e658l02jr5l0g6j
    foreign key (default_image_id)
    references plant_image;

alter table if exists plant
    add constraint FKgftb924tni5356agmwn1rdgmc
    foreign key (owner_id)
    references leaflet_user;

alter table if exists plant_collection
    add constraint FKu89dsrfmvat4ksryc1fty029
    foreign key (owner_id)
    references leaflet_user;

alter table if exists plant_image
    add constraint FKna7jbtxqelkxpc3ifx6m3yrr3
    foreign key (plant_id)
    references plant;
