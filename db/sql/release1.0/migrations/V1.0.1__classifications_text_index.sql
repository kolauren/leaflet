create index classifications_text_index
    on plant_classification
    using gin(to_tsvector('english', common_name || ' ' || scientific_name));