import { ApiService } from "api/shared/service";
import { AxiosResponse } from "axios";
import { LoginForm, SignUpForm } from "./models";

function jsonToFormData(form: { [field: string]: string}): FormData {
    const formData = new FormData();
    for (const field in form) {
        formData.append(field, form[field]);
    }
    return formData;
}

const AUTH_API = "/auth/";

export const AuthApi = {
    login: (form: LoginForm): Promise<AxiosResponse<string>> => {
        return ApiService.http.post(`${AUTH_API}login`, jsonToFormData(form), {
            headers: { "Content-Type": "multipart/form-data" }
        });
    },
    logout: (): Promise<AxiosResponse<string>> => ApiService.http.post(`${AUTH_API}logout`),
    signup: (form: SignUpForm): Promise<AxiosResponse<string>> => ApiService.http.post(`${AUTH_API}signup`, form)
}