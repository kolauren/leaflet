export interface Form {
    [field: string]: string;
}

export interface LoginForm extends Form {
    email: string;
    password: string;
}

export interface SignUpForm extends Form {
    email: string;
    firstName: string;
    password: string;
}