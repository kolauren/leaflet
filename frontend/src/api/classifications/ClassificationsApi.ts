import { ApiService } from "api/shared/service";
import { AxiosResponse } from "axios";
import { Classification } from ".";

export const CLASSIFICATIONS_API = "/classifications";

export const ClassificationsApi = {
    get: (query: string): Promise<AxiosResponse<Classification[]>> => {
        return ApiService.http.get(`${CLASSIFICATIONS_API}`, { params: { query }});
    }
}