export interface Classification {
    id: number;
    scientificName: string;
    commonName?: string;
}