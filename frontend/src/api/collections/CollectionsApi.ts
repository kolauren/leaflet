import { ApiService } from "api/shared/service";
import { AxiosResponse } from "axios";
import { Collection } from ".";

export const COLLECTIONS_API = "/me/collections";

export const CollectionsApi = {
    get: (): Promise<AxiosResponse<Collection[]>> => {
        return ApiService.http.get(`${COLLECTIONS_API}`);
    },
    add: (name: string): Promise<AxiosResponse<Collection>> => {
        return ApiService.http.post(`${COLLECTIONS_API}`, {
            name
        });
    },
    edit: (room: Collection): Promise<AxiosResponse<Collection>> => {
        return ApiService.http.put(`${COLLECTIONS_API}/${room.id}`, {
            name: room.name
        });
    },
    delete: (room: Collection): Promise<AxiosResponse<Collection>> => {
        return ApiService.http.delete(`${COLLECTIONS_API}/${room.id}`);
    }
}