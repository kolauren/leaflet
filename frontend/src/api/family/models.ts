export interface Family {
    name: string;
    amount: number;
    location: string;
}