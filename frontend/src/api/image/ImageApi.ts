import { ApiService } from "api/shared/service";
import { AxiosResponse } from "axios";
import { PlantImage } from ".";

export const PLANTS_API = "/me/plants";
export const IMAGE_API_SUFFIX = "images";

export const ImageApi = {
    getAll: (plantId: number): Promise<AxiosResponse<PlantImage[]>> => ApiService.http.get(`${PLANTS_API}/${plantId}/${IMAGE_API_SUFFIX}`),
    add: (plantId: number, file: File, isDefault = false): Promise<AxiosResponse<PlantImage[]>> => {
        const form = new FormData();
        form.append("profile", "true");
        form.append("file", file);
        form.append("isDefault", isDefault.toString());
        return ApiService.http.post(`${PLANTS_API}/${plantId}/${IMAGE_API_SUFFIX}`, form, {
            headers: {
                "Content-Type": "multipart/form-data"
            }
        });
    },
    delete: (plantId: number, imageId: number): Promise<AxiosResponse<PlantImage[]>> => ApiService.http.delete(`${PLANTS_API}/${plantId}/${IMAGE_API_SUFFIX}/${imageId}`),
    setDefault: (plantId: number, imageId: number): Promise<AxiosResponse<PlantImage[]>> => ApiService.http.put(`${PLANTS_API}/${plantId}/${IMAGE_API_SUFFIX}/${imageId}/default`)
}