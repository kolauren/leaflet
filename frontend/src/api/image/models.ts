export interface PlantImage {
    id: number;
    url: string;
}