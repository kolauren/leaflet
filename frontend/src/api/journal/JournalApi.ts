import { PLANTS_API } from "api/plant";
import { ApiService } from "api/shared/service";
import { AxiosResponse } from "axios";
import { JournalEntry, JournalEntryForm, JournalSummary } from ".";

const JOURNAL_API = "journal";

export const JournalApi = {
    getEntries: (plantId: number, from: string, to: string): Promise<AxiosResponse<JournalSummary>> => {
        return ApiService.http.get(`${PLANTS_API}/${plantId}/${JOURNAL_API}`, { 
            params: {
                from,
                to
            }
        });
    },
    getEntry: (plantId: number, entryId: number): Promise<AxiosResponse<JournalEntry>> => 
        ApiService.http.get(`${PLANTS_API}/${plantId}/${JOURNAL_API}/${entryId}`),
    addEntry: (plantId: number, entry: JournalEntryForm): Promise<AxiosResponse<JournalEntry>> => {
        return ApiService.http.post(`${PLANTS_API}/${plantId}/${JOURNAL_API}`, entry);
    },
    updateEntry: (plantId: number, entry: JournalEntry): Promise<AxiosResponse<JournalEntry>> => {
        return ApiService.http.put(`${PLANTS_API}/${plantId}/${JOURNAL_API}/${entry.id}`, entry);
    }
}