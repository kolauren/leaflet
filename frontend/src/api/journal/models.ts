export interface JournalSummary {
    records: JournalEntry[];
}

export interface JournalEntryBase {
    note?: string;
    entryDate: string;
    watered?: boolean;
    mood?: EntryMood;
}

export interface JournalEntry extends JournalEntryBase {
    id: number;
    plantId: number;
}

export interface JournalEntryForm extends JournalEntryBase {
}

export enum EntryMood {
    HAPPY = "HAPPY",
    OKAY = "OKAY",
    SAD = "SAD",
    DEAD = "DEAD",
    NA = "N/A"
}