import { ApiService } from "api/shared/service";
import { AxiosResponse } from "axios";
import { Plant, PlantForm } from ".";

export const PLANTS_API = "/me/plants";

export const PlantApi = {
    getAll: (): Promise<AxiosResponse<Plant[]>> => ApiService.http.get(PLANTS_API),
    get: (id: number): Promise<AxiosResponse<Plant>> => ApiService.http.get(`${PLANTS_API}/${id}`),
    add: (form: PlantForm): Promise<AxiosResponse<Plant>> => ApiService.http.post(PLANTS_API, form),
    edit: (plant: PlantForm): Promise<AxiosResponse<Plant>> => ApiService.http.put(`${PLANTS_API}/${plant.id}`, plant),
    delete: (id: number): Promise<AxiosResponse<Plant>> => ApiService.http.delete(`${PLANTS_API}/${id}`)
}