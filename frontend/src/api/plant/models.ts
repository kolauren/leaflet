import { Classification } from "api/classifications";
import { Collection } from "api/collections";
import { PlantImage } from "api/image";

interface PlantBase {
    name: string;
    species?: string;
    bornDate: string;
    acquiredDate: string;
}

export interface Plant extends PlantBase {
    id: number;
    collection?: Collection;
    classification?: Classification;
    defaultImage?: PlantImage;
}

export interface PlantForm extends PlantBase {
    collectionId?: number;
    classificationId?: number;
    id?: number;
}