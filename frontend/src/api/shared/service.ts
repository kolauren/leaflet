import { COLLECTIONS_API } from "api/collections";
import { IMAGE_API_SUFFIX } from "api/image";
import { PLANTS_API } from "api/plant";
import Axios, { AxiosError, AxiosInstance, AxiosRequestConfig } from "axios";
import { IAxiosCacheAdapterOptions, setupCache } from "axios-cache-adapter";

const API_URL = "/api";

function getCookieValue(name: string): string | undefined {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    return (parts.length === 2) ? (parts.pop() || "").split(";").shift() : undefined;
}

function parseErrorMessage(error: AxiosError<{ message: string }>): string {
    if (error && error.response) {
        const { response } = error
        return `${response.status} - ${(response.data && response.data.message ? response.data.message : response.statusText)}`;
    }
    return "Something went wrong";
}

class ApiService {
    private static singleton: ApiService;

    private axiosInstance: AxiosInstance;

    constructor() {
        if (ApiService.singleton) {
            throw new Error("ApiService singleton already exists");
        }

        const cache = setupCache({
            maxAge: 5 * 60 * 1000,

            // TODO: fix mismapped typings
            invalidate: async (cfg: IAxiosCacheAdapterOptions, req: AxiosRequestConfig) => {
                const method = req.method!.toLowerCase();
                const uuid = (cfg as any).uuid;
                const store = (cfg.store as any);

                if (method !== "get") {
                    if (req.url?.includes(COLLECTIONS_API)) {
                        store.removeItem(PLANTS_API);
                        store.removeItem(COLLECTIONS_API);
                    }

                    if (req.url?.includes(IMAGE_API_SUFFIX) || req.url?.includes(PLANTS_API)) {
                        store.removeItem(PLANTS_API);
                    }

                    if (req.url?.includes(IMAGE_API_SUFFIX)) {
                        let imageUrlParts = uuid.split("/");
                        let imageIndex = imageUrlParts.indexOf(IMAGE_API_SUFFIX);
                        imageUrlParts = imageUrlParts.slice(0, imageIndex + 1);

                        store.removeItem(imageUrlParts.join("/"));
                    }

                    await store.removeItem(uuid);
                }
              }
        });

        this.axiosInstance = Axios.create({
            baseURL: `${API_URL}`,
            headers: {
                "X-XSRF-TOKEN": getCookieValue("XSRF-TOKEN")
            },
            adapter: cache.adapter
        });

        this.axiosInstance.interceptors.response.use(response => response, error => Promise.reject(parseErrorMessage(error)));
    }

    public static get instance(): ApiService {
        if (!ApiService.singleton) {
            ApiService.singleton = new ApiService();
        }

        return ApiService.singleton;
    }

    public get http(): AxiosInstance {
        return this.axiosInstance;
    }
}

const instance = ApiService.instance;
export { instance as ApiService };
