import { ApiService } from "api/shared/service";
import { AxiosResponse } from "axios";

export const USER_API = "/me";

export const UserApi = {
    getCurrent: (): Promise<AxiosResponse<any>> => ApiService.http.get(USER_API)
}