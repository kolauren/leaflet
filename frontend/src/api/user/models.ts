export interface User {
    email: string;
    firstName: string;
    id: number;
}