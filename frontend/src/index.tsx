import React, { CSSProperties } from "react";
import ReactDOM from "react-dom";
import ReactModal from "react-modal";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import configureStore from "state/shared/configureStore";
import { App } from "view/app";

const MODAL_OVERLAY_STYLES: CSSProperties = {
  position: "fixed",
  inset: 0,
  zIndex: 200,
  backgroundColor: "rgba(0, 0, 0, 0.6)"
};

const store = configureStore();

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);

ReactModal.setAppElement("#root");
ReactModal.defaultStyles.overlay = MODAL_OVERLAY_STYLES;