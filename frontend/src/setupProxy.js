// This adds a middleware to the `react-scripts start` server that proxies all API requests
// to a running back-end service. That way, the front-end can still be developed as if
// it were part of the full environment, but still get automatically recompiled by the 
// react-scripts toolsets.
//
const { createProxyMiddleware } = require("http-proxy-middleware");

const host = process.env.REACT_APP_PROXY_HOST || "localhost:8080";

module.exports = function(app) {
    app.use(
        "/api",
        createProxyMiddleware({
            target: `http://${host}`,
            changeOrigin: true
        })
    );
};