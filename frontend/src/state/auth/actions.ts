import { LoginForm, SignUpForm } from "api/auth";
import { AuthApi } from "api/auth/AuthApi";
import { ThunkDispatch } from "redux-thunk";
import { ActionType } from "state/shared";

export const LOGIN_ERROR_ACTION = "LOGIN_ERROR";

export interface LoginErrorAction {
    type: typeof LOGIN_ERROR_ACTION;
    payload?: string;
}

export function login(form: LoginForm): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<LoginErrorAction|void> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return AuthApi.login(form).then(() => {
            window.location.pathname = "/plants";
        }, error => dispatch({
            type: LOGIN_ERROR_ACTION,
            payload: error
        }));
    }
}

export function logout(): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<LoginErrorAction|void> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return AuthApi.logout().then(() => {
            window.location.pathname = "/";
        }, error => dispatch({
            type: LOGIN_ERROR_ACTION,
            payload: error
        }));
    }
}

export function signup(form: SignUpForm): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<LoginErrorAction|void> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return AuthApi.signup(form).then(() => {
            window.location.pathname = "/plants";
        }, error => dispatch({
            type: LOGIN_ERROR_ACTION,
            payload: error
        }));
    }
}

export function clearError(): LoginErrorAction {
    return {
        type: LOGIN_ERROR_ACTION
    };
}