import { Classification, ClassificationsApi } from "api/classifications";
import { ThunkDispatch } from "redux-thunk";
import { ActionType } from "state/shared";

export const RECEIVE_CLASSIFICATIONS_ACTION = "RECEIVE_CLASSIFICATIONS";

export interface ReceiveClassificationsAction {
    type: typeof RECEIVE_CLASSIFICATIONS_ACTION;
    payload: Classification[];
}

export function fetchClassifications(
    query: string
): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveClassificationsAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        if (!query) {
            return Promise.resolve(dispatch({
                type: RECEIVE_CLASSIFICATIONS_ACTION,
                payload: []
            }));
        }
        
        return ClassificationsApi.get(query)
            .then(response => dispatch({
                type: RECEIVE_CLASSIFICATIONS_ACTION,
                payload: response.data
            }));
    }
}