import { ActionType } from "state/shared";
import { RECEIVE_CLASSIFICATIONS_ACTION } from "./actions";

export function classifications(state = [], action: ActionType) {
    if (action.type !== RECEIVE_CLASSIFICATIONS_ACTION) {
        return state;
    }

    return action.payload;
}