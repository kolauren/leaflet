import { LOGIN_ERROR_ACTION } from "state/auth";
import { IMAGES_ERROR_ACTION } from "state/image";
import { PLANT_ERROR_ACTION } from "state/plant";
import { ActionType } from "state/shared";

export function errors(state = {}, action: ActionType) {
    switch(action.type) {
        case LOGIN_ERROR_ACTION:
            return {
                ...state,
                login: action.payload
            };
        case PLANT_ERROR_ACTION: 
            return {
                ...state,
                plant: action.payload
            }
        case IMAGES_ERROR_ACTION:
            return {
                ...state,
                images: action.payload
            }
        default:
            return state;
    }
}