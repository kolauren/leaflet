import { ImageApi, PlantImage } from "api/image";
import { ThunkDispatch } from "redux-thunk";
import { ActionType } from "state/shared";

export const RECEIVE_IMAGES_ACTION = "RECEIVE_IMAGES";
export const LOADING_IMAGES_ACTION = "LOADING_IMAGES";
export const IMAGES_ERROR_ACTION = "IMAGES_ERROR_ACTION";

export interface ReceiveImagesAction {
    type: typeof RECEIVE_IMAGES_ACTION;
    payload: PlantImage[];
}

export interface LoadingImagesAction {
    type: typeof LOADING_IMAGES_ACTION;
    loading: boolean;
}

export interface ImagesErrorAction {
    type: typeof IMAGES_ERROR_ACTION;
    payload?: string;
}

export function getPlantImages(plantId: number): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveImagesAction | ImagesErrorAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        dispatch({ 
            type: LOADING_IMAGES_ACTION,
            loading: true
        });
        return ImageApi.getAll(plantId)
            .then(response => dispatch({
                type: RECEIVE_IMAGES_ACTION,
                payload: response.data
            }), error => dispatch({
                type: IMAGES_ERROR_ACTION,
                payload: error
            }));
    }
}

export function uploadPlantImage(
    plantId: number, 
    file: File,
    isDefault = false
): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveImagesAction | ImagesErrorAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        dispatch({ 
            type: LOADING_IMAGES_ACTION,
            loading: true
        });
        return ImageApi.add(plantId, file, isDefault)
            .then(response => dispatch({
                type: RECEIVE_IMAGES_ACTION,
                payload: response.data
            }), error => dispatch({
                type: IMAGES_ERROR_ACTION,
                payload: error
            }));
    }
}

export function deletePlantImage(
    plantId: number, 
    imageId: number
): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveImagesAction | ImagesErrorAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        dispatch({ 
            type: LOADING_IMAGES_ACTION,
            loading: true
        });
        return ImageApi.delete(plantId, imageId)
            .then(response => dispatch({
                type: RECEIVE_IMAGES_ACTION,
                payload: response.data
            }), error => dispatch({
                type: IMAGES_ERROR_ACTION,
                payload: error
            }));
    }
}

export function setDefaultPlantImage(
    plantId: number, 
    imageId: number
): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveImagesAction | ImagesErrorAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        dispatch({ 
            type: LOADING_IMAGES_ACTION,
            loading: true
        });
        return ImageApi.setDefault(plantId, imageId)
            .then(response => dispatch({
                type: RECEIVE_IMAGES_ACTION,
                payload: response.data
            }), error => dispatch({
                type: IMAGES_ERROR_ACTION,
                payload: error
            }));
    }
}

export function clearImageErrors(): (dispatch: ThunkDispatch<any, any, ActionType>) => ImagesErrorAction {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        dispatch({ 
            type: LOADING_IMAGES_ACTION,
            loading: false
        });
    
        return {
            type: IMAGES_ERROR_ACTION
        };
    }
}