import { ActionType } from "state/shared";
import { RECEIVE_IMAGES_ACTION } from ".";
import { LOADING_IMAGES_ACTION } from "./actions";

export function plantImages(state = { loading: false, images: [] }, action: ActionType) {
    switch(action.type) {
        case LOADING_IMAGES_ACTION: 
            return { 
                loading: action.loading,
                images: state.images 
            };
        case RECEIVE_IMAGES_ACTION: 
            return { 
                loading: false, 
                images: action.payload 
            };
        default:
            return state;
    }
}
