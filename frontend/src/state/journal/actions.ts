import { JournalApi, JournalEntry, JournalEntryForm } from "api/journal";
import { ThunkDispatch } from "redux-thunk";
import { ActionType } from "state/shared";
import { formatDateForApi } from "view/utils";

export const RECEIVE_JOURNAL_ENTRY_ACTION = "RECEIVE_JOURNAL_ENTRY";
export const RECEIVE_JOURNAL_SUMMARY_ACTION = "RECEIVE_JOURNAL_SUMMARY";

export interface ReceiveJournalEntry {
    type: typeof RECEIVE_JOURNAL_ENTRY_ACTION;
    payload: JournalEntry;
}

export interface ReceiveJournalSummary {
    type: typeof RECEIVE_JOURNAL_SUMMARY_ACTION;
    payload: JournalEntry[];
}

export function fetchJournalEntries(
    plantId: number, 
    from: Date, 
    to: Date
): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveJournalSummary> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        const fromDate = formatDateForApi(from);
        const toDate = formatDateForApi(to);
        return JournalApi.getEntries(plantId, fromDate, toDate)
            .then(response => dispatch({
                type: RECEIVE_JOURNAL_SUMMARY_ACTION,
                payload: response.data ? response.data.records : []
            }));
    }
} 

export function fetchJournalEntry(
    plantId: number, 
    entryId: number
): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveJournalEntry> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return JournalApi.getEntry(plantId, entryId)
            .then(response => dispatch({
                type: RECEIVE_JOURNAL_ENTRY_ACTION,
                payload: response.data
            }));
    }
} 

export function addJournalEntry(
    plantId: number, 
    entry: JournalEntryForm
): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveJournalEntry> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return JournalApi.addEntry(plantId, entry)
            .then(response => dispatch({
                type: RECEIVE_JOURNAL_ENTRY_ACTION,
                payload: response.data
            }));
    }
}

export function updateJournalEntry(
    plantId: number, 
    entry: JournalEntry
): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceiveJournalEntry> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return JournalApi.updateEntry(plantId, entry)
            .then(response => dispatch({
                type: RECEIVE_JOURNAL_ENTRY_ACTION,
                payload: response.data
            }));
    }
}