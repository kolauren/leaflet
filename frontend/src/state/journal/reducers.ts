import { JournalEntry } from "api/journal";
import { ActionType, JournalState } from "state/shared";
import { RECEIVE_JOURNAL_ENTRY_ACTION, RECEIVE_JOURNAL_SUMMARY_ACTION } from "./actions";

function entry(state = {}, action: ActionType) {
    if (action.type !== RECEIVE_JOURNAL_ENTRY_ACTION) {
        return { ...state };
    }
    return action.payload;
}

function entries(state: JournalEntry[] = [], action: ActionType) {
    if (action.type === RECEIVE_JOURNAL_ENTRY_ACTION) {
        const entry = action.payload;
        return [
            entry,
            ...state.filter(s => s.entryDate !== entry.entryDate)
        ];
    }

    if (action.type !== RECEIVE_JOURNAL_SUMMARY_ACTION) {
        return [ ...state ];
    }

    return action.payload;
}

export function journal(state: JournalState = {}, action: ActionType) {
    return {
        entry: entry(state.entry, action),
        entries: entries(state.entries, action)
    };
}