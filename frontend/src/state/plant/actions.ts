import { Plant, PlantApi, PlantForm } from "api/plant";
import { ThunkDispatch } from "redux-thunk";
import { ActionType } from "state/shared";

export const PLANT_ERROR_ACTION = "PLANT_ERROR_ACTION";
export const RECEIVE_PLANTS_ACTION = "RECEIVE_PLANTS";
export const RECEIVE_PLANT_DETAILS_ACTION = "RECEIVE_PLANT_DETAILS";
export const PLANT_FORM_ERROR_ACTION = "PLANT_FORM_ERROR";

export interface ReceivePlantsAction {
    type: typeof RECEIVE_PLANTS_ACTION;
    payload: Plant[];
}

export interface ReceivePlantDetailsAction {
    type: typeof RECEIVE_PLANT_DETAILS_ACTION;
    payload: Plant | {};
}

export interface PlantFormErrorAction {
    type: typeof PLANT_FORM_ERROR_ACTION;
    payload: string;
}

export interface PlantErrorAction {
    type: typeof PLANT_ERROR_ACTION;
    payload: string;
}

export function fetchPlants(): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceivePlantsAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return PlantApi.getAll()
            .then(response => dispatch({
                type: RECEIVE_PLANTS_ACTION,
                payload: response.data
            }));
    }
}

export function fetchPlantDetails(id: number): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ReceivePlantDetailsAction|PlantErrorAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return PlantApi.get(id)
            .then(response => dispatch({
                type: RECEIVE_PLANT_DETAILS_ACTION,
                payload: response.data
            }), error => dispatch({
                type: PLANT_ERROR_ACTION,
                payload: error
            }));
    }
}

export function clearPlantDetails(): ReceivePlantDetailsAction {
    return {
        type: RECEIVE_PLANT_DETAILS_ACTION,
        payload: {}
    }
}

export function addPlant(plant: PlantForm, history: any): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<PlantFormErrorAction | void> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return PlantApi.add(plant)
            .then(() => history.push("/plants"), error => dispatch({
                type: PLANT_FORM_ERROR_ACTION,
                payload: error
            }));
    }
}

export function editPlant(plant: PlantForm, history: any): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<PlantFormErrorAction | void> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return PlantApi.edit(plant)
            .then(() => history.push(`/plants/${plant.id}`), error => dispatch({
                type: PLANT_FORM_ERROR_ACTION,
                payload: error
            }));
    }
}

export function deletePlant(id: number, history: any): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<PlantFormErrorAction | void> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return PlantApi.delete(id)
            .then(() => history.push("/plants"), error => dispatch({
                type: PLANT_FORM_ERROR_ACTION,
                payload: error
            }));
    }
}
