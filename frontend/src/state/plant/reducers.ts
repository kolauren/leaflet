import { Plant } from "api/plant";
import { ActionType } from "state/shared";
import { RECEIVE_PLANTS_ACTION, RECEIVE_PLANT_DETAILS_ACTION } from "./actions";

function getPlantsByRoom(plants: Plant[]): { [id: number]: Plant[] } {
    return plants.reduce((acc, plant) => {
        const collectionId = plant.collection ? plant.collection.id : -1;
        if (!acc[collectionId]) {
            acc[collectionId] = [];
        } 
        acc[collectionId].push(plant);
        return acc;
    }, {} as { [id: number]: Plant[] });
}

export function plants(state = {}, action: ActionType) {
    if (action.type !== RECEIVE_PLANTS_ACTION) {
        return state;
    }

    const { payload } = action;

    return { 
        all: payload,
        byRoom: getPlantsByRoom(action.payload) 
    };
}

export function plantDetails(state = {}, action: ActionType) {
    return action.type === RECEIVE_PLANT_DETAILS_ACTION 
        ? action.payload
        : state;
}