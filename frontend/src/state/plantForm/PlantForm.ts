import { ValidatorFn } from "./Validators";

export class LeafletForm {
    /**
     * Parse the form state from the elements
     * @param form HTML form
     * @param formFields The fields that we want to keep track of
     */
    public static getFormState(form: HTMLFormElement, formFields: string[]): FormState {
        const formElements = form.elements;
        const formState: FormState = {};
        for (let name of formFields) {
            const input = formElements.namedItem(name) as HTMLInputElement;

            if (input) {
                formState[name] = input.type === "number" ? (input.value ? parseInt(input.value, 10) : 0) : input.value;
            }
        }
        return formState;
    }

    /**
     * Create a new form for validating state values
     * 
     * @param _validators List of form field to validators required
     * @param _formValidators List of custom validators that check the entire form (e.g. multiple form fields)
     */
    constructor(
        private readonly _validators: LeafletFormValidators, 
        private readonly _formValidators: LeafletFormValidators = {}
    ) {}

    /**
     * Return a list of errors if this form has any
     * 
     * @param formState The current form state
     */
    public getErrors(formState: FormState): ErrorState {
        const errorState: ErrorState = {};

        for (let formField in formState) {
            const validators = this._validators[formField];
            if (validators) { 
                errorState[`${formField}Error`] = !(validators.reduce((isValid, validator) => isValid && validator(formState[formField]), true));
            }
        }

        const { _formValidators } = this;
        for (let formField in _formValidators) {
            errorState[`${formField}Error`] = !(_formValidators[formField].reduce((isValid, validator) => isValid && validator(formState), true));
        }

        return errorState;
    }

    /**
     * Check if this form is valid based on the current set of validators
     * 
     * @param formState The current form state
     */
    public isValid(formState: FormState): boolean {
        return Object.values(this.getErrors(formState)).reduce((acc, hasError) => acc && !hasError, true);
    }
}

type LeafletFormValidators = { [formFieldName: string]: ValidatorFn[] };

export type FormState = { [formFieldName: string]: any };

export type ErrorState = { [formFieldName: string]: boolean }