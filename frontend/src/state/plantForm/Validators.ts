export class Validators {
    public static required(value: any): boolean {
        if (typeof value === "string") {
            return !!value.length;
        }
        return typeof value !== "undefined" && value !== null;
    }
}

export interface ValidatorFn {
    (value: any): boolean;
}