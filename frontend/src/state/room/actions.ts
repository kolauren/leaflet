import { Collection, CollectionsApi } from "api/collections";
import { ThunkDispatch } from "redux-thunk";
import { ActionType, ModifyRoomsState } from "state/shared";

export const PLANTS_FILTER_BY_ROOM_ACTION = "PLANTS_FILTER_BY_ROOM_ACTION";
export const UPDATE_ROOMS_ACTION = "UPDATE_ROOMS_ACTION";
export const MODIFY_ROOMS_ACTION = "MODIFY_ROOMS_ACTION";
export const GET_ROOMS_ACTION = "GET_ROOMS_ACTION";

export const ALL_PLANTS_FILTER_STATE = {
    id: -1,
    name: "All Plants"
};

export interface PlantsFilterByRoomAction {
    type: typeof PLANTS_FILTER_BY_ROOM_ACTION;
    payload: Collection;
}

export interface UpdateRoomsAction {
    type: typeof UPDATE_ROOMS_ACTION;
    payload: Collection
}

export interface ModifyRoomsAction {
    type: typeof MODIFY_ROOMS_ACTION;
    payload: ModifyRoomsState;
}

export interface GetRoomsAction {
    type: typeof GET_ROOMS_ACTION;
    payload: Collection[];
}

function toggleModifyRooms(state: ModifyRoomsState): ModifyRoomsAction {
    return {
        type: MODIFY_ROOMS_ACTION,
        payload: state
    }
}

export function showAddRoom(): ModifyRoomsAction {
    return toggleModifyRooms({ add: true, edit: false, loading: false });
}

export function showEditRoom(): ModifyRoomsAction {
    return toggleModifyRooms({ add: false, edit: true, loading: false });
}

export function showLoadingRoom(): ModifyRoomsAction {
    return toggleModifyRooms({ add: false, edit: false, loading: true });
}

export function hideAllRoomActions(): ModifyRoomsAction {
    return toggleModifyRooms({ add: false, edit: false, loading: false });
}

export function addRoom(room: string): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ActionType> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return CollectionsApi.add(room).then(response => {
            dispatch({
                type: UPDATE_ROOMS_ACTION,
                payload: response.data
            });
            return dispatch(getRooms())
        })
    }
}

export function editRoom(room: Collection): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ActionType> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return CollectionsApi.edit(room).then(response => {
            dispatch({
                type: UPDATE_ROOMS_ACTION,
                payload: response.data
            });
            return dispatch(getRooms());
        })
    }
}

export function deleteRoom(room: Collection): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<ActionType> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return CollectionsApi.delete(room).then(response => {
            dispatch({
                type: UPDATE_ROOMS_ACTION,
                payload: response.data
            });
            dispatch(getRooms());
            return dispatch(filterPlantsByRoom(ALL_PLANTS_FILTER_STATE));
        })
    }
}

export function getRooms(): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<GetRoomsAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return CollectionsApi.get().then(response => dispatch({
            type: GET_ROOMS_ACTION,
            payload: response.data
        }))
    }
}

export function filterPlantsByRoom(room: Collection): PlantsFilterByRoomAction {
    return {
        type: PLANTS_FILTER_BY_ROOM_ACTION,
        payload: room
    }
}