import { ActionType } from "state/shared";
import { GET_ROOMS_ACTION, PLANTS_FILTER_BY_ROOM_ACTION } from ".";
import { ALL_PLANTS_FILTER_STATE, MODIFY_ROOMS_ACTION } from "./actions";

export function currentRoom(state = ALL_PLANTS_FILTER_STATE, action: ActionType) {
    return action.type === PLANTS_FILTER_BY_ROOM_ACTION
        ? action.payload
        : state
}

export function rooms(state = [], action: ActionType) {
    if (action.type !== GET_ROOMS_ACTION) {
        return state;
    }
    return action.payload;
}

const DEFAULT_MODIFY_ROOMS_STATE = { add: false, edit: false, loading: false };

export function modifyRooms(state = DEFAULT_MODIFY_ROOMS_STATE, action: ActionType) {
    switch(action.type) {
        case MODIFY_ROOMS_ACTION:
            return action.payload;
        case GET_ROOMS_ACTION:
            return DEFAULT_MODIFY_ROOMS_STATE;
        default:
            return state;
    }
}