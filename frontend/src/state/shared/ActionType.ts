import { LoginErrorAction } from "state/auth";
import { ReceiveClassificationsAction } from "state/classifications/actions";
import { ImagesErrorAction, LoadingImagesAction, ReceiveImagesAction } from "state/image";
import { ReceiveJournalEntry, ReceiveJournalSummary } from "state/journal";
import { PlantErrorAction, PlantFormErrorAction, ReceivePlantDetailsAction, ReceivePlantsAction } from "state/plant";
import { GetRoomsAction, ModifyRoomsAction, PlantsFilterByRoomAction, UpdateRoomsAction } from "state/room";
import { CurrentUserAction } from "state/user";

export type ActionType = ReceivePlantsAction 
    | ReceivePlantDetailsAction 
    | PlantFormErrorAction
    | PlantErrorAction
    | PlantsFilterByRoomAction
    | UpdateRoomsAction
    | ModifyRoomsAction
    | GetRoomsAction
    | LoginErrorAction
    | CurrentUserAction
    | ReceiveClassificationsAction
    | ReceiveImagesAction
    | LoadingImagesAction
    | ImagesErrorAction
    | ReceiveJournalEntry
    | ReceiveJournalSummary;
