import { Classification } from "api/classifications";
import { Collection } from "api/collections";
import { PlantImage } from "api/image";
import { JournalEntry } from "api/journal";
import { Plant } from "api/plant";
import { User } from "api/user";


export interface AppState {
    errors: AppErrors;
    plants: PlantListAppState;
    plantDetails: Plant;
    currentRoom: Collection;
    rooms: Collection[];
    currentUser: User | false;
    modifyRooms: ModifyRoomsState;
    classifications: Classification[];
    journal: JournalState;
    plantImages: PlantImagesState;
}

export interface PlantImagesState {
    images: PlantImage[];
    loading: boolean;
}

export interface JournalState {
    entry?: JournalEntry;
    entries?: JournalEntry[];
}

export interface ModifyRoomsState {
    add: boolean;
    edit: boolean;
    loading: boolean;
}

export interface PlantListAppState {
    all?: Plant[], 
    byRoom: { [id: number]: Plant[]}
}

export interface AppErrors {
    login?: string;
    plant?: string;
    images?: string;
}