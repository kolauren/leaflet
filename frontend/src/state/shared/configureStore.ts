import { applyMiddleware, createStore } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import rootReducer from "state/shared/rootReducer";

export default function configureStore() {
    return createStore(
        rootReducer,
        applyMiddleware(thunk)
    )
}