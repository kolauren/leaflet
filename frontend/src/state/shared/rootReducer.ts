import { combineReducers } from "@reduxjs/toolkit";
import { classifications } from "state/classifications";
import { errors } from "state/errors";
import { plantImages } from "state/image";
import { journal } from "state/journal";
import { plantDetails, plants } from "state/plant";
import { currentRoom, modifyRooms, rooms } from "state/room";
import { currentUser } from "state/user";

const rootReducer = combineReducers({
    plants,
    plantDetails,
    currentRoom,
    rooms,
    modifyRooms,
    errors,
    currentUser,
    classifications,
    journal,
    plantImages
});

export default rootReducer