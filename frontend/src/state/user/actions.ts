import { User, UserApi } from "api/user";
import { ThunkDispatch } from "redux-thunk";
import { ActionType } from "state/shared";

export const CURRENT_USER_ACTION = "CURRENT_USER";

export interface CurrentUserAction {
    type: typeof CURRENT_USER_ACTION;
    payload: User;
}

export const ANONYMOUS_EMAIL = "ANONYMOUS";

const ANONYMOUS_USER = {
    email: ANONYMOUS_EMAIL,
    firstName: "",
    id: -1
};

export function getCurrentUser(): (dispatch: ThunkDispatch<any, any, ActionType>) => Promise<CurrentUserAction> {
    return (dispatch: ThunkDispatch<any, any, ActionType>) => {
        return UserApi.getCurrent().then(response => dispatch({
            type: CURRENT_USER_ACTION,
            payload: response.data ? response.data : { ...ANONYMOUS_USER }
        }), () => dispatch({
            type: CURRENT_USER_ACTION,
            payload: { ...ANONYMOUS_USER }
        }));
    }
}