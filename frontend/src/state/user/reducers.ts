import { ActionType } from "state/shared";
import { CURRENT_USER_ACTION } from ".";

export function currentUser(state = false, action: ActionType) {
    return action.type === CURRENT_USER_ACTION
        ? action.payload
        : state
}