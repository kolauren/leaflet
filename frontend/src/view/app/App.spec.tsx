import { Store } from "@reduxjs/toolkit";
import { render, RenderResult, screen } from "@testing-library/react";
import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { App } from "./App";

const middlewares = [thunk];
const mockStore = configureStore(middlewares) as any;

describe("App", () => {
    let renderedComponent: RenderResult;

    const renderComponent = (store: Store) => {
      return render (
        <BrowserRouter>
          <Provider store={store}>
              <App />
          </Provider>
        </BrowserRouter>
      );
    }
    
    beforeEach(() => {
        const store = mockStore({
          state: "testing"
        });
        renderedComponent = renderComponent(store);
    });

    it("renders App component", () => {
      expect(screen.getByText("Leaflet")).toBeVisible();
    });

    describe("Footer", () => {      
      it("should have text", () => {
        expect(screen.getByText("Leaflet is a small project created by Lauren and Mike.")).toBeVisible();
      });
    })
});