import { User } from "api/user"
import React, { Component, Fragment } from "react"
import { connect } from "react-redux"
import { Redirect, Route, Switch } from "react-router-dom"
import { ThunkDispatch } from "redux-thunk"
import { ActionType, AppState } from "state/shared"
import { ANONYMOUS_EMAIL, CurrentUserAction, getCurrentUser } from "state/user"
import "view/app/Global.module.scss"
import { AddPlantPage, EditPlantPage, ExplorePage, LandingPage, LoginPage, NoMatchPage, PlantDetailsPage, PlantsPage, SignUpPage } from "view/pages"
import { ScrollToTop } from "."
import styles from "./App.module.scss"
import { AppHeader } from "./AppHeader"
import { AuthenticatedRoute } from "./AuthenticatedRoute"

class App extends Component<AppProps, {}> {
    componentDidMount() {
        this.props.getCurrentUser();
    }

    render() {
        const { currentUser } = this.props;
        const loggedIn = !!(currentUser && currentUser.email !== ANONYMOUS_EMAIL) || false;

        return (
            <Fragment>
                <AppHeader loggedIn={loggedIn} />
                <ScrollToTop>
                    <Switch>
                        <Route exact path="/signup" children={<SignUpPage />} />
                        <Route exact path="/login" children={<LoginPage />} />
                        <Route exact path="/explore" children={<ExplorePage />} />
                        <AuthenticatedRoute path="/plants" exact={true}>
                            <PlantsPage />
                        </AuthenticatedRoute>
                        <AuthenticatedRoute path="/plants/add" exact={true}>
                            <AddPlantPage />
                        </AuthenticatedRoute>
                        <AuthenticatedRoute path="/plants/:id" exact={true}>
                            <PlantDetailsPage />
                        </AuthenticatedRoute>
                        <AuthenticatedRoute path="/plants/:id/edit" exact={false}>
                            <EditPlantPage />
                        </AuthenticatedRoute>
                        {loggedIn ? <Redirect from="/" to="/plants" exact /> : <Route path = "/"><LandingPage /></Route>}
                        <Route path="*" children={<NoMatchPage />} />
                    </Switch>
                </ScrollToTop>
                <footer className={styles.footer}>
                    <span>Leaflet is a small project created by Lauren and Mike.</span>
                    <span>Please leave us feedback at <a href="mailto:emil@email.com">insertemailhere@gmail.com</a>.</span>
                </footer>
            </Fragment>
        )
    }
}

interface AppProps {
    currentUser: User | false;
    getCurrentUser: () => Promise<CurrentUserAction|void>,
}

const mapStateToProps = (state: AppState) => {
    return {
        currentUser: state.currentUser
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    getCurrentUser: () => dispatch(getCurrentUser())
})

const connected = connect(mapStateToProps, mapDispatchToProps)(App)
export { connected as App }
