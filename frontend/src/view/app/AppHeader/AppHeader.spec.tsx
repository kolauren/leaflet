import { Store } from "@reduxjs/toolkit";
import { render, screen } from "@testing-library/react";
import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { AppHeader } from ".";

const middlewares = [thunk];
const mockStore = configureStore(middlewares) as any;

describe("AppHeader", () => { 
    const renderComponent = (store: Store, loggedIn: boolean) => {
        return render(
            <BrowserRouter>
                <Provider store={store}>
                    <AppHeader loggedIn={loggedIn} />
                </Provider>
            </BrowserRouter>
        );
    }

    describe("For anonymous users", () => {
        beforeEach(() => {
            const store = mockStore({
                state: "testing"
            });
    
            renderComponent(store, false);
        });
    
        it("should show all the links available for anonymous users", () => {
            expect(screen.getByText("Leaflet")).toBeVisible();
            expect(screen.getByText("Explore")).toBeVisible();
            expect(screen.getByText("Login")).toBeVisible();
            expect(screen.queryByText("Plants")).toBeNull();
            expect(screen.queryByText("Logout")).toBeNull();
        });
    });

    describe("For anonymous users", () => {
        beforeEach(() => {
            const store = mockStore({
                state: "testing"
            });
    
            renderComponent(store, false);
        });
    
        it("should show all the links available for anonymous users", () => {
            expect(screen.getByText("Leaflet")).toBeVisible();
            expect(screen.getByText("Explore")).toBeVisible();
            expect(screen.getByText("Login")).toBeVisible();
            expect(screen.queryByText("My Plants")).toBeNull();
            expect(screen.queryByText("Logout")).toBeNull();
        });
    });

    describe("For logged in users", () => {
        beforeEach(() => {
            const store = mockStore({
                state: "testing"
            });
    
            renderComponent(store, true);
        });
    
        it("should show all the links available for logged in users", () => {
            expect(screen.getByText("Leaflet")).toBeVisible();
            expect(screen.getByText("My Plants")).toBeVisible();
            expect(screen.getByText("Explore")).toBeVisible();
            expect(screen.getByText("Logout")).toBeVisible();
            expect(screen.queryByText("Login")).toBeNull();
        });
    });
});