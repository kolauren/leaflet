import classNames from "classnames";
import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { logout } from "state/auth";
import { ActionType } from "state/shared";
import styles from "./AppHeader.module.scss";

const PUBLIC_LINKS = [
    {
        route: "/explore",
        name: "Explore"
    }
];

const AUTHENTICATED_LINKS = [
    {
        route: "/plants",
        name: "My Plants"
    }
];

class AppHeader extends Component<AppHeaderProps, {}> {
    handleLogout = () => {
        this.props.logout();
    }

    getRightSideLinks(loggedIn: boolean): ReactNode {
        if (loggedIn) {
            return <button 
                    className={classNames(styles.headerLink, styles.rightSideLink, "simple-button")} 
                    onClick={this.handleLogout}>Logout</button>
        }
    
        return <NavLink className={classNames(styles.headerLink, styles.rightSideLink)} to="/login">Login</NavLink>;
    }
    
    getHeaderLinks(loggedIn: boolean): ReactNode[] {
        const links = loggedIn ? [...AUTHENTICATED_LINKS, ...PUBLIC_LINKS] : [...PUBLIC_LINKS];
        const nodes = [];
    
        for (let link of links) {
            nodes.push(
                <li className={styles.headerLinkItem} key={link.name}>
                    <NavLink 
                        className={styles.headerLink} 
                        activeClassName={styles.headerLinkActive} 
                        to={link.route}>{link.name}</NavLink>
                </li>
            )
        }
    
        return nodes;
    }

    render() {
        const { loggedIn } = this.props;
        return (
            <header className={styles.header}>
                <div className={styles.innerHeader}>
                    <h2 className={styles.title}><NavLink className={styles.titleLink} to="/">Leaflet</NavLink></h2>
                    <ul className={styles.headerlinks}>
                        {this.getHeaderLinks(loggedIn)}
                    </ul>
                    {this.getRightSideLinks(loggedIn)}
                </div>
            </header>
        )
    }
}

interface AppHeaderProps {
    loggedIn: boolean;
    logout: () => Promise<ActionType | void>
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    logout: () => dispatch(logout())
})

const connected = connect(null, mapDispatchToProps)(AppHeader)
export { connected as AppHeader };

