import { User } from "api/user";
import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { ActionType, AppState } from "state/shared";
import { getCurrentUser } from "state/user";
import "view/app/Global.module.scss";

class AuthenticatedRoute extends Component<AuthenticatedRouteProps, {}> {
    render() {
        if (!this.props.currentUser) {
            return "";
        }

        if (this.props.currentUser.email === "ANONYMOUS") {
            return <Redirect from={this.props.path} to="/login" />
        }
        
        return (
            <Route exact={this.props.exact} path={this.props.path}>
                {this.props.children}
            </Route>
        )
    }
}

interface AuthenticatedRouteProps {
    children: ReactNode;
    path: string;
    currentUser: User | false;
    exact: boolean;
}

const mapStateToProps = (state: AppState) => {
    return {
        currentUser: state.currentUser
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    getCurrentUser: () => dispatch(getCurrentUser())
})

const connected = connect(mapStateToProps, mapDispatchToProps)(AuthenticatedRoute)
export { connected as AuthenticatedRoute };


