import React, { Fragment, FunctionComponent, useEffect } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";

type ScrollToTopProps = RouteComponentProps<{}>;

/**
 * ScrollToTop solution from zurfyx on Stack Overflow:
 * https://stackoverflow.com/a/54343182
 */
const ScrollToTop: FunctionComponent<ScrollToTopProps> = ({ history, children }) => {
    useEffect(() => {
      const unlisten = history.listen(() => {
        window.scrollTo(0, 0);
      });
      return () => {
        unlisten();
      }
    }, [history]);
  
    return <Fragment>{children}</Fragment>;
  }
  
const scrollToTop = withRouter(ScrollToTop);
export { scrollToTop as ScrollToTop };
