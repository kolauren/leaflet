import React, { Component, ReactNode, RefObject } from "react";
import styles from "./Autocomplete.module.scss";

class Autocomplete extends Component<AutocompleteProps, AutocompleteState> {
    public ref: RefObject<HTMLDivElement>;

    constructor(props: AutocompleteProps) {
        super(props);
        this.state = { open: false, inputValue: props.defaultValue }
        this.ref = React.createRef();
    }

    handleBodyClick = () => {
        this.setState({ open: false });
    }

    handleOptionSelected = (option: AutocompleteOption) => {
        return (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            event.stopPropagation();
            this.setState({ inputValue: option.title, open: false });
            this.props.onOptionSelected(option);
        }
    }

    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const inputValue = event.target.value;
        this.setState({ inputValue: inputValue, open: true });
        this.props.onInputChange(inputValue);
    }

    handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Escape") {
            this.setState({ open: false });
        }
    }

    toggleOpen = (toggle: boolean) => {
        return () => {
            this.setState({ open: toggle })
        }
    }

    renderAutocomplete(): ReactNode {
        const { options } = this.props;
        const { open, inputValue } = this.state;

        if (!open || !options.length) {
            return "";
        }

        return (
            <ul className={styles.autocomplete}>
                {options.map((option, i) => {
                    return (
                        <li key={i}>
                            <button 
                                className={styles.option}
                                type="button" 
                                onMouseDown={this.handleOptionSelected(option)}>
                                <span 
                                    className={styles.title} 
                                    dangerouslySetInnerHTML={wrapSubstringWithTags(option.title, inputValue)}></span>
                                <span 
                                    className={styles.subtitle}
                                    dangerouslySetInnerHTML={wrapSubstringWithTags(option.subtitle, inputValue)}></span>
                            </button>
                        </li>
                    )
                })}
            </ul>
        )
    }

    render() {
        const { inputClassName, placeholder, name } = this.props;
        return (
            <div ref={this.ref} className={styles.wrapper}>
                <input 
                    className={inputClassName}
                    onFocus={this.toggleOpen(true)}
                    onBlur={this.toggleOpen(false)}
                    value={this.state.inputValue}
                    name={name} 
                    type="text" 
                    placeholder={placeholder}
                    onChange={this.handleInputChange}
                    onKeyDown={this.handleKeyDown} />
                {this.renderAutocomplete()}
            </div>
    )
    }
}

function wrapSubstringWithTags(value: string, substr?: string): { __html: string } {
    if (!substr) {
        return { __html: value };
    }

    let re = new RegExp(`(${substr})`, "gi");
    return { __html: value.replace(re, "<b>$1</b>") };
}

interface AutocompleteProps {
    defaultValue: string | undefined;
    inputClassName: string;
    options: AutocompleteOption[];
    name: string;
    placeholder: string;
    onInputChange: (value: string) => any;
    onOptionSelected: (option: AutocompleteOption) => any;
}

interface AutocompleteState {
    open: boolean;
    inputValue: string | undefined;
}

export interface AutocompleteOption {
    title: string;
    subtitle: string;
    id?: number;
}

export { Autocomplete };
