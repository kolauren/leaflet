import React, { FunctionComponent } from "react"
import styles from "./Body.module.scss"

export const Body: FunctionComponent<{}> = ({ children }) => (
    <div className={styles.body}>
        <div className={styles.container}>
            {children}
        </div>
    </div>
)