import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import { BodyClickListener } from ".";

describe("BodyClickListener", () => {    
    const renderComponent = (
        attach: boolean, 
        onBodyClick: (event: MouseEvent) => any,
        element: HTMLElement) => {
        return render(
            <BodyClickListener 
                attach={attach} 
                onBodyClick={onBodyClick}
                element={element}>
                <div>testing</div>
            </BodyClickListener>
        );
    }
    
    let element: HTMLButtonElement;

    beforeAll(() => {
        element = document.createElement("button");
        element.innerHTML = "button";
        document.body.appendChild(element);
    });

    describe("When attach is true", () => {
        let testFn: () => any;

        beforeEach(() => {
            testFn = jest.fn();
            renderComponent(true, testFn, element);
        });

        afterEach(() => {
            cleanup();
        });

        it("should trigger body click function when anything outside of the target element is clicked", () => {
            userEvent.click(screen.getByText("testing"));
            expect(testFn).toHaveBeenCalled();
        }); 

        it("should not trigger body click function when target element is clicked", () => {
            userEvent.click(screen.getByText("button"));
            expect(testFn).not.toHaveBeenCalled();
        }); 
    });

    describe("When attach is false", () => {
        let testFn: () => any;

        beforeEach(() => {
            testFn = jest.fn();
            renderComponent(false, testFn, element);
        });

        afterEach(() => {
            cleanup();
        });

        it("should not trigger body click function when anything outside of the target element is clicked", () => {
            userEvent.click(screen.getByText("testing"));
            expect(testFn).not.toHaveBeenCalled();
        }); 

        it("should not trigger body click function when target element is clicked", () => {
            userEvent.click(screen.getByText("button"));
            expect(testFn).not.toHaveBeenCalled();
        }); 
    });
});