import React, { Component, Fragment, ReactNode } from "react";

class BodyClickListener extends Component<BodyClickListenerProps, {}> {
    public bodyClickHandler: (event: MouseEvent) => void;

    constructor(props: BodyClickListenerProps) {
        super(props);
        
        this.bodyClickHandler = this.handleBodyClick.bind(this);
        this.state = { attached: false };
    }

    componentDidMount() {
        if (this.props.attach) {
            document.addEventListener("mousedown", this.bodyClickHandler);
        }
    }

    componentDidUpdate() {
        if (this.props.attach) {
            document.removeEventListener("mousedown", this.bodyClickHandler);
            document.addEventListener("mousedown", this.bodyClickHandler);
        } else {
            document.removeEventListener("mousedown", this.bodyClickHandler);
        }
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.bodyClickHandler);
    }

    handleBodyClick(event: MouseEvent): void {
        const { element } = this.props;
        if (!element || !element.contains(event.target as Node)) {
            this.props.onBodyClick(event);
        }
    }
    
    render() {
        return (
            <Fragment>{this.props.children}</Fragment>
        )
    }
}

interface BodyClickListenerProps {
    attach: boolean;
    children: ReactNode[] | ReactNode;
    onBodyClick: (event: MouseEvent) => any;
    element: HTMLElement | null;
}

export { BodyClickListener };
