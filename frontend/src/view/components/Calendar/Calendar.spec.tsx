import { Store } from "@reduxjs/toolkit";
import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { JournalApi } from "api/journal";
import { AxiosResponse } from "axios";
import { addMonths, format, getDaysInMonth, startOfMonth, subMonths } from "date-fns";
import React from "react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { Calendar } from "./Calendar";

const middlewares = [thunk];
const mockStore = configureStore(middlewares) as any;

describe("Calendar", () => {  
    const renderComponent = (store: Store, plantId: number) => {
        return render(
            <Provider store={store}>
                <Calendar plantId={plantId} />
            </Provider>
        );
    }

    beforeEach(() => {
        const store = mockStore({
            journal: { entries: [] }
        });

        jest.spyOn(JournalApi, "getEntries").mockImplementation(() => Promise.resolve({
            data: { records: [] }
        } as AxiosResponse));
        
        renderComponent(store, 1);
    });

    afterEach(() => {
        cleanup();
    });

    it("should show the current month as the title", () => {
        const currentMonth = new Date();

        expect(screen.getByText(format(currentMonth, "MMMM yyyy"))).toBeVisible();
    });

    it("should show the next month if you click the right arrow", () => {
        let month = startOfMonth(new Date());
        month = addMonths(month, 1);
        userEvent.click(screen.getByLabelText("Next month"));

        expect(screen.getByText(format(month, "MMMM yyyy"))).toBeVisible();
    });

    it("should show the previous month if you click the left arrow", () => {
        let month = startOfMonth(new Date());
        month = subMonths(month, 1);
        userEvent.click(screen.getByLabelText("Previous month"));
        
        expect(screen.getByText(format(month, "MMMM yyyy"))).toBeVisible();
    });

    it("should show all the days of the current month", async () => {
        const currentMonth = new Date()

        const daysInMonth = getDaysInMonth(currentMonth);
        for (let i = 1; i <= daysInMonth; i++) {
            expect(await screen.findAllByText(i)).not.toBeNull();
        }
    });
});