import { JournalEntry } from "api/journal";
import classNames from "classnames";
import { addDays, addMonths, addWeeks, endOfWeek, format, formatISO, getDaysInMonth, isSameDay, isSameMonth, startOfMonth, startOfWeek, subMonths } from "date-fns";
import { ReactComponent as LeftArrow } from "images/left-arrow.svg";
import { ReactComponent as RightArrow } from "images/right-arrow.svg";
import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { fetchJournalEntries } from "state/journal";
import { ActionType, AppState } from "state/shared";
import { CalendarDay, CalendarDayDetails } from ".";
import styles from "./Calendar.module.scss";

class Calendar extends Component<CalendarProps, CalendarState> {
    constructor(props: CalendarProps) {
        super(props);
        
        this.state = { 
            selectedDay: new Date(), 
            ...getMonthState(startOfMonth(new Date()))
        };
    }

    componentDidMount() {
        const { startDay, lastDay } = this.state;
        this.fetchMonthSummary(startDay, lastDay);
    }

    fetchMonthSummary(startDay: Date, lastDay: Date) {
        const { plantId, fetchJournalEntries } = this.props;
        fetchJournalEntries(plantId, startDay, lastDay);
    }

    selectDay = (day: Date) => {
        return () => {
            this.setState({ selectedDay: day });
        };
    }

    isSelected(day: Date): boolean {
        return isSameDay(this.state.selectedDay, day);
    }

    toggleMonth(next = false): () => void {
        return () => {
            const newMonth = next ? addMonths(this.state.currentMonth, 1) : subMonths(this.state.currentMonth, 1);
            const monthState = getMonthState(newMonth);
            this.setState({ ...monthState });

            this.fetchMonthSummary(monthState.startDay, monthState.lastDay);
        }
    }

    getDaysOfWeek(day: Date, currentMonth: Date, totalSummary: JournalEntry[]): ReactNode[] {
        const days = [];
        const { plantId } = this.props;
    
        for (let i = 0; i < 7; i++) {
            days.push(
                <div 
                    key={i}     
                    className={styles.day}  
                    onClick={this.selectDay(day)} 
                    role="button">
                    <CalendarDay day={day} 
                                 isPrevious={!isSameMonth(day, currentMonth)}
                                 entry={getEntry(day, totalSummary)} 
                                 plantId={plantId}
                                 selected={this.isSelected(day)} />
                </div>
            );
            day = addDays(day, 1);
        }
    
        return days;
    }
    
    getWeeks(rows: number, startDate: Date, currentMonth: Date, totalSummary: JournalEntry[]): ReactNode[] {
        let weeks = [];
    
        for (let i = 0; i < rows; i++) {
            weeks.push(<div className={styles.week} key={i}>{this.getDaysOfWeek(startDate, currentMonth, totalSummary)}</div>);
            startDate = addDays(startDate, 7);
        }
    
        return weeks;
    }

    render() {
        const { currentMonth, startDay, rows, selectedDay } = this.state;
        const { entries, plantId } = this.props;
            
        return (
            <div className={styles.main}>
                <div className={styles.column}>
                    <div className={styles.header}>
                        <button 
                            aria-label="Previous month"
                            className={classNames(styles.toggleButton, "secondary-button")} 
                            onClick={this.toggleMonth(false)}>
                            <LeftArrow className={styles.toggleIcon}/>
                        </button>
                        <h2 className={styles.title}>{format(currentMonth, "MMMM yyyy")}</h2>
                        <button 
                            aria-label="Next month"
                            className={classNames(styles.toggleButton, "secondary-button")} 
                            onClick={this.toggleMonth(true)}>
                            <RightArrow className={styles.toggleIcon}/>
                        </button>
                    </div>
                    <div className={styles.month}>
                        {this.getWeeks(rows, startDay, currentMonth, entries)}
                    </div>
                </div>
                <div className={styles.column}>
                    <CalendarDayDetails 
                        day={selectedDay}
                        currentEntry={getEntry(selectedDay, entries)}
                        plantId={plantId} />
                </div>
            </div>
        )
    }
}

interface CalendarState {
    selectedDay: Date;
    currentMonth: Date;
    startDay: Date;
    lastDay: Date;
    rows: number;
}

interface CalendarProps {
    plantId: number;
    entries: JournalEntry[];
    fetchJournalEntries: (plantId: number, from: Date, to: Date) => Promise<ActionType>;
}

function getMonthState(currentMonth: Date): { currentMonth: Date, startDay: Date, lastDay: Date, rows: number } {
    const startDay = startOfWeek(currentMonth);
    const rows = getNumRows(currentMonth);
    return {
        currentMonth,
        rows,
        startDay,
        lastDay: endOfWeek(addWeeks(startDay, rows - 1))
    };
}

function getNumRows(monthStart: Date): number {
    const startDay = monthStart.getDay();
    const daysInMonth = getDaysInMonth(monthStart);

    let rows = (startDay + daysInMonth) / 7;
    if ((startDay + daysInMonth) % 7 !== 0) {
        rows++;
    }

    return Math.floor(rows);
}


function getEntry(day: Date, entries: JournalEntry[]): JournalEntry | undefined {
    const key = formatISO(day, { representation: "date" });
    return entries.find(entry => entry.entryDate === key);
}

const mapStateToProps = (state: AppState) => {    
    const { entries } = state.journal;
    return {
        entries: entries || []
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => {
    return {
        fetchJournalEntries: (plantId: number, from: Date, to: Date) => dispatch(fetchJournalEntries(plantId, from, to)),
    };
}

const connected = connect(mapStateToProps, mapDispatchToProps)(Calendar)
export { connected as Calendar };


