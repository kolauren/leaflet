import { JournalEntry, JournalEntryForm } from "api/journal";
import classNames from "classnames";
import React, { Component } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { addJournalEntry, updateJournalEntry } from "state/journal";
import { ActionType } from "state/shared";
import { formatDateForApi } from "view/utils";
import styles from "./CalendarDay.module.scss";
import { WaterIcon } from "./WaterIcon/WaterIcon";

class CalendarDay extends Component<CalendarDayProps, {}> { 
    handleToggleWatered = () => {
        const { entry, plantId, day } = this.props;
        
        if (entry) {
            if (!entry.watered) {
                this.props.updateJournalEntry(plantId, {
                    ...entry,
                    watered: !entry.watered
                });
            }

            return;
        }

        return this.props.addJournalEntry(plantId, {
            entryDate: formatDateForApi(day),
            watered: true
        });
    }

    render() {
        const { entry, isPrevious, selected, day } = this.props;
        const watered = !!(entry && entry.watered);

        return (
            <div className={classNames(styles.day, { [styles.dayPast]: isPrevious, [styles.dayWatered]: watered })}>
                <span className={classNames(styles.number, { [styles.numberSelected]: selected })}>{day.getDate()}</span>
                <div className={styles.icons}>
                    <WaterIcon watered={watered} toggleIcon={this.handleToggleWatered} />
                </div>
            </div>
        )
    }
}

interface CalendarDayProps {
    day: Date;
    isPrevious: boolean;
    entry?: JournalEntry;
    plantId: number;
    selected: boolean;
    addJournalEntry: (plantId: number, entry: JournalEntryForm) => Promise<ActionType>;
    updateJournalEntry: (plantId: number, entry: JournalEntry) => Promise<ActionType>;
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => {
    return {
        updateJournalEntry: (plantId: number, entry: JournalEntry) => dispatch(updateJournalEntry(plantId, entry)),
        addJournalEntry: (plantId: number, entry: JournalEntryForm) => dispatch(addJournalEntry(plantId, entry)),
    };
}

const connected = connect(null, mapDispatchToProps)(CalendarDay)
export { connected as CalendarDay };
