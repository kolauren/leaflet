import classNames from "classnames";
import { ReactComponent as WaterDrop } from "images/water-drop.svg";
import React, { Component } from "react";
import styles from "./WaterIcon.module.scss";

class WaterIcon extends Component<WaterIconProps, {}> {
    render() {
        const { watered } = this.props;

        return (
            <span className={styles.wrapper} onClick={this.props.toggleIcon}>
                <span className={styles.tooltip}>{watered ? "Watered" : "Mark as watered"}</span>
                <WaterDrop className={classNames(styles.icon, { [styles.iconWatered]: watered })} />
            </span>
        )
    }
}

interface WaterIconProps {
    watered: boolean;
    toggleIcon: () => any;
}

export { WaterIcon };
