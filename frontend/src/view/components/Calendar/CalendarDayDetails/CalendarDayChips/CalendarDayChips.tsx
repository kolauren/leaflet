import { EntryMood } from "api/journal";
import React, { Component } from "react";
import { Chip, MoodChip } from "view/components/Chip";
import { Select } from "view/components/Select";
import styles from "./CalendarDayChips.module.scss";

export class CalendarDayChips extends Component<CalendarDayChipsProps, {}> {
    render() {
        const { watered, mood, editing } = this.props;

        if (editing) {
            return (
                <div className={styles.selects}>
                    <span className={styles.selectWrapper}>
                        <label htmlFor="watered">Watered</label>
                        <Select 
                            selectClass={styles.select}
                            defaultValue={watered ? "true" : "false"} 
                            name="watered" 
                            onChange={this.props.onWateredChange}>
                            <option value={"true"}>Yes</option>
                            <option value={"false"}>No</option>
                        </Select>
                    </span>
                    <span className={styles.selectWrapper}>
                        <label htmlFor="watered">Mood</label>   
                        <Select 
                            data-testid="mood-select"
                            selectClass={styles.select}
                            defaultValue={mood}
                            name="mood" 
                            onChange={this.props.onMoodChange}>
                            <option value={EntryMood.NA}>N/A</option>
                            <option value={EntryMood.HAPPY}>Happy</option>
                            <option value={EntryMood.OKAY}>Okay</option>
                            <option value={EntryMood.SAD}>Sad</option>
                            <option value={EntryMood.DEAD}>Dead</option>
                        </Select>
                    </span>
                </div>
            )
        }

        return (
            <div>
                <Chip 
                    className={styles.chip}
                    title={"Watered"} 
                    desc={watered ? "Yes" : "No"}
                    value={watered ? 1 : 0} />
                <MoodChip 
                    title={"Mood"} 
                    mood={mood} />
            </div>
        )
    }
}

interface CalendarDayChipsProps {
    editing: boolean;
    watered: boolean;
    mood: EntryMood;
    onMoodChange: (event: React.ChangeEvent<HTMLSelectElement>) => any;
    onWateredChange: (event: React.ChangeEvent<HTMLSelectElement>) => any;
}