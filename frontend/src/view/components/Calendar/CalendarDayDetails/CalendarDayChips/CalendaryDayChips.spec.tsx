import { cleanup, fireEvent, render, screen } from "@testing-library/react";
import { EntryMood } from "api/journal";
import React from "react";
import { CalendarDayChips } from ".";

describe("CalendarDayChips", () => {  
    
    const renderComponent = (
        editing: boolean,
        watered: boolean,
        mood: EntryMood,
        onMoodChange: (event: React.ChangeEvent<HTMLSelectElement>) => any,
        onWateredChange: (event: React.ChangeEvent<HTMLSelectElement>) => any
    ) => {
        return render(
            <CalendarDayChips 
                editing={editing}
                watered={watered}
                mood={mood}
                onMoodChange={onMoodChange}
                onWateredChange={onWateredChange} />
        );
    }

    describe("Editing", () => {
        afterEach(() => {
            cleanup();
        });

        it("should not show form inputs when edit is false", () => {
            renderComponent(false, true, EntryMood.SAD, () => {}, () => {});

            expect(screen.queryByDisplayValue("Yes")).toBeNull();
            expect(screen.queryByDisplayValue("Sad")).toBeNull();

            expect(screen.getByText("Yes")).toBeVisible();
            expect(screen.getByText("Sad")).toBeVisible();
        });

        it("should show form inputs when edit is true", () => {
            renderComponent(true, true, EntryMood.SAD, () => {}, () => {});

            expect(screen.getByDisplayValue("Yes")).toBeVisible();
            expect(screen.getByDisplayValue("Sad")).toBeVisible();
        });
    });

    describe("Watered", () => {
        afterEach(() => {
            cleanup();
        });

        it("should show Yes if true", () => {
            renderComponent(false, true, EntryMood.SAD, () => {}, () => {});

            expect(screen.getByText("Yes")).toBeVisible();
            expect(screen.queryByText("No")).toBeNull();
        });

        it("should show No if false", () => {
            renderComponent(false, false, EntryMood.SAD, () => {}, () => {});
            
            expect(screen.getByText("No")).toBeVisible();
            expect(screen.queryByText("Yes")).toBeNull();
        });
    });

    describe("Mood", () => {
        it("should show correct mood based on number", () => {
            renderComponent(false, true, EntryMood.SAD, () => {}, () => {});
            renderComponent(false, true, EntryMood.HAPPY, () => {}, () => {});
            renderComponent(false, true, EntryMood.NA, () => {}, () => {});


            expect(screen.getByText("Sad")).toBeVisible();
            expect(screen.getByText("Happy")).toBeVisible();
            expect(screen.getByText("N/A")).toBeVisible();

            cleanup();
        });
    });

    describe("Form inputs", () => {
        let wateredFn: jest.Mock;
        let moodFn: jest.Mock;; 

        beforeEach(() => {
            wateredFn = jest.fn();
            moodFn = jest.fn();
            renderComponent(true, true, EntryMood.SAD, moodFn, wateredFn);

        });

        afterEach(() => {
            cleanup();
        });

        it("should call watered change when new option is selected for watered", () => {
            const selects = screen.queryAllByTestId("select");
            fireEvent.change(selects[0], { target: { value: "false" }});

            expect(screen.getByDisplayValue("No")).toBeVisible();
            expect(wateredFn).toHaveBeenCalled();
        });

        it("should call mood change when new option is selected for mood", () => {
            const selects = screen.queryAllByTestId("select");
            fireEvent.change(selects[1], { target: { value: "OKAY" }});

            expect(screen.getByDisplayValue("Okay")).toBeVisible();
            expect(moodFn).toHaveBeenCalled();
        });
    });

});