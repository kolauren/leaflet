import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { EntryMood, JournalApi, JournalEntry } from "api/journal";
import { AxiosResponse } from "axios";
import { toDate } from "date-fns";
import React from "react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { getDateString } from "view/utils";
import { CalendarDayDetails } from "./CalendarDayDetails";

const middlewares = [thunk];
const mockStore = configureStore(middlewares) as any;

describe("CalendarDayDetails", () => {  

    const renderComponent = (day: Date, plantId: number, currentEntry?: JournalEntry) => {
        const store = mockStore({
            journal: { }
        });

        return render(
            <Provider store={store}>
                <CalendarDayDetails 
                    day={day} 
                    plantId={plantId}
                    currentEntry={currentEntry} />
            </Provider>
        );
    }

    beforeAll(() => {
        jest.spyOn(JournalApi, "addEntry").mockImplementation(() => Promise.resolve({
            data: {
                id: 1, 
                plantId: 1,
                entryDate: "2021-01-01",
                note: "Testing",
                watered: true, 
                mood: EntryMood.HAPPY
            }
        } as AxiosResponse));

        jest.spyOn(JournalApi, "updateEntry").mockImplementation(() => Promise.resolve({
            data: {
                id: 1, 
                plantId: 1,
                entryDate: "2021-01-01",
                note: "Testing",
                watered: true, 
                mood: EntryMood.HAPPY
            }
        } as AxiosResponse));
        
    });

    describe("Title", () => {
        it("should show today's date when passed in", async () => {
            renderComponent(new Date(), 1);
            expect(await screen.getByText(getDateString(new Date()))).toBeVisible();
            cleanup();
        });

        it("should show date when passed in", async () => {
            const date = new Date(2014, 1, 11);
            renderComponent(date, 1);
            expect(await screen.getByText(getDateString(toDate(date)))).toBeVisible();
            cleanup();
        });
    });

    describe("Buttons", () => {
        let editButton: HTMLElement;

        beforeEach(() => {
            renderComponent(new Date(), 1, {
                id: 1, 
                plantId: 1,
                entryDate: "2021-01-01",
                note: "Testing",
                watered: true, 
                mood: EntryMood.HAPPY
            });
            editButton = screen.getByLabelText("Edit");
        });

        afterEach(() => {
            cleanup();
        });

        it("should show save and cancel buttons when clicking edit", () => {
            expect(editButton).toBeVisible();

            userEvent.click(editButton);

            expect(screen.getByText("Save")).toBeVisible();
            expect(screen.getByText("Cancel")).toBeVisible();
        });

        it("should hide save and cancel buttons after save is clicked", () => {
            userEvent.click(editButton);

            const saveButton = screen.getByText("Save");
            expect(saveButton).toBeVisible();
            userEvent.click(saveButton);

            expect(screen.queryByText("Save")).toBeNull();
            expect(screen.queryByText("Cancel")).toBeNull();
        });

        it("should hide save and cancel buttons after cancel is clicked", () => {
            userEvent.click(editButton);

            const cancelButton = screen.getByText("Cancel");
            expect(cancelButton).toBeVisible();
            userEvent.click(cancelButton);

            expect(screen.queryByText("Save")).toBeNull();
            expect(screen.queryByText("Cancel")).toBeNull();
        });
    });

    describe("Form inputs", () => {
        let editButton: HTMLElement;

        beforeEach(() => {
            renderComponent(new Date(), 1, {
                id: 1, 
                plantId: 1,
                entryDate: "2021-01-01",
                note: "Testing",
                watered: true, 
                mood: EntryMood.HAPPY
            });
            editButton = screen.getByLabelText("Edit");
        });

        afterEach(() => {
            cleanup();
        });

        it("should show when edit button is clicked", () =>{
            userEvent.click(editButton);

            expect(screen.getByDisplayValue("Yes")).toBeVisible();
            expect(screen.getByDisplayValue("Happy")).toBeVisible();
            expect(screen.getByDisplayValue("Testing")).toBeVisible();
        });

        it("should hide when cancel button is clicked", () =>{
            userEvent.click(editButton);
            
            const cancelButton = screen.getByText("Cancel");
            expect(cancelButton).toBeVisible();
            userEvent.click(cancelButton);

            expect(screen.queryByDisplayValue("Yes")).toBeNull();
            expect(screen.queryByDisplayValue("Happy")).toBeNull();
            expect(screen.queryByDisplayValue("Testing")).toBeNull();

            expect(screen.getByText("Yes")).toBeVisible();
            expect(screen.getByText("Happy")).toBeVisible();
            expect(screen.getByText("Testing")).toBeVisible();
        });

        it("should hide when save button is clicked", () =>{
            userEvent.click(editButton);
            
            const saveButton = screen.getByText("Save");
            expect(saveButton).toBeVisible();
            userEvent.click(saveButton);

            expect(screen.queryByDisplayValue("Yes")).toBeNull();
            expect(screen.queryByDisplayValue("Happy")).toBeNull();
            expect(screen.queryByDisplayValue("Testing")).toBeNull();

            expect(screen.getByText("Yes")).toBeVisible();
            expect(screen.getByText("Happy")).toBeVisible();
            expect(screen.getByText("Testing")).toBeVisible();
        });
    });
});