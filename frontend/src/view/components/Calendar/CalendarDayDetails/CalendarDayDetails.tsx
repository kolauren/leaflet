import { EntryMood, JournalEntry, JournalEntryForm } from "api/journal";
import classNames from "classnames";
import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { addJournalEntry, updateJournalEntry } from "state/journal";
import { ActionType } from "state/shared";
import { EditButton } from "view/components/EditButton";
import { formatDateForApi, getDateString } from "view/utils";
import { CalendarDayChips } from "./CalendarDayChips";
import styles from "./CalendarDayDetails.module.scss";
import { CalendarDayNotes } from "./CalendarDayNotes";

class CalendarDayDetails extends Component<CalendarDayDetailsProps, CalendarDayDetailsState> {
   
    constructor(props: CalendarDayDetailsProps) {
        super(props);
        
        this.state = { 
            editing: false
        };
    }

    handleEdit = () => {
        const { currentEntry } = this.props;
        let newState = {};

        if (currentEntry) {
            newState = {
                isWatered: currentEntry.watered ? "true" : "false",
                mood: currentEntry.mood,
                note: currentEntry.note
            };
        }

        this.setState({ editing: true, ...newState });
    }

    handleCancel = () => {
        this.setState({ editing: false });
    }

    handleSave = () => {
        const { isWatered, mood, note } = this.state;
        const { day, plantId, currentEntry } = this.props;

        if (currentEntry) {
            this.props.updateJournalEntry(plantId, {
                id: currentEntry.id,
                plantId,
                entryDate: formatDateForApi(day),
                watered: isWatered === "true",
                mood: mood === EntryMood.NA ? undefined : mood,
                note
            });
        } else {
            this.props.addJournalEntry(plantId, {
                entryDate: formatDateForApi(day),
                watered: isWatered === "true",
                mood: mood === EntryMood.NA ? undefined : mood,
                note
            });
        }

        this.setState({ editing: false });
    }

    handleFormChange = (propName: string) => {
        return (event: React.ChangeEvent<HTMLSelectElement|HTMLTextAreaElement>) => {
            this.setState({ editing: true, [propName]: event.target.value });
        }
    }

    renderSaveButton(): ReactNode | string {
        return (
            <div className={"button-group"}>
                <button 
                    className={"action-button"}
                    onClick={this.handleSave}>Save</button>
                <button 
                    className={"secondary-button"}
                    onClick={this.handleCancel}>Cancel</button>
            </div>
        )
    }

    render() {
        const { props } = this;
        const { day } = props;
        const { editing } = this.state;
        const entry = this.props.currentEntry;

        return (
            <section>
                <div className={styles.header}>
                    <h1>{getDateString(day)}</h1>
                    <EditButton
                        buttonClass={classNames({ [styles.editing]: editing })}
                        onClick={this.handleEdit} />
                </div>
                <CalendarDayChips 
                    editing={editing}
                    watered={entry && entry.watered ? entry.watered : false}
                    mood={entry && entry.mood ? entry.mood : EntryMood.NA}
                    onWateredChange={this.handleFormChange("isWatered")}
                    onMoodChange={this.handleFormChange("mood")} />
                <div className={styles.notes}>
                    <h1>Notes:</h1>
                    <CalendarDayNotes 
                        notes={entry ? entry.note : undefined}
                        editing={editing}
                        onNotesClick={this.handleEdit}
                        onTextareaChange={this.handleFormChange("note")} />
                </div>
                {editing ? this.renderSaveButton() : ""}
            </section>
        )
    }
}

interface CalendarDayDetailsState {
    editing: boolean;
    note?: string;
    isWatered?: string;
    mood?: EntryMood;
}

interface CalendarDayDetailsProps {
    day: Date;
    plantId: number;
    currentEntry: JournalEntry | undefined;
    addJournalEntry: (plantId: number, entry: JournalEntryForm) => Promise<ActionType>;
    updateJournalEntry: (plantId: number, entry: JournalEntry) => Promise<ActionType>;
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => {
    return {
        addJournalEntry: (plantId: number, entry: JournalEntryForm) => dispatch(addJournalEntry(plantId, entry)),
        updateJournalEntry: (plantId: number, entry: JournalEntry) => dispatch(updateJournalEntry(plantId, entry))
    };
}

const connected = connect(null, mapDispatchToProps)(CalendarDayDetails)
export { connected as CalendarDayDetails };
