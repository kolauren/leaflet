import { cleanup, fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import { CalendarDayNotes } from ".";

describe("CalendarDayNotes", () => {  
    
    const renderComponent = (
        editing: boolean,
        onNotesClick: () => any,
        onTextareaChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => any,
        notes?: string
    ) => {
        return render(
            <CalendarDayNotes 
                editing={editing}
                notes={notes}
                onNotesClick={onNotesClick}
                onTextareaChange={onTextareaChange} />
        );
    }

    describe("Editing", () => {
        afterEach(() => {
            cleanup();
        });

        it("should not show text area when editng is false", () => {
            renderComponent(false, () => {}, () => {}, "Test");

            expect(screen.getByText("Test")).toBeVisible();
            expect(screen.queryByDisplayValue("Test")).toBeNull();
        });

        it("should show text area when editing is true", () => {
            renderComponent(true, () => {}, () => {}, "Test");

            expect(screen.getByDisplayValue("Test")).toBeVisible();
            expect(screen.queryByText("Test")).toBeVisible();
        });
    });

    describe("Notes", () => {
        afterEach(() => {
            cleanup();
        });

        it("should show a message to prompt users to click if there are empty", () => {
            renderComponent(false, () => {}, () => {});

            expect(screen.getByText("Click to add notes")).toBeVisible();
        });

        it("should display if they are not empty", () => {
            renderComponent(false, () => {}, () => {}, "Testing some notes");

            expect(screen.getByText("Testing some notes")).toBeVisible();
        });

        it("should be clickable when they are empty", () => {
            const notesClicked = jest.fn();
            renderComponent(false, notesClicked, () => {});
            
            const clickable = screen.getByText("Click to add notes");
            expect(clickable).toBeVisible();

            userEvent.click(clickable);
            expect(notesClicked).toHaveBeenCalled();
        });
    });

    describe("When text area is changed", () => {
        it("should notify outer function", () => {
            const textAreaChanged = jest.fn();
            renderComponent(true, () => {}, textAreaChanged, "testing");
            
            const textarea = screen.getByDisplayValue("testing");
            fireEvent.change(textarea, { target: { value: "new value" } });
            expect(textAreaChanged).toHaveBeenCalled();

            cleanup();
        });
    });
});