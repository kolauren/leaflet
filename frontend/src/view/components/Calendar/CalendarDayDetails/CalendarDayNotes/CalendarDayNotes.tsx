import classNames from "classnames";
import React, { Component, RefObject } from "react";
import styles from "./CalendarDayNotes.module.scss";

export class CalendarDayNotes extends Component<CalendarNotesProps, {}> {
    textareaRef: RefObject<HTMLTextAreaElement>;

    constructor(props: CalendarNotesProps) {
        super(props);
        this.textareaRef = React.createRef();
    }

    componentDidUpdate() {
        if (this.props.editing && this.textareaRef.current) {
            this.textareaRef.current?.focus();
        }
    }
    
    render() {
        const { editing, notes } = this.props;

        if (editing) {
            return (
                <textarea 
                    ref={this.textareaRef}
                    className={styles.textarea}
                    defaultValue={notes}
                    onChange={this.props.onTextareaChange}></textarea>
            )
        }

        return (
            <p 
                className={classNames({ [styles.noNotes]: !notes })}
                role="button"
                onClick={this.props.onNotesClick}>
                {!notes ? "Click to add notes" : notes}     
            </p>
        )
    }
}

interface CalendarNotesProps {
    editing: boolean;
    notes: string | undefined;
    onNotesClick: () => any;
    onTextareaChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => any;
}