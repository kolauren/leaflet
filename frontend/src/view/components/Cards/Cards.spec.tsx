import { cleanup, render, screen } from "@testing-library/react";
import React from "react";
import { BrowserRouter } from "react-router-dom";
import { CardItem, Cards } from ".";

describe("Cards", () => {  
    const renderComponent = (cards: CardItem[]) => {
        return render(
            <BrowserRouter>
                <Cards cards={cards}/>
            </BrowserRouter>
        );
    }

    describe("When there are no cards", () => {
        it("should show a loading state", () => {
            renderComponent([]);

            expect(screen.queryAllByTestId("loading").length).toBe(5);

            cleanup();
        });
    });

    describe("When there are cards", () => {
        it("should show the cards", () => {
            renderComponent([
                {
                    title: "Testing1",
                    subTitle: "Testing2",
                    link: "/testing"
                },
                {
                    title: "Testing3",
                    subTitle: "Testing4",
                    link: "/testing"
                },
            ]);

            expect(screen.getByText("Testing1")).toBeVisible();
            expect(screen.getByText("Testing2")).toBeVisible();
            expect(screen.getByText("Testing3")).toBeVisible();
            expect(screen.getByText("Testing4")).toBeVisible();

            cleanup();
        });
    });
});