import defaultPhoto from "images/default-photo.png";
import React, { Fragment, FunctionComponent, ReactNode } from "react";
import { Link } from "react-router-dom";
import styles from "./Cards.module.scss";


const Cards: FunctionComponent<{ cards: CardItem[] }> = ({ cards }) => {
    return (
        <section className={styles.cards}>
            {!cards || cards.length <= 0 ? getLoadingCards() : cards.map((cardItem, index) => getCard(cardItem, index))}
        </section>
    )
}

function getLoadingCards(): ReactNode {
    return (
        <Fragment>
            <div data-testid="loading" className={styles.loading}></div>
            <div data-testid="loading" className={styles.loading}></div>
            <div data-testid="loading" className={styles.loading}></div>
            <div data-testid="loading" className={styles.loading}></div>
            <div data-testid="loading" className={styles.loading}></div>
        </Fragment>
    )
}

function getCard(card: CardItem, index: number) {
    const { title, subTitle, link, image } = card;
    return (
        <Link to={link} key={index}>
            <div className={styles.card}>
                <div className={styles.image}>
                    <span className={styles.hoverText}>View</span>
                    <div className={styles.bg} style={{ backgroundImage: `url(${image ? image : defaultPhoto})` }}></div>
                </div>
                <div className={styles.info}>
                    <span className={styles.title}>{title}</span>
                    <span className={styles.subTitle}>{subTitle}</span>
                </div>
            </div>
        </Link>
    )
}

export interface CardItem {
    title: string;
    subTitle: string;
    link: string;
    image?: string;
}

export { Cards };
