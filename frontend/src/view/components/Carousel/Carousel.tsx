import { PlantImage } from "api/image";
import classNames from "classnames";
import defaultPhoto from "images/default-photo.png";
import { ReactComponent as LeftArrow } from "images/left-arrow.svg";
import { ReactComponent as RightArrow } from "images/right-arrow.svg";
import React, { Component, Fragment, ReactNode } from "react";
import ReactModal from "react-modal";
import { connect } from "react-redux";
import { CSSTransition } from "react-transition-group";
import { ThunkDispatch } from "redux-thunk";
import { clearImageErrors, getPlantImages, ImagesErrorAction, ReceiveImagesAction } from "state/image";
import { ActionType, AppState } from "state/shared";
import { LoadingDots } from "../LoadingDots";
import styles from "./Carousel.module.scss";
import { CarouselMenu } from "./CarouselMenu/CarouselMenu";

const MAX_IMAGES = 5;

class Carousel extends Component<CarouselProps, CarouselState> {
    constructor(props: CarouselProps) {
        super(props);
        this.state = { currentIndex: 0, errorOpen: false };
    }

    componentDidMount() {
        this.props.fetchImages(this.props.plantId);
    }

    componentDidUpdate(prevProps: CarouselProps) {
        const { loading, error } = this.props;

        if (prevProps.loading === false && loading === true) {
            this.setState({ currentIndex: 0 });
        }

        if (prevProps.error == null && error != null) {
            this.setState({ errorOpen: true });
        }

        if (prevProps.error != null && error == null) {
            this.setState({ errorOpen: false });
        }
    }

    getPreviousImage = () => {
        let { currentIndex } = this.state;
        if (currentIndex >= 1) {
            this.setState({ currentIndex: currentIndex - 1  });
        }
    }

    getNextImage = () => {
        let { currentIndex } = this.state;
        const images = this.props.images || [];

        if (currentIndex < images.length - 1) {
            this.setState({ currentIndex: currentIndex + 1 });
        }
    }
    
    closeErrorModal = () => {
        this.setState({ errorOpen: false });
        this.props.clearImageErrors();
    }

    renderImages(): ReactNode {
        let images = this.props.images;
        
        if (!images || images.length === 0) {
            return (
                <div className={classNames(styles.imageWrapper, styles.default)}>
                    <img
                        alt="plant"
                        src={defaultPhoto}
                        className={classNames(styles.img)} />
                    <CarouselMenu
                        className={classNames(styles.menu, styles.noPhoto)}
                        plantId={this.props.plantId}
                        showUpload={true}
                        showDelete={false}
                        showMakeDefault={false}
                    />
                </div>
            );
        }

        const { currentIndex } = this.state; 

        return images.map((image, index) => {
            const isCurrent = index === currentIndex;
            return (
                <CSSTransition 
                    key={image.id}
                    in={isCurrent}
                    appear={index === 0}
                    classNames={"CarouselImg"}
                    timeout={200}>
                    <div className={styles.imageWrapper}>
                        <img
                            alt="plant"
                            src={image.url}
                            className={classNames(styles.img)} />
                        { isCurrent ? (
                            <CarouselMenu
                                className={classNames(styles.menu)}
                                plantId={this.props.plantId}
                                image={image}
                                showUpload={images.length < MAX_IMAGES}
                                showDelete={true}
                                showMakeDefault={index !== 0}
                            />
                        ) : undefined}
                    </div>
                </CSSTransition>
            )
        })
    }

    render() {
        const { currentIndex, errorOpen } = this.state;
        const { loading, images, error } = this.props;

        return (
            <Fragment>
                <div className={styles.carousel}>
                    <button 
                        aria-label="Previous image"
                        className={classNames(styles.arrowButton, styles.left, "secondary-button", { [styles.hidden]: currentIndex <= 0 || loading })} 
                        onClick={this.getPreviousImage}>
                        <LeftArrow className={styles.icon}/>
                    </button>
                    <div className={classNames(styles.container, { [styles.containerLoading]: loading })}>
                        {!loading ? this.renderImages() : <LoadingDots large={true}/>}
                    </div>
                    <button 
                        aria-label="Next image"
                        className={classNames(styles.arrowButton, styles.right, "secondary-button", { [styles.hidden]: currentIndex >= images.length - 1 || loading })} 
                        onClick={this.getNextImage}>
                        <RightArrow className={styles.icon}/>
                    </button>
                    <div className={classNames(styles.pager, { [styles.hidden]: images.length <= 1 || loading } )}>{currentIndex + 1}/{images.length}</div>
                </div>
                <ReactModal
                    isOpen={errorOpen}
                    onRequestClose={this.closeErrorModal}
                    contentLabel={"Image request failed"}
                    className="small-modal">
                    <section>
                        <h2>Something went wrong:</h2>
                        <p>Error: {error}</p>
                        <div className={"button-group"}>
                            <button
                                onClick={this.closeErrorModal}
                                className={"action-button"}>Okay</button>
                        </div>
                    </section>
                </ReactModal>
            </Fragment>
        )
    }
}

export interface CarouselProps {
    plantId: number;
    images: PlantImage[];
    loading: boolean;
    error?: string;
    fetchImages: (plantId: number) => Promise<ReceiveImagesAction | ImagesErrorAction>;
    clearImageErrors: () => ImagesErrorAction;
}

export interface CarouselState {
    currentIndex: number;
    errorOpen: boolean;
}

const mapStateToProps = (state: AppState) => {
    const { loading, images } = state.plantImages;
    let error;

    if (state.errors.images != null) {
        error = state.errors.images;
    }

    return {
        loading,
        images,
        error
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => {
    return {
        fetchImages: (plantId: number) => dispatch(getPlantImages(plantId)),
        clearImageErrors: () => dispatch(clearImageErrors())
    };
}


const connected = connect(mapStateToProps, mapDispatchToProps)(Carousel)
export { connected as Carousel };
