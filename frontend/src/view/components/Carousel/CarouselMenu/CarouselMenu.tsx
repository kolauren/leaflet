import { PlantImage } from "api/image";
import classNames from "classnames";
import { ReactComponent as EditIcon } from "images/edit.svg";
import { ReactComponent as UploadIcon } from "images/upload.svg";
import React, { Component, Fragment, ReactNode } from "react";
import ReactModal from "react-modal";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { ReceiveImagesAction, setDefaultPlantImage } from "state/image";
import { ActionType } from "state/shared";
import { DropdownMenu } from "view/components/DropdownMenu";
import { ImageDelete } from "../ImageDelete";
import { ImageUpload } from "../ImageUpload";
import styles from "./CarouselMenu.module.scss";

class CarouselMenu extends Component<CarouselMenuProps, CarouselMenuState> {
    constructor(props: any) {
        super(props);
        this.state = { uploadModal: false, deleteModal: false }
    }

    toggleUploadModal = (uploadModal: boolean) => {
        return () => {
            this.setState({ uploadModal });
        }
    }

    toggleDeleteModal = (deleteModal: boolean) => {
        return () => {
            this.setState({ deleteModal });
        }
    }

    handleMakeDefault = () => {
        const { plantId, image, setDefaultPlantImage } = this.props;
        
        if (image != null) {
            setDefaultPlantImage(plantId, image.id);
        }
    }

    renderEditButton(): ReactNode | void {
        const { showDelete, showMakeDefault, plantId, image } = this.props;

        if (showDelete || showMakeDefault) {
            const buttons = [];

            if (showDelete) {
                buttons.push(<button key={1} onClick={this.toggleDeleteModal(true)}>Delete</button>)
            }
    
            if (showMakeDefault) {
                buttons.push(<button key={2} onClick={this.handleMakeDefault}>Make default</button>)
            }

            return (
                <Fragment>
                    <DropdownMenu 
                        icon={<EditIcon />}
                        small={true}>
                        {buttons}
                    </DropdownMenu>
                    <ReactModal 
                        isOpen={this.state.deleteModal}
                        onRequestClose={this.toggleDeleteModal(false)}
                        contentLabel={"Delete"}
                        className={"small-modal"}>
                        <ImageDelete 
                            plantId={plantId}
                            image={image}
                            hideModal={this.toggleDeleteModal(false)}
                        />
                    </ReactModal>
                </Fragment>
            )
        }
    }

    renderUploadButton(): ReactNode | void {
        if (this.props.showUpload) {
            return (
                <Fragment>
                    <button 
                        aria-label="Upload"
                        className={classNames("primary-button", "icon-button", { [styles.uploadButton]: !!this.props.image })}
                        onClick={this.toggleUploadModal(true)}>
                        <UploadIcon />
                    </button>
                    <ReactModal 
                        isOpen={this.state.uploadModal}
                        onRequestClose={this.toggleUploadModal(false)}
                        contentLabel={"Upload"}
                        className={"small-modal"}>
                        <ImageUpload 
                            plantId={this.props.plantId}
                            hideModal={this.toggleUploadModal(false)}
                        />
                    </ReactModal>
                </Fragment>
            );
        }
    }

    render() {
        return (
            <div className={this.props.className}>
                {this.renderUploadButton()}
                {this.renderEditButton()}
            </div>
        );
    }
}

interface CarouselMenuProps {
    className?: string;
    plantId: number;
    image?: PlantImage;
    showUpload: boolean;
    showDelete: boolean;
    showMakeDefault: boolean;
    setDefaultPlantImage: (plantId: number, imageId: number) => Promise<ReceiveImagesAction>;
}

interface CarouselMenuState {
    uploadModal: boolean;
    deleteModal: boolean;
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => {
    return {
        setDefaultPlantImage: (plantId: number, imageId: number) => dispatch(setDefaultPlantImage(plantId, imageId))
    };
}

const connected = connect(null, mapDispatchToProps)(CarouselMenu)
export { connected as CarouselMenu };
