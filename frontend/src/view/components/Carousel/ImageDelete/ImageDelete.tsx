import { PlantImage } from "api/image";
import React, { Component } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { deletePlantImage } from "state/image";
import { ActionType } from "state/shared";

class ImageDelete extends Component<ImageDeleteProps, {}> {
    onDelete = (event: React.MouseEvent<HTMLButtonElement>) => {
        const { deleteImage, plantId, image } = this.props;

        if (image) {
            deleteImage(plantId, image.id);
        }
        
        this.props.hideModal();
    }

    render() {
        return (
            <section>
                <h2>Delete photo</h2>
                <p>Are you sure you want to delete this photo?</p>
                <div className={"button-group"}>
                    <button 
                        onClick={this.onDelete}
                        className={"warning-button"}>Delete</button>
                    <button 
                        onClick={this.props.hideModal}
                        className={"secondary-button"}>Nevermind</button>
                </div>
            </section>
        )
    }
}

interface ImageDeleteProps {
    plantId: number;
    image?: PlantImage;
    deleteImage: (plantId: number, imageId: number) => Promise<ActionType>;
    hideModal: () => any;
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    deleteImage: (plantId: number, imageId: number) => dispatch(deletePlantImage(plantId, imageId))
});

const connected = connect(null, mapDispatchToProps)(ImageDelete)
export { connected as ImageDelete };
