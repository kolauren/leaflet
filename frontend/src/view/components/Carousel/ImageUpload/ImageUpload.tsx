import classNames from "classnames";
import defaultPhoto from "images/default-photo.png";
import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { uploadPlantImage } from "state/image";
import { ActionType } from "state/shared";
import styles from "./ImageUpload.module.scss";

class ImageUpload extends Component<ImageUploadProps, ImageUploadState> {
    constructor(props: ImageUploadProps) {
        super(props);
        this.state = { isDefault: false };
    }

    onFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { files } = event.currentTarget;
        this.setState({ file: files ? files[0] : undefined });
    }

    onUpload = (event: React.MouseEvent<HTMLButtonElement>) => {
        const { file, isDefault } = this.state;

        if (!file) {
            return;
        }

        const { upload, plantId } = this.props;
        upload(plantId, file, isDefault);

        this.props.hideModal();
    }

    onDefaultChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ isDefault: event.target.checked });
    }

    getActionButton(hasFile: boolean): ReactNode {
        if (!hasFile) {
            return (
                <label className={classNames(styles.fileChooser, "action-button")} htmlFor="fileInput">
                    Choose file
                </label>
            )
        }

        return (
            <button
                onClick={this.onUpload}
                className={"action-button"}>Upload</button>
        )
    }

    render() {
        const file = this.state && this.state.file;
        const url = file ? URL.createObjectURL(file) : defaultPhoto;

        return (
            <section>
                <h2>Upload new photo</h2>
                <img className={styles.preview} src={url} alt="plant" />
                <input 
                    className={styles.fileInput}
                    type="file" 
                    name="fileInput"
                    id="fileInput"
                    onChange={this.onFileChange} 
                    accept="image/jpg,image/jpeg,image/png" />
                <label htmlFor="imageDefault" className={classNames(styles.defaultLabel, { [styles.disabled]: !file })}>
                    <input 
                        id="imageDefault"
                        className={styles.defaultCheckbox}
                        type="checkbox"
                        checked={this.state.isDefault}
                        onChange={this.onDefaultChanged}
                        disabled={!file} />
                    Set as default?
                </label>
                <div className={"button-group"}>
                    {this.getActionButton(!!file)}
                    <button 
                        onClick={this.props.hideModal}
                        className={"secondary-button"}>Nevermind</button>
                </div>
            </section>
        )
    }
}

interface ImageUploadProps {
    plantId: number;
    upload: (plantId: number, file: File, isDefault: boolean) => Promise<ActionType>;
    hideModal: () => any;
}

interface ImageUploadState {
    file?: File;
    isDefault: boolean;
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    upload: (plantId: number, file: File, isDefault: boolean) => dispatch(uploadPlantImage(plantId, file, isDefault))
});

const connected = connect(null, mapDispatchToProps)(ImageUpload)
export { connected as ImageUpload };
