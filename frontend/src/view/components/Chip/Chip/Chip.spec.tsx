import { cleanup, render, screen } from "@testing-library/react";
import React from "react";
import { Chip } from "./Chip";

describe("Chip", () => {
    const renderComponent = (title: string, desc: string, value: number) => {
        return render(
            <Chip 
                title={title}
                desc={desc}
                value={value} />
        );
    }

    it("should render a chip with the title and description displayed", () => {
        renderComponent("Testing", "Desc", 0);

        expect(screen.getByText("Testing:"));
        expect(screen.getByText("Desc"));

        cleanup();
    });
});