import classNames from "classnames";
import React from "react";
import styles from "./Chip.module.scss";

export const Chip = ({ title, desc, value, className }: ChipProps ) => {
    return (
        <div className={classNames(
            styles.chip, 
            styles[`value${value}`], 
            className)}>
            <span className={styles.title}>{title}:</span>
            <span className={styles.desc}>{desc}</span>
        </div>
    )
}

interface ChipProps {
    title: string;
    desc: string;
    value: number;
    className?: string;
}