import { EntryMood } from "api/journal";
import classNames from "classnames";
import React from "react";
import { Chip } from "../Chip/Chip";
import styles from "./MoodChip.module.scss";

function getMood(mood?: EntryMood): string {
    switch (mood) {
        case EntryMood.DEAD: 
            return "Dead";
        case EntryMood.SAD: 
            return "Sad";
        case EntryMood.OKAY:
            return "Okay";
        case EntryMood.HAPPY:
            return "Happy";
        default:
            return "N/A";
    }
}

export const MoodChip = ({ title, mood }: MoodChipProps ) => {
    return (
        <Chip 
            className={classNames(styles.mood, styles[`value-${mood}`])}
            title={title}
            desc={getMood(mood)}
            value={0} />
    )
};

interface MoodChipProps {
    title: string;
    mood?: EntryMood;
}