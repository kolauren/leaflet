import classNames from "classnames";
import React, { Component, RefObject } from "react";
import { BodyClickListener } from "../BodyClickListener";
import styles from "./DropdownMenu.module.scss";

class DropdownMenu extends Component<DropdownMenuProps, DropdownMenuState> {
    public dropdownRef: RefObject<HTMLUListElement>;
    
    constructor(props: DropdownMenuProps) {
        super(props);
        this.state = { showDropdown: false };
        this.dropdownRef = React.createRef();
    }

    handleButtonClick = () => {
        this.setState({ showDropdown: !this.state.showDropdown });
    }

    hideDropdown = () => {
        this.setState({ showDropdown: false });
    }

    render() {
        const { icon, children, small } = this.props;
        return (
            <div className={styles.main}>
                <button 
                    className={classNames("primary-button", "icon-button", styles.button, { [styles.large]: !small })} 
                    onClick={this.handleButtonClick}>
                    {icon}
                </button>
                <BodyClickListener
                    attach={this.state.showDropdown}
                    element={this.dropdownRef.current}
                    onBodyClick={this.hideDropdown}>
                    <ul className={classNames(styles.dropdown, { [styles.dropdownShowing]: this.state.showDropdown })}
                        onClick={this.hideDropdown}
                        ref={this.dropdownRef}>
                        { React.Children.map(children, child => <li>{child}</li>) }
                    </ul>
                </BodyClickListener>
            </div>
        )
    }
}

interface DropdownMenuProps {
    icon: React.ReactNode;
    small?: boolean;
}

interface DropdownMenuState {
    showDropdown: boolean;
}

export { DropdownMenu };
