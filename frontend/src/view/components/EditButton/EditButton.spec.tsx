import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import { EditButton } from "./EditButton";

describe("EditButton", () => { 
    const renderComponent = (buttonClass?: string, small?: boolean, onClick?: (event: React.MouseEvent<HTMLButtonElement>) => any) => {
        return render(
            <EditButton 
                buttonClass={buttonClass}
                small={small}
                onClick={onClick} />
        );
    }

    afterEach(() => {
        cleanup();
    });

    it("should render an edit button", () => {
        renderComponent();
        expect(screen.getByLabelText("Edit")).toBeVisible();
    });

    it("should call outer function when edit button is clicked", () => {
        let onClickFn = jest.fn();;
        renderComponent(undefined, undefined, onClickFn);

        const editButton = screen.getByLabelText("Edit");
        userEvent.click(editButton);

        expect(onClickFn).toHaveBeenCalled();
    });

});