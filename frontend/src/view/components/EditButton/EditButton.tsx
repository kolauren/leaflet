import classNames from "classnames";
import { ReactComponent as EditIcon } from "images/edit.svg";
import React, { FunctionComponent } from "react";

const EditButton: FunctionComponent<EditButtonProps> = ({ large, onClick, buttonClass }) => {
    return (
        <button 
            aria-label="Edit"
            className={classNames(buttonClass, "icon-button", "primary-button")}
            onClick={onClick}>
            <EditIcon />
        </button>
    )
}

interface EditButtonProps {
    buttonClass?: string;
    large?: boolean;
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => any;
}

export { EditButton };
