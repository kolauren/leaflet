import { cleanup, render, screen } from "@testing-library/react";
import React, { FormEvent, ReactNode } from "react";
import { Form } from ".";

describe("Form", () => {
    const renderComponent = (
        submitName: string, 
        onSubmit: (event: FormEvent<HTMLFormElement>) => void,
        children: ReactNode[]) => {
        return render(
            <Form 
                onSubmit={onSubmit}
                submitName={submitName}>
                {children}
            </Form>
        );
    }

    afterEach(() => {
        cleanup();
    });

    it("should render a form and any children", () => {
        const nodes = [
            <input key={1} type="text" name="test1" defaultValue="test1" />,
            <input key={2} type="text" name="test2" defaultValue="test2" />
        ];
        renderComponent("testing", () => {}, nodes);

        expect(screen.getByTestId("form")).toBeVisible();
        expect(screen.getByDisplayValue("test1")).toBeVisible();
        expect(screen.getByDisplayValue("test2")).toBeVisible();
    });

    it("should take on the submit name of the one that is passed in", () => {
        renderComponent("testing", () => {}, []);
        expect(screen.getByDisplayValue("testing")).toBeVisible()
    });

    it("should call outer function when submit button is clicked", () => {
        window.HTMLFormElement.prototype.submit = () => {};
        const onSubmitFn = jest.fn();
        renderComponent("testing", onSubmitFn, []);

        // Have to trigger form submit this way because HTMLFormElement.prototype.submit is not implemented in jsdom
        // https://github.com/jsdom/jsdom/issues/1937
        const form = screen.getByTestId("form");
        form.dispatchEvent(new Event("submit"));

        expect(onSubmitFn).toHaveBeenCalled();
    });
})