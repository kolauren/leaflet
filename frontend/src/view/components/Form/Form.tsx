import classNames from "classnames";
import React, { FormEvent, FunctionComponent } from "react";
import styles from "./Form.module.scss";

export const Form: FunctionComponent<FormProps> = ({ onSubmit, submitName, children }) => (
    <form data-testid="form" onSubmit={onSubmit}>
        {children}
        <input 
            className={classNames("action-button", styles.submit)} 
            type="submit" 
            value={submitName} />
    </form>
)

interface FormProps {
    onSubmit: (event: FormEvent<HTMLFormElement>) => void;
    submitName: string;
}
