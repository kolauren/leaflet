import React, { Fragment, FunctionComponent } from "react"
import styles from "./FormError.module.scss"

export const FormError: FunctionComponent<{ showError: boolean }> = ({ showError, children }) => (
    <Fragment>
        {showError ? <div className={styles.error}>{children}</div> : ""}
    </Fragment>
)