import classNames from "classnames"
import React, { FunctionComponent } from "react"
import styles from "./FormField.module.scss"

export const FormField: FunctionComponent<{ showError: boolean }> = ({ showError, children }) => (
    <div className={classNames(styles.formField, { [styles.error]: showError })}>{children}</div>
)