import classNames from "classnames";
import React, { FunctionComponent } from "react";
import styles from "./LoadingDots.module.scss";

const LoadingDots: FunctionComponent<LoadingDotsProps> = ({ className, large }) => (
    <div className={classNames(styles.loadingDots, className, { [styles.large]: large })}>
        <div className={classNames(styles.dot, styles.dot1)}></div>
        <div className={classNames(styles.dot, styles.dot2)}></div>
        <div className={classNames(styles.dot, styles.dot3)}></div>
    </div>
)

interface LoadingDotsProps {
    large?: boolean;
    className?: string;
}

export { LoadingDots };
