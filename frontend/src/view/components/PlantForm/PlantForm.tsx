import { Classification } from "api/classifications";
import { Collection } from "api/collections";
import { PlantForm as ApiPlantForm } from "api/plant";
import classNames from "classnames";
import { formatISO, parseISO, subDays, subMonths, subYears } from "date-fns";
import React, { Component, FormEvent, ReactNode } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { FormState, LeafletForm, Validators } from "state/plantForm";
import { getRooms } from "state/room";
import { ActionType, AppState } from "state/shared";
import { Select } from "../Select";
import styles from "./PlantForm.module.scss";
import { SpeciesAutocomplete } from "./SpeciesAutocomplete";


const FORM_FIELD_NAMES = [
    "name", 
    "species", 
    "collectionId", 
    "acquired", 
    "ageYears", 
    "ageMonths", 
    "ageDays", 
    "classificationId"
];

const INITIAL_ERROR_STATE = { 
    nameError: false,
    speciesError: false,
    roomError: false,
    ageError: false,
    acquiredError: false
};

class PlantForm extends Component<PlantFormProps, PlantFormState> {
    private readonly _form: LeafletForm;
    
    constructor(props: PlantFormProps) {
        super(props);
        
        this.state = { ...INITIAL_ERROR_STATE }

        this._form = new LeafletForm({
            name: [Validators.required],
            room: [Validators.required],
            acquired: [Validators.required]
        }, {
            age: [validateAge],
            species: [validateSpecies]
        });
    }

    componentDidMount() {
        this.props.getRooms();
    }

    handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        this.setState({ ...INITIAL_ERROR_STATE });

        const { _form } = this;
        const formState = LeafletForm.getFormState(event.target as HTMLFormElement, FORM_FIELD_NAMES);      

        if (_form.isValid(formState)) {
            const plant = this.normalizePlantForm(formState as UIPlantForm);
            this.props.handleSubmit({
                id: this.props.plant?.id,
                ...plant
            });
        } else {
            // TODO: fix typecasting
            this.setState({ ...this._form.getErrors(formState) } as any);
        }
    }

    normalizePlantForm(form: UIPlantForm): ApiPlantForm {
        const acquired = parseISO(form.acquired);
        const born = getDateBorn(form.ageDays, form.ageMonths, form.ageYears);
        const { name, classificationId, species } = form;
        let { collectionId } = form;

        if (collectionId && typeof collectionId === "string") {
            collectionId = parseInt(collectionId, 10);
        }
            
        return {
            name,
            species: classificationId ? undefined : species,
            acquiredDate: formatISO(acquired),
            bornDate: formatISO(born),
            collectionId: collectionId as number | undefined,
            classificationId: form.classificationId
        };
    }

    renderRoomOptions(): ReactNode | undefined {
        const { rooms, plant } = this.props;

        const options = rooms.map(room => (
            <option key={room.id} value={room.id}>{room.name}</option>
        ));

        options.push(<option key={"empty"} value="">N/A</option>)

        return (
            <div className={styles.formField}>
                <label 
                    className={styles.label} 
                    htmlFor="room">Room (where your plant is located)</label>
                <Select 
                    defaultValue={plant && plant.collectionId ? plant.collectionId : ""}
                    name="collectionId">
                    {options}
                </Select>                      
            </div>
        );
    }

    render() {
        const {
            nameError,
            speciesError,
            ageError,
            acquiredError
        } = this.state;
        const { props, handleSubmit } = this;
        const { plant, classification } = props;

        return (
            <form onSubmit={handleSubmit}>
                <div className={classNames(styles.formField, { [styles.error]: nameError })}>
                    <label 
                        className={styles.label} 
                        htmlFor="name">Name</label>
                    <input 
                        defaultValue={plant ? plant.name : ""}
                        className={styles.input} 
                        name="name" type="text" 
                        placeholder="e.g. Fluffy" />
                </div>
                <div className={classNames(styles.formField, { [styles.error]: speciesError })}>
                    <label 
                        className={styles.label} 
                        htmlFor="species">Botanical name</label>
                    <SpeciesAutocomplete 
                        inputClassName={styles.input}
                        speciesDefault={plant && plant.species ? plant.species : undefined}
                        classificationDefault={classification} />
                </div>
                <div className={classNames(styles.formField, { [styles.error]: ageError })}>
                    <label className={styles.label} htmlFor="age">Age (approximate)</label>
                    <div className={styles.formFieldRow}>
                        <div className={styles.formFieldCol}>
                            <label 
                                className={styles.label} 
                                htmlFor="ageYears">Years</label>
                            <input 
                                className={styles.input} 
                                defaultValue={plant ? plant.ageYears : 0}
                                name="ageYears" 
                                type="number" 
                                placeholder="0" 
                                min="0" />
                        </div>
                        <div className={styles.formFieldCol}>
                            <label 
                                className={styles.label} 
                                htmlFor="ageMonths">Months</label>
                            <input 
                                className={styles.input} 
                                defaultValue={plant ? plant.ageMonths : 0}
                                name="ageMonths" 
                                type="number" 
                                placeholder="0" 
                                min="0" />
                        </div>
                        <div className={styles.formFieldCol}>
                            <label 
                                className={styles.label} 
                                htmlFor="ageDays">Days</label>
                            <input 
                                className={styles.input} 
                                defaultValue={plant && plant.ageDays ? plant.ageDays : 0}
                                name="ageDays" 
                                type="number" 
                                placeholder="0" 
                                min="0" />
                        </div>
                    </div>
                </div>
                <div className={classNames(styles.formField, { [styles.error]: acquiredError })}>
                    <label 
                        className={styles.label} 
                        htmlFor="acquired">Date acquired</label>
                    <input 
                        defaultValue={plant ? plant.acquired : ""}
                        className={styles.input} 
                        name="acquired" 
                        type="date" />
                </div>
                {this.renderRoomOptions()}
                <input className={"action-button"} type="submit" value="Submit" />
                <Link to={plant ? `/plants/${plant.id}` : `/plants`}>
                    <button className={"primary-button"}>Cancel</button>
                </Link>
            </form>
        )
    }
}

interface PlantFormProps {
    rooms: Collection[];
    plant?: UIPlantForm;
    classification?: Classification;
    handleSubmit: (plant: ApiPlantForm) => any;
    getRooms: () => Promise<ActionType>;
}

interface PlantFormState {
    nameError: boolean;
    speciesError: boolean;
    roomError: boolean;
    ageError: boolean;
    acquiredError: boolean;
}

export interface UIPlantForm {
    id?: number;
    acquired: string;
    ageDays: number;
    ageMonths: number;
    ageYears: number;
    name: string;
    collectionId?: string | number;
    species?: string;
    classificationId?: number;
}

function validateAge(formState: FormState): boolean {
    const { ageYears, ageMonths, ageDays } = formState;
    return (ageYears > 0 || ageMonths > 0 || ageDays > 0);
}

function validateSpecies(formState: FormState): boolean {
    const { species, classificationId } = formState;
    return species || classificationId;
}

function getDateBorn(days: number, months: number, years: number): Date {
    let born = new Date();
    born = subDays(born, days);
    born = subMonths(born, months);
    born = subYears(born, years);
    return born;
}

const mapStateToProps = (state: AppState) => {
    return {
        rooms: state.rooms
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    getRooms: () => dispatch(getRooms()),
})

const connected = connect(mapStateToProps, mapDispatchToProps)(PlantForm)
export { connected as PlantForm };
