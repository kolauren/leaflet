import { Classification } from "api/classifications";
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { fetchClassifications, ReceiveClassificationsAction } from "state/classifications";
import { ActionType, AppState } from "state/shared";
import _ from "underscore";
import { Autocomplete, AutocompleteOption } from "view/components/Autocomplete";

class SpeciesAutocomplete extends Component<SpeciesAutocompleteProps, SpeciesAutocompleteState> {

    private debounced: (value: string) => void;

    constructor(props: SpeciesAutocompleteProps) {
        super(props);
        this.state = { inputValue: "", openAutocomplete: false }
        this.debounced = _.debounce((query: string) => {
            this.props.getClassifications(query);
        }, 300);
    }

    handleInputChange = (value: string) => {
        if (value === "") {
            this.setState({ classificationId: 0 });
        } else {
            this.debounced(value);
        }
        this.setState({ inputValue: value });
    }

    handleOptionSelected = (option: AutocompleteOption) => {
        this.setState({ classificationId: option.id });
    }

    getSpecies(): string {
        const { speciesDefault } = this.props;
        const { inputValue } = this.state;
        return inputValue ? inputValue : (speciesDefault || "");
    }

    getClassificationId(): number {
        const { classificationDefault } = this.props;
        const { classificationId } = this.state;
        return classificationId != null ? classificationId : (classificationDefault ? classificationDefault.id : 0);
    }

    getDefaultValue(): string {
        const { speciesDefault, classificationDefault } = this.props;
        
        if (!classificationDefault) {
            return speciesDefault || "";
        }

        return classificationDefault.scientificName;
    }

    render() {
        const { inputClassName } = this.props;

        return (
            <Fragment>
                <Autocomplete
                    name={"autocomplete"}
                    placeholder={"e.g. Monstera deliciosa"}
                    options={this.props.options}
                    onInputChange={this.handleInputChange}
                    onOptionSelected={this.handleOptionSelected}
                    inputClassName={inputClassName}
                    defaultValue={this.getDefaultValue()}>
                </Autocomplete>
                <input 
                    name="species"
                    type="text"
                    value={this.getSpecies()}
                    readOnly
                    hidden/>
                <input 
                    name="classificationId"
                    type="number"
                    value={this.getClassificationId()} 
                    readOnly
                    hidden/>
            </Fragment>
        )
    }
}

interface SpeciesAutocompleteState {
    inputValue: string;
    openAutocomplete: boolean;
    species?: string;
    classificationId?: number;
}

interface SpeciesAutocompleteProps {
    speciesDefault?: string;
    classificationDefault?: Classification;
    inputClassName: string;
    options: AutocompleteOption[];
    getClassifications: (query: string) => Promise<ReceiveClassificationsAction>;
}

const mapStateToProps = (state: AppState) => {
    const sliced = (state.classifications || []).slice(0, 20);
    return {
        options: sliced.map(c => {
            return {
                title: c.scientificName,
                subtitle: c.commonName || "",
                id: c.id
            };
        })
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    getClassifications: (query: string) => dispatch(fetchClassifications(query)),
})

const connected = connect(mapStateToProps, mapDispatchToProps)(SpeciesAutocomplete)
export { connected as SpeciesAutocomplete };
