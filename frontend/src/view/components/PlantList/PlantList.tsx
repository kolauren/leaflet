import { Collection } from "api/collections";
import { Plant } from "api/plant";
import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { fetchPlants } from "state/plant";
import { ALL_PLANTS_FILTER_STATE } from "state/room";
import { ActionType, AppState, PlantListAppState } from "state/shared";
import { RoomFilterButtons } from "view/components/PlantList/RoomFilterButtons";
import { getSpeciesNameShort } from "view/utils";
import { Cards } from "../Cards";
import { LoadingDots } from "../LoadingDots";
import styles from "./PlantList.module.scss";
import { RoomEditButton } from "./RoomEditButton";

class PlantList extends Component<PlantListProps, PlantListState> {
    constructor(props: PlantListProps) {
        super(props);
        this.state = { searchFilter: "", plantsByRoom: {} };
    }

    componentDidMount() {
        this.props.fetchPlants();
    }
    
    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ searchFilter: event.target.value });
    }

    renderPlants(plants: Plant[]): ReactNode {
        const cards = plants.map((plant: Plant) => {
            const { name, id, defaultImage } = plant;
            return {
                title: name,
                subTitle: getSpeciesNameShort(plant),
                link: `/plants/${id}`,
                image: defaultImage ? defaultImage.url : undefined
            };
        });

        return <Cards cards={cards}/>
    }

    renderPlantsByFilter(): ReactNode[] | ReactNode {
        const roomId = this.props.currentRoom.id;
        const { all, byRoom } = this.props.plants;
        const allPlants = all || [];
        const searchFilter = this.state.searchFilter.toLowerCase();

        const filteredPlants = searchFilter
            ? allPlants.filter((plant: Plant) => plant.name.toLowerCase().indexOf(searchFilter) > -1 || (plant.species || "").toLowerCase().indexOf(searchFilter) > -1)
            : roomId === ALL_PLANTS_FILTER_STATE.id ? allPlants : byRoom[roomId];

        return filteredPlants && filteredPlants.length
            ? this.renderPlants(filteredPlants)
            : <div className={styles.notFound}>No plants in this room.</div>;
    }

    renderRoomsFilter(): ReactNode | string {
        if (this.state.searchFilter) {
            return "";
        }

        const selected = this.props.currentRoom;

        return (
            <div className={styles.roomFilters}>
                <RoomFilterButtons 
                    className={styles.roomFilterButtons}
                    selected={selected} />
                {this.props.currentRoom.id === -1 ? "" : <RoomEditButton selected={selected} />}
            </div>
        )
    }

    render() {
        const { plants } = this.props;
        let renderedPlants;

        if (!plants || !plants.all) {
            renderedPlants = <LoadingDots large={true} className={styles.loading} />
        } else {
            renderedPlants = plants.all.length <= 0 ? <div className={styles.notFound}>You have no plants in your family. <Link to="/plants/add">Add a new plant</Link></div> : this.renderPlantsByFilter();
        }

        return (
            <section className={styles.main}>
                <input 
                    className={styles.searchInput} 
                    type="text" 
                    placeholder="Search" 
                    onChange={this.handleInputChange} />
                {this.renderRoomsFilter()}
                <div className={styles.plants}>
                    {renderedPlants}
                </div>
            </section>
        );
    }
}

interface PlantListProps {
    fetchPlants: () => Promise<ActionType>;
    plants: PlantListAppState;
    currentRoom: Collection;
}

interface PlantListState {
    searchFilter: string;
    plantsByRoom: { [id: number]: Plant[] }
}

const mapStateToProps = (state: AppState) => {
    return {
        plants: state.plants || {},
        currentRoom: state.currentRoom
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    fetchPlants: () => dispatch(fetchPlants())
})

const connected = connect(mapStateToProps, mapDispatchToProps)(PlantList)
export { connected as PlantList };
