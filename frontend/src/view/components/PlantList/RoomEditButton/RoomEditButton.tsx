import { Collection } from "api/collections";
import { ReactComponent as EditIcon } from "images/edit.svg";
import React, { Component, Fragment } from "react";
import ReactModal from "react-modal";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { deleteRoom, showEditRoom } from "state/room";
import { ActionType } from "state/shared";
import { DropdownMenu } from "view/components/DropdownMenu";

class RoomFilterButtons extends Component<RoomEditButtonProps, RoomEditButtonState> {
    constructor(props: RoomEditButtonProps) {
        super(props);
        this.state = { showDelete: false };
    }

    handleToggleDelete = (showDelete: boolean) => {
        return () => {
            this.setState({ showDelete });
        }
    }

    handleEditRoomName = () => {
        this.props.showEditRoom();
    }

    handleDeleteRoom = () => {
        this.props.deleteRoom(this.props.selected);
        this.setState({ showDelete: false });
    }

    render() {
        const { selected } = this.props;

        return (
            <Fragment>
                <DropdownMenu icon={<EditIcon />}>
                    <button onClick={this.handleEditRoomName}>Edit room name</button>
                    <button onClick={this.handleToggleDelete(true)}>Delete room</button>
                </DropdownMenu>
                <ReactModal 
                    isOpen={this.state.showDelete}
                    onRequestClose={this.handleToggleDelete(false)}
                    contentLabel={"Delete room"}
                    className={"small-modal"}>
                    <section>
                        <h2>Delete "{selected.name}"?</h2>
                        <p>Plants currently located in "{selected.name}" will move to "All Plants".</p>
                        <div className={"button-group"}>
                            <button
                                onClick={this.handleDeleteRoom}
                                className={"warning-button"}>Yes, delete this room</button>
                            <button 
                                onClick={this.handleToggleDelete(false)}
                                className={"secondary-button"}>Nevermind</button>
                        </div>
                    </section>
                </ReactModal>
            </Fragment>
        )
    }
}

interface RoomEditButtonProps {
    selected: Collection;
    showEditRoom: () => ActionType;
    deleteRoom: (room: Collection) => Promise<ActionType>;
}

interface RoomEditButtonState {
    showDelete: boolean;
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    showEditRoom: () => dispatch(showEditRoom()),
    deleteRoom: (room: Collection) => dispatch(deleteRoom(room)),
})

const connected = connect(null, mapDispatchToProps)(RoomFilterButtons)
export { connected as RoomEditButton };

