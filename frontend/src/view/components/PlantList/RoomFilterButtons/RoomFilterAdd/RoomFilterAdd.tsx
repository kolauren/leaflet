import classNames from "classnames";
import { ReactComponent as CheckIcon } from "images/tick.svg";
import React, { Component, RefObject } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { addRoom, hideAllRoomActions } from "state/room";
import { ActionType, AppState } from "state/shared";
import { BodyClickListener } from "view/components/BodyClickListener";
import styles from "./RoomFilterAdd.module.scss";

class RoomFilterAdd extends Component<RoomFilterAddProps, RoomFilterAddState> {
    public addInputRef: RefObject<HTMLSpanElement>;

    constructor(props: RoomFilterAddProps) {
        super(props);
        this.state = { 
            newRoomName: "", 
        };

        this.addInputRef = React.createRef();
    }

    handleEnterKey = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            this.handleSaveNewRoom();
        }
    }

    handleSaveNewRoom = () => {
        const { newRoomName } = this.state;
        if (newRoomName) {
            this.props.addRoom(newRoomName);
        }
        this.props.hideAllActions();
    }

    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ newRoomName: event.target.value });
    }

    handleCancelClick = () => {
        this.setState({ newRoomName: "" });
        this.props.hideAllActions();
    }

    render() {
        return (
            <BodyClickListener
                attach={true}
                element={this.addInputRef.current}
                onBodyClick={this.handleCancelClick}>
                <span className={styles.add} ref={this.addInputRef}>
                    <input 
                        className={styles.input} 
                        autoFocus 
                        type="text" 
                        placeholder="Enter room name" 
                        onKeyDown={this.handleEnterKey}
                        onChange={this.handleInputChange} />
                    <button 
                        className={classNames(styles.smallButton, styles.saveButton)} 
                        onClick={this.handleSaveNewRoom}><CheckIcon className={styles.checkIcon} /></button>
                    <button 
                        className={classNames(styles.smallButton, styles.cancelButton)} 
                        onClick={this.handleCancelClick}>&#10005;</button>
                </span>
            </BodyClickListener>

        )
    }
}

interface RoomFilterAddProps {
    addRoom: (room: string) => Promise<ActionType>;
    hideAllActions: () => ActionType;
}

interface RoomFilterAddState {
    newRoomName: string;
}

const mapStateToProps = (state: AppState) => ({
})

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    addRoom: (room: string) => dispatch(addRoom(room)),
    hideAllActions: () => dispatch(hideAllRoomActions()),
})

const connected = connect(mapStateToProps, mapDispatchToProps)(RoomFilterAdd)
export { connected as RoomFilterAdd };
