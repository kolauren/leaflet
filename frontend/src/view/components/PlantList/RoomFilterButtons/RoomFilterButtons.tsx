import { Collection } from "api/collections";
import classNames from "classnames";
import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { CSSTransition } from "react-transition-group";
import { ThunkDispatch } from "redux-thunk";
import { ALL_PLANTS_FILTER_STATE, filterPlantsByRoom, getRooms } from "state/room";
import { ActionType, AppState } from "state/shared";
import { LoadingDots } from "view/components/LoadingDots";
import { RoomFilterAdd } from "./RoomFilterAdd";
import styles from "./RoomFilterButtons.module.scss";
import { RoomFilterEdit } from "./RoomFilterEdit";

class RoomFilterButtons extends Component<RoomFilterButtonsProps, { }> {
    componentDidMount() {
        this.props.getRooms();
    }

    handleFilterClick = (room: Collection) => {
        return () => {
            this.props.filterPlantsByRoom(room);
        }
    }

    renderRoomFilterButton(currentRoom: Collection, selected: Collection): ReactNode {
        const currentIsSelected = selected.id === currentRoom.id;
        
        if (this.props.showEditRoom && currentIsSelected) {
            return <RoomFilterEdit key={currentRoom.id} currentRoom={currentRoom} />
        }

        const { loading } = this.props;

        return (
            <button 
                key={currentRoom.id} 
                className={classNames(styles.button, { [styles["button--active"]]: currentIsSelected && !loading})} 
                onClick={this.handleFilterClick(currentRoom)}>{currentIsSelected && loading ? <LoadingDots /> : currentRoom.name}</button>
        )
    }

    render() {
        const { rooms, selected, showAddRoom, className } = this.props;

        return (
            <CSSTransition
                in={showAddRoom}
                classNames="RoomFilterButtons"
                timeout={100}>
                <div className={className}>
                    {rooms.map((room: Collection) => this.renderRoomFilterButton(room, selected))}
                    {showAddRoom ? <RoomFilterAdd /> : ""}
                </div>
            </CSSTransition>
        )
    }
}

interface RoomFilterButtonsProps {
    className: string;
    showAddRoom: boolean;
    showEditRoom: boolean;
    loading: boolean;
    rooms: Collection[];
    selected: Collection;
    filterPlantsByRoom: (room: Collection) => ActionType;
    getRooms: () => Promise<ActionType>;
}

const mapStateToProps = (state: AppState) => ({
    rooms: [ALL_PLANTS_FILTER_STATE, ...state.rooms],
    showAddRoom: state.modifyRooms.add,
    showEditRoom: state.modifyRooms.edit,
    loading: state.modifyRooms.loading
})

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    filterPlantsByRoom: (room: Collection) => dispatch(filterPlantsByRoom(room)),
    getRooms: () => dispatch(getRooms())
})

const connected = connect(mapStateToProps, mapDispatchToProps)(RoomFilterButtons)
export { connected as RoomFilterButtons };
