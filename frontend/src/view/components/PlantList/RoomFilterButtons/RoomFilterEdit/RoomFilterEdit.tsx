import { Collection } from "api/collections";
import React, { Component, RefObject } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { editRoom, hideAllRoomActions, showLoadingRoom } from "state/room";
import { ActionType, AppState } from "state/shared";
import { BodyClickListener } from "view/components/BodyClickListener";
import styles from "./RoomFilterEdit.module.scss";

class RoomFilterEdit extends Component<RoomFilterEditProps, RoomFilterEditState> {
    public editInputRef: RefObject<HTMLInputElement>;

    constructor(props: RoomFilterEditProps) {
        super(props);
        this.state = { 
            newRoomName: "", 
        };

        this.editInputRef = React.createRef();
    }

    handleEnterKey = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            this.handleSaveRoom();
        }
    }

    handleBodyClick = (event: MouseEvent) => {
        if (!this.editInputRef.current!.contains(event.target as Node)) {
            this.handleSaveRoom();
        }
    }

    handleSaveRoom = () => {
        const { newRoomName } = this.state;
        const { currentRoom } = this.props;

        if (newRoomName && newRoomName !== currentRoom.name) {
            this.props.editRoom({ id: currentRoom.id, name: newRoomName });
            this.props.showLoadingRoom();
        } else {
            this.props.hideAllActions();
        }
    }

    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ newRoomName: event.target.value });
    }

    render() {
        const { currentRoom } = this.props;

        return (
            <BodyClickListener
                attach={true}
                element={this.editInputRef.current}
                onBodyClick={this.handleBodyClick}>
                <input 
                    ref={this.editInputRef}
                    className={styles.input}
                    autoFocus 
                    type="text" 
                    onKeyDown={this.handleEnterKey}
                    onChange={this.handleInputChange}
                    defaultValue={currentRoom.name} />
            </BodyClickListener>

        )
    }
}

interface RoomFilterEditProps {
    currentRoom: Collection;
    editRoom: (room: Collection) => Promise<ActionType>;
    hideAllActions: () => ActionType;
    showLoadingRoom: () => ActionType;
}

interface RoomFilterEditState {
    newRoomName: string;
}

const mapStateToProps = (state: AppState) => ({
})

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    editRoom: (room: Collection) => dispatch(editRoom(room)),
    hideAllActions: () => dispatch(hideAllRoomActions()),
    showLoadingRoom: () => dispatch(showLoadingRoom())
})

const connected = connect(mapStateToProps, mapDispatchToProps)(RoomFilterEdit)
export { connected as RoomFilterEdit };
