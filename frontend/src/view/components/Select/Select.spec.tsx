import { cleanup, fireEvent, render, screen } from "@testing-library/react";
import React, { ReactNode } from "react";
import { Select } from ".";

const options = [
    <option key={1} value="test1">test1</option>,
    <option key={2} value="test2">test2</option>,
    <option key={3} value="test3">test3</option>
];

describe("Select", () => {
    const renderComponent = (
        defaultValue: string|number,
        name: string,
        children: ReactNode[],
        onChange?: (event: React.ChangeEvent<HTMLSelectElement>) => any) => {
        return render(
            <Select 
                defaultValue={defaultValue} 
                name={name}
                onChange={onChange}>
                {children}
            </Select>
        );
    }

    afterEach(() => {
        cleanup();
    });

    it("should render a select" , () => {
        renderComponent("testing", "testing", options);
        expect(screen.getByTestId("select"));
    });

    it("should select the correct default value", () => {
        renderComponent("test2", "testing", options);
        expect(screen.getByDisplayValue("test2")).toBeVisible();
    });

    it("should trigger the outer function when the selection changes", () => {
        const selectFn = jest.fn();
        renderComponent("test2", "testing", options, selectFn);

        const select = screen.getByDisplayValue("test2");
        fireEvent.change(select, { target: { value: "test3"}});
        expect(selectFn).toHaveBeenCalled();
    });
});