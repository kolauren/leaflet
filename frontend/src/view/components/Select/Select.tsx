import classNames from "classnames";
import { ReactComponent as LeftArrow } from "images/left-arrow.svg";
import React, { FunctionComponent } from "react";
import styles from "./Select.module.scss";

const Select: FunctionComponent<SelectProps> = ({ selectClass, defaultValue, name, onChange, children }) => {
    return (
        <div className={styles.selectWrapper}>
            <select
                data-testid="select"
                className={classNames(selectClass, styles.select)}
                defaultValue={defaultValue}
                name={name}
                onChange={onChange}>
                {children}
            </select>
            <LeftArrow className={styles.svg} />
        </div>
    )
}

interface SelectProps {
    selectClass?: string;
    defaultValue: string|number;
    name: string;
    onChange?: (event: React.ChangeEvent<HTMLSelectElement>) => any;
}

export { Select };
