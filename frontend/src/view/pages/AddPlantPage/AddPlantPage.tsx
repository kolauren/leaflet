import { PlantForm as ApiPlantForm } from "api/plant";
import React, { Component } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { addPlant, PlantFormErrorAction } from "state/plant";
import { ActionType } from "state/shared";
import { PlantForm } from "view/components/PlantForm";
import styles from "./AddPlantPage.module.scss";

class AddPlantPage extends Component<AddPlantPageProps, {}> {
    handleSubmit = (plant: ApiPlantForm) => {
        const { addPlant, history } = this.props;
        addPlant(plant, history);
    }

    render() {
        return (
            <div className={styles.main}>
                <h1>{"Add a plant"}</h1>
                <PlantForm handleSubmit={this.handleSubmit} />
            </div>
        );
    }
}

interface AddPlantPageProps extends RouteComponentProps<any> {
    addPlant: (plant: ApiPlantForm, history: any) => Promise<PlantFormErrorAction | void>;
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    addPlant: (plant: ApiPlantForm, history: any) => dispatch(addPlant(plant, history))
})

const connected = withRouter(connect(null, mapDispatchToProps)(AddPlantPage))
export { connected as AddPlantPage };

