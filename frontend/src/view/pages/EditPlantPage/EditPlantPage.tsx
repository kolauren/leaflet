import { Classification } from "api/classifications";
import { Plant, PlantForm as ApiPlantForm } from "api/plant";
import classNames from "classnames";
import React, { Component } from "react";
import ReactModal from "react-modal";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { deletePlant, editPlant, fetchPlantDetails, PlantFormErrorAction } from "state/plant";
import { ActionType, AppState } from "state/shared";
import { LoadingDots } from "view/components/LoadingDots";
import { PlantForm, UIPlantForm } from "view/components/PlantForm";
import { getAgeFromDate, getIdFromProps } from "view/utils";
import styles from "./EditPlantPage.module.scss";

class EditPlantPage extends Component<EditPlantPageProps, EditPlantPageState> {
    constructor(props: EditPlantPageProps) {
        super(props);
        this.state = { showDelete: false };
    }

    componentDidMount() {
        const id = getIdFromProps(this.props);

        if (!isNaN(id)) {
            this.props.fetchPlantDetails(id);
        }
    }

    handleToggleDelete = (showDelete: boolean) => {
        return () => {
            this.setState({ showDelete: showDelete });
        }
    }

    handleDelete = () => {
        const { deletePlant, plant, history } = this.props;

        if (plant?.id) {
            deletePlant(plant!.id, history);
        }
    }

    handleSubmit = (plant: ApiPlantForm) => {
        const { editPlant, history } = this.props;
        editPlant(plant, history);
    }

    render() {
        const { plant, classification } = this.props;
        const deleteButton = <button 
            className={classNames("warning-button", styles.delete)} 
            onClick={this.handleToggleDelete(true)}>Delete {plant?.name}</button>;

        return (
            <div className={styles.main}>
                <div className={styles.header}>
                    <h1>{"Edit plant"}</h1>
                    {plant ? deleteButton : ""} 
                </div>
                {plant 
                    ? <PlantForm handleSubmit={this.handleSubmit} plant={plant} classification={classification} /> 
                    : <LoadingDots className={styles.loading} large={true} /> }
                <ReactModal 
                    isOpen={this.state.showDelete}
                    onRequestClose={this.handleToggleDelete(false)}
                    contentLabel={"Delete room"}
                    className={"small-modal"}>
                    <section>
                        <h2>Delete {plant?.name}?</h2>
                        <p>Would you like to delete {plant?.name}?</p>
                        <div className={"button-group"}>
                            <button
                                onClick={this.handleDelete}
                                className={"warning-button"}>Yes, delete this plant</button>
                            <button 
                                onClick={this.handleToggleDelete(false)}
                                className={"secondary-button"}>Nevermind</button>
                        </div>
                    </section>
                </ReactModal>
            </div>
        );
    }
}

interface EditPlantPageProps extends RouteComponentProps<any> {
    plant?: UIPlantForm;
    classification?: Classification;
    fetchPlantDetails: (id: number) => Promise<ActionType>;
    editPlant: (plant: ApiPlantForm, history: any) => Promise<PlantFormErrorAction | void>;
    deletePlant: (id: number, history: any) => Promise<PlantFormErrorAction | void>;
}

interface EditPlantPageState {
    showDelete: boolean;
}

function mapPlantToForm(plantDetails: Plant): UIPlantForm | undefined {
    if (!plantDetails || !plantDetails.name) {
        return undefined;
    }

    const ages = getAgeFromDate(plantDetails.bornDate);
    const { classification, collection } = plantDetails;

    return {
        id: plantDetails.id,
        name: plantDetails.name,
        acquired: plantDetails.acquiredDate,
        collectionId: collection ? collection.id : undefined,
        species: !classification ? plantDetails.species : undefined,
        ageDays: ages.days,
        ageMonths: ages.months,
        ageYears: ages.years,
        classificationId: classification ? classification.id : undefined
    }
}

const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<{ id: string }>) => {
    return {
        plant: mapPlantToForm(state.plantDetails),
        classification: state.plantDetails.classification
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    editPlant: (plant: ApiPlantForm, history: any) => dispatch(editPlant(plant, history)),
    deletePlant: (id: number, history: any) => dispatch(deletePlant(id, history)),
    fetchPlantDetails: (id: number) => dispatch(fetchPlantDetails(id)),
})

const connected = withRouter(connect(mapStateToProps, mapDispatchToProps)(EditPlantPage))
export { connected as EditPlantPage };

