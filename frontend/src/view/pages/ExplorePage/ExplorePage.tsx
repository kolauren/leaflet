import { Family } from "api/family/models";
import React from "react";
import { Body } from "view/components/Body";
import { CardItem, Cards } from "view/components/Cards";
import styles from "./ExplorePage.module.scss";

const families = [
    {
        name: "Abbey",
        amount: 14,
        location: "California"
    },
    {
        name: "Brian",
        amount: 28,
        location: "Berlin"
    },
    {
        name: "Christina",
        amount: 384,
        location: "Vancouver"
    },
    {
        name: "Tori",
        amount: 2349,
        location: "Chicago"
    },
    {
        name: "Brian2",
        amount: 28,
        location: "Berlin"
    },
    {
        name: "Christina2",
        amount: 384,
        location: "Vancouver"
    },
    {
        name: "Tori2",
        amount: 2349,
        location: "Chicago"
    },
    {
        name: "Brian3",
        amount: 28,
        location: "Berlin"
    },
    {
        name: "Christina3",
        amount: 384,
        location: "Vancouver"
    },
    {
        name: "Tori3",
        amount: 2349,
        location: "Chicago"
    },
    {
        name: "Tori4",
        amount: 2349,
        location: "Chicago"
    },
    {
        name: "Tori45",
        amount: 2349,
        location: "Chicago"
    }
]

const ExplorePage = () => {
    return (
        <Body>
            <h1 className={styles.title}>Explore</h1>
            <Cards cards={getFamiliesAsCards(families)} />
            <div className={styles.loadMore}>
                <button className={"primary-button"}>Load more</button>        
            </div>
        </Body>
    )
}

function getFamiliesAsCards(families: Family[]): CardItem[] {
    return families.map(family => ({ 
        title: `${family.name} (${family.amount})`,
        subTitle: family.location,
        link: "/"
    }))
}

export { ExplorePage };

