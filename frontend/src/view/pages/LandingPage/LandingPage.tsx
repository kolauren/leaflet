import { Family } from "api/family/models"
import bg from "images/hero-image.jpg"
import React, { Fragment } from "react"
import { Link } from "react-router-dom"
import { CardItem, Cards } from "view/components/Cards"
import styles from "./LandingPage.module.scss"

const families = [
    {
        name: "Abbey",
        amount: 14,
        location: "California"
    },
    {
        name: "Brian",
        amount: 28,
        location: "Berlin"
    },
    {
        name: "Christina",
        amount: 384,
        location: "Vancouver"
    },
    {
        name: "Tori",
        amount: 2349,
        location: "Chicago"
    },
    {
        name: "Brian2",
        amount: 28,
        location: "Berlin"
    }
]

const LandingPage = () => {
    return (
        <Fragment>
            <section className={styles.hero} style={{ backgroundImage: `url(${bg})` }}>
                <div className={styles.desc}>
                    <span className={styles.line}>
                        <span className={styles.emphasize}>Organize</span> your indoor plants
                    </span>
                    <span className={styles.line}>
                        <span className={styles.emphasize}>Track</span> their growth
                    </span>
                    <span className={styles.line}>
                        <span className={styles.emphasize}>Show off</span> your family
                    </span>
                    <Link to="/signup">
                        <button className={styles.signup}>Sign up now</button>
                    </Link>
                </div>
            </section>
            <section className={styles.recentlyAdded}>
                <h1>Recently added</h1>
                <Cards cards={getFamiliesAsCards(families)} />
                <Link className={styles.viewMore} to="/explore">
                    <button className={"primary-button"}>View more</button>
                </Link>
            </section>
        </Fragment>
    )
}

function getFamiliesAsCards(families: Family[]): CardItem[] {
    return families.map(family => ({ 
        title: `${family.name} (${family.amount})`,
        subTitle: family.location,
        link: "/"
    }))
}

export { LandingPage }
