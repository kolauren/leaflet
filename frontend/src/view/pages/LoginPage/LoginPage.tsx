import { LoginForm } from "api/auth";
import React, { Component, FormEvent } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { clearError, login, LoginErrorAction } from "state/auth";
import { LeafletForm, Validators } from "state/plantForm";
import { ActionType, AppState } from "state/shared";
import { Body } from "view/components/Body";
import { Form, FormError, FormField } from "view/components/Form";
import styles from "./LoginPage.module.scss";

const FORM_FIELD_NAMES = ["email", "password"];

class LoginPage extends Component<LoginProps, LoginState> {
    private readonly _form: LeafletForm;

    constructor(props: any) {
        super(props);

        this.state = {
            emailError: false,
            passwordError: false
        };

        this._form = new LeafletForm({ 
            email: [Validators.required],
            password: [Validators.required]
        });
    }

    componentWillUnmount() {
        this.props.clearError();
    }

    handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const { _form } = this;
        const formState = LeafletForm.getFormState(event.target as HTMLFormElement, FORM_FIELD_NAMES);  
             
        if (_form.isValid(formState)) {
            this.props.login(formState as LoginForm);
        } else {
            // TODO: fix typecasting
            this.setState({ ...this._form.getErrors(formState) } as any);
        }
    }
    
    render() {
        const {
            emailError,
            passwordError
          } = this.state;

        const { serverError } = this.props;

        return (
            <Body>
                <div className={styles.main}>
                    <div className={styles.form}>
                        <h1>Login</h1>
                        <div>Don"t have an account? <Link to="/signup">Sign up</Link></div>
                        <Form onSubmit={this.handleSubmit} submitName={"Login"}>
                            <FormField showError={emailError}>
                                <input name="email" type="email" placeholder="E-mail" />
                            </FormField>
                            <FormField showError={passwordError}>
                                <input name="password" type="password" placeholder="Your password" />
                            </FormField>
                        </Form>
                        <FormError showError={!!serverError}>{`Error: ${serverError}`}</FormError>
                    </div>
                </div>
            </Body>
        )
    }
}

interface LoginState {
    emailError: boolean;
    passwordError: boolean;
}

interface LoginProps {
    serverError: string;
    login: (form: LoginForm) => Promise<LoginErrorAction|void>;
    clearError: () => LoginErrorAction;
}

const mapStateToProps = (state: AppState) => {
    return {
        serverError: state.errors.login
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    login: (form: LoginForm) => dispatch(login(form)),
    clearError: () => dispatch(clearError()),
})


const connected = connect(mapStateToProps, mapDispatchToProps)(LoginPage)
export { connected as LoginPage };
