import React from "react";
import { Link } from "react-router-dom";
import { Body } from "view/components/Body";
import styles from "./NoMatchPage.module.scss";

const NoMatchPage = () => (
    <Body>
        <section className={styles.wrapper}>
            <h1 className={styles.title}>oops!</h1>
            <span>This page does not exist. <Link to="/">Go back home</Link></span>
        </section>
    </Body>
)

export { NoMatchPage };
