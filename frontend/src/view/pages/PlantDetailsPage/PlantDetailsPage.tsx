import { EntryMood } from "api/journal";
import { Plant } from "api/plant";
import classNames from "classnames";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { clearPlantDetails, fetchPlantDetails } from "state/plant";
import { ActionType, AppState } from "state/shared";
import { Body } from "view/components/Body";
import { Calendar } from "view/components/Calendar";
import { Carousel } from "view/components/Carousel";
import { Chip, MoodChip } from "view/components/Chip";
import { EditButton } from "view/components/EditButton";
import { LoadingDots } from "view/components/LoadingDots";
import { getAgeString, getDateString, getIdFromProps, getSpeciesNameLong } from "view/utils";
import { NoMatchPage } from "..";
import styles from "./PlantDetailsPage.module.scss";


class PlantDetailsPage extends Component<PlantDetailsPageProps, PlantDetailsPageState> {
    constructor(props: PlantDetailsPageProps) {
        super(props);
        this.state = { loading: true };
    }

    componentDidMount() {       
        const id = getIdFromProps(this.props);

        if (!isNaN(id)) {
            this.props.fetchPlantInfo(id);
        }
    }

    componentWillUnmount() {
        this.props.clearPlantInfo();
    }

    render() {
        const { plantDetails, error } = this.props;
        const id = parseInt(this.props.match.params.id, 10);

        if (isNaN(id) || error) {
            return (
                <NoMatchPage />
            )
        }

        if (!plantDetails || !plantDetails.name) {
            return (
                <Body>
                    <LoadingDots className={styles.loading} large={true} />
                </Body>
            )
        }
    
        return (
            <Body>
                <section className={styles.main}>
                    <div className={styles.mainLeft}>
                        <Carousel plantId={plantDetails.id} />
                    </div>
                    <div className={classNames(styles.mainRight, styles.card)}>
                        <div className={styles.header}>
                            <h1>{plantDetails.name}</h1>
                            <Link to={`/plants/${id}/edit`} className={styles.editButton}>
                                <EditButton />
                            </Link>
                        </div>
                        <div className={styles.detailLine}>
                            <span className={styles.subTitle}>Botanical name</span>
                            <span>{getSpeciesNameLong(plantDetails)}</span>
                        </div>
                        <div className={styles.detailLine}>
                            <span className={styles.subTitle}>Age</span>
                            <span>{getAgeString(plantDetails.bornDate)}</span>
                        </div>
                        <div className={styles.detailLine}>
                            <span className={styles.subTitle}>Acquired</span>
                            <span>{getDateString(plantDetails.acquiredDate)}</span>
                        </div>
                        <div className={styles.detailLine}>
                            <span className={styles.subTitle}>Located in</span>
                            <span>{plantDetails.collection ? plantDetails.collection.name : "Uncategorized"}</span>
                        </div>
                        <div className={styles.chips}>
                            <Chip 
                                className={styles.bottomBuffer}
                                title={"Last watered"} 
                                desc={"Jan 1, 2021"} 
                                value={1} />
                            <MoodChip 
                                title={"Latest mood"} 
                                mood={EntryMood.HAPPY} />
                        </div>
                    </div>
                </section>
                <section className={classNames(styles.card, styles.calendar)}>
                    <Calendar plantId={id} />
                </section>
            </Body>
        )
    }
}

interface PlantDetailsPageProps extends RouteComponentProps<{ id: string }> {
    fetchPlantInfo: (id: number) => Promise<ActionType>;
    clearPlantInfo: () => ActionType;
    plantDetails: Plant;
    error?: string;
}

interface PlantDetailsPageState {
    loading: boolean;
}

const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<{ id: string }>) => {    
    return {
        ...ownProps,
        plantDetails: state.plantDetails,
        error: state.errors.plant
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => {
    return {
        fetchPlantInfo: (id: number) => dispatch(fetchPlantDetails(id)),
        clearPlantInfo: () => dispatch(clearPlantDetails())
    };
}

const connected = withRouter(connect(mapStateToProps, mapDispatchToProps)(PlantDetailsPage))
export { connected as PlantDetailsPage };
