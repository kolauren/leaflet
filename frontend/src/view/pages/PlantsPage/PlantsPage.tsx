import { User } from "api/user";
import classNames from "classnames";
import bg from "images/hero-image.jpg";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { showAddRoom } from "state/room";
import { ActionType, AppState } from "state/shared";
import { Body } from "view/components/Body";
import { PlantList } from "view/components/PlantList";
import styles from "./PlantsPage.module.scss";

class PlantsPage extends Component<PlantsPageProps> {    
    handleAddRoomClick = () => {
        this.props.showAddRoom();
    } 

    render() {
        const { currentUser } = this.props;
        const firstName = currentUser ? currentUser.firstName :  "";

        return (
            <Body>
                <div className={styles.header} style={{ backgroundImage: `url(${bg})` }}>
                    <h1 className={styles.title}>{firstName ? `${firstName}'s` : "My"} Plant Family</h1>
                    <div className={styles.actions}>
                        <button 
                            className={classNames(styles.button, "primary-button")} 
                            onClick={this.handleAddRoomClick}>+ Add Room</button>
                        <Link to="/plants/add">
                            <button className={classNames(styles.button, "primary-button")}>+ Add Plant</button>
                        </Link>
                    </div>
                </div>
                <PlantList />
            </Body>
        )
    }
}

interface PlantsPageProps {
    showAddRoom: () => ActionType;
    currentUser: User | false;
}

const mapStateToProps = (state: AppState) => {
    return {
        currentUser: state.currentUser
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    showAddRoom: () => dispatch(showAddRoom())
})

const connected = connect(mapStateToProps, mapDispatchToProps)(PlantsPage)
export { connected as PlantsPage };
