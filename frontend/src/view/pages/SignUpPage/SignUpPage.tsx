import { SignUpForm } from "api/auth";
import React, { Component, FormEvent } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { clearError, LoginErrorAction, signup } from "state/auth";
import { FormState, LeafletForm, Validators } from "state/plantForm";
import { ActionType, AppState } from "state/shared";
import { Body } from "view/components/Body";
import { Form, FormError, FormField } from "view/components/Form";
import styles from "./SignUpPage.module.scss";

const FORM_FIELD_NAMES = ["firstName", "email", "password", "passwordRepeat"];

const INTIIAL_FORM_STATE = {
    firstNameError: false,
    emailError: false,
    passwordError: false,
    passwordRepeatError: false,
    passwordMatchError: false
};

class SignUpPage extends Component<SignUpProps, SignUpState> {
    private readonly _form: LeafletForm;

    constructor(props: any) {
        super(props);

        this.state = {
            ...INTIIAL_FORM_STATE
        };

        this._form = new LeafletForm({ 
            firstName: [Validators.required],
            email: [Validators.required],
            password: [Validators.required],
            passwordRepeat: [Validators.required]
        }, {
            passwordMatch: [validatePasswordsMatch]
        });
    }

    componentWillUnmount() {
        this.props.clearError();
    }

    handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        this.setState({ ...INTIIAL_FORM_STATE });

        const { _form } = this;
        const formState = LeafletForm.getFormState(event.target as HTMLFormElement, FORM_FIELD_NAMES);  
             
        if (_form.isValid(formState)) {
            const { email, firstName, password } = formState;
            this.props.signup({ email, firstName, password });
        } else {
            // TODO: fix typecasting
            this.setState({ ...this._form.getErrors(formState) } as any);
        }
    }

    render() {
        const {
            firstNameError,
            emailError,
            passwordError,
            passwordRepeatError,
            passwordMatchError
        } = this.state;

        const { serverError } = this.props;

        return (
            <Body>
                <div className={styles.main}>
                    <div className={styles.form}>
                        <h1>Sign up</h1>
                        <div>Already have an account? <Link to="/login">Login</Link></div>
                        <Form onSubmit={this.handleSubmit} submitName={"Create account"}>
                            <FormField showError={firstNameError}>
                                <input name="firstName" type="text" placeholder="Your first name" />
                            </FormField>
                            <FormField showError={emailError}>
                                <input name="email" type="email" placeholder="E-mail" />
                            </FormField>
                            <FormField showError={passwordError || passwordMatchError}>
                                <input name="password" type="password" placeholder="Your password" />
                            </FormField>
                            <FormField showError={passwordRepeatError || passwordMatchError}>
                                <input name="passwordRepeat" type="password" placeholder="Your password again" />
                            </FormField>
                        </Form>
                        <FormError showError={passwordMatchError}>Passwords do not match.</FormError>
                        <FormError showError={!!serverError}>{`Error: ${serverError}`}</FormError>
                    </div>
                </div>
            </Body>
        )
    }
}

interface SignUpState {
    firstNameError: boolean;
    emailError: boolean;
    passwordError: boolean;
    passwordRepeatError: boolean;
    passwordMatchError: boolean;
}

interface SignUpProps {
    serverError: string;
    signup: (form: SignUpForm) => Promise<LoginErrorAction|void>;
    clearError: () => LoginErrorAction;
}

function validatePasswordsMatch(formState: FormState): boolean {
    const { password, passwordRepeat } = formState;
    return password === passwordRepeat;
}

const mapStateToProps = (state: AppState) => {
    return {
        serverError: state.errors.login
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionType>) => ({
    signup: (form: SignUpForm) => dispatch(signup(form)),
    clearError: () => dispatch(clearError()),
})

const connected = connect(mapStateToProps, mapDispatchToProps)(SignUpPage)
export { connected as SignUpPage };
