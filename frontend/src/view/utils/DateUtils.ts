import { addMonths, addYears, differenceInDays, differenceInMonths, differenceInYears, format, formatISO, parseISO } from "date-fns";

function getPlural(amount: number, word: string): string {
    if (amount !== 1) {
        return `${word}`;
    }
    return word.slice(0, -1);
}

export function getAgeString(date: string): string {
    const order = ["years", "months", "days"];
    const ages = getAgeFromDate(date) as { [key: string]: number };
    let ageString = [];

    for (let key of order) {
        const age = ages[key];
        if (age > 0) {
            ageString.push((ageString.length > 0 ? ", " : "") + `${age} ${getPlural(age, key)}`);
        }
    }

    return `${ageString.join("")} old`;
}

export function getAgeFromDate(date: string): { years: number, months: number, days: number } {
    const today = new Date();
    const result = {
        years: 0,
        months: 0,
        days: 0
    };
    let age = parseISO(date);

    const ageInYears = differenceInYears(today, age);
    if (ageInYears > 0) {
        result.years = ageInYears;
        age = addYears(age, ageInYears);
    }

    const ageInMonths = differenceInMonths(today, age);
    if (ageInMonths > 0) {
        result.months = ageInMonths;
        age = addMonths(age, ageInMonths);
    }

    result.days = differenceInDays(today, age);

    return result;
}

export function getDateString(date?: string | Date): string {
    const normalizedDate = typeof date === "string" ? parseISO(date) : date as Date;
    return normalizedDate ? format(normalizedDate, "MMM d, yyyy") : "";
}

export function formatDateForApi(date: Date): string {
    return formatISO(date, { representation: "date" });
}