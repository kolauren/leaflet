import { Plant } from "api/plant";
import { RouteComponentProps } from "react-router-dom";

export function getIdFromProps(props: RouteComponentProps<{id: string}>): number {
    return parseInt(props.match.params.id, 10);
}

export function getSpeciesNameShort(plant: Plant): string {
    const { classification } = plant;
    return classification ? classification.scientificName : (plant.species || "");
} 


export function getSpeciesNameLong(plant: Plant): string {
    const { classification } = plant;

    if (!classification) {
        return plant.species || "";
    }

    return classification.scientificName + (classification.commonName ? ` (${classification.commonName})` : "");
} 